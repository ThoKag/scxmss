package com.oracle.jcclassic.samples.helloworld;

import javacard.framework.APDU;
import javacard.framework.Applet;
import javacard.framework.ISO7816;
import javacard.framework.ISOException;
import javacard.framework.JCSystem;
import javacard.framework.SystemException;
import javacard.framework.Util;
import javacard.security.AESKey;
import javacard.security.InitializedMessageDigest;
//import javacard.security.AlgorithmParameterSpec;
//import javacard.security.InitializedMessageDigest;
import javacard.security.KeyBuilder;
//import javacard.security.KeyPair;
import javacard.security.MessageDigest;
import javacard.security.RandomData;
import javacard.security.SM4Key;
import javacard.security.SecretKey;
import javacardx.security.derivation.*;
import javacardx.crypto.*;
import javacardx.apdu.ExtendedLength;



//GENERAL TODOS:
/*
TODO: Make arrays/object transient where possible/
		Some Arrays are too large for the simulator. 
		-Problem on real card as well?
		-Optimization? 

TODO: Implement transactions
TODO: Multi-Tree

*/
/*
0x80 0xa0 0x01 0x02 0x06 0x01 0x02 0xfa 0x03 0x04 0x05 0x7F;

0x80 0xA0 0x07 0x00 0x20 0x19 0xAD 0x0A 0x03 0xAD 0x0A 0x92 0x8B 0x00 0x32 0x7A 0x05 0x22 0xAD 0x0B 0x2D 0x10 0x06 0x32 0x7B 0x00 0x31 0x03 0x1A 0x03 0x10 0x06 0x8D 0x00 0x17 0x3B 0x19 0x7F;


80 A0 07 00 20 19 AD 0A 03 AD 0A 92 8B 00 32 7A 05 22 AD 0B 2D 10 06 32 7B 00 31 03 1A 03 10 06 8D 00 17 3B 19 7F
 */

public class HelloWorld extends Applet implements ExtendedLength{

    private byte[] echoBytes;
    private static final short LENGTH_ECHO_BYTES = 256;
    private byte[] flags;
    //MOCKUp STUFF
    private final byte[] sk_seed = {0x00, 0x01, 0x02, 0x04, 0x05, 0x06, 0x07, 0x08, 0x09, 
    		0x0a, 0x0b, 0x0c, 0x0d, 0x0e, 0x0f};
    private final static byte GENERATE_PK = (byte) 0xAA;
    private final static byte INIT = (byte) 0x60;
    private final static byte SIGN_MESSAGE = (byte) 0xa0;
    private final static byte DBG_GEN_KEYPAIR = (byte) 0xF0;
    private final static byte DBG_MESSAGE_BASE = (byte) 0xF1;
    private final static byte PREPARE_NEXT_AUTH_PATH = (byte) 0xF2;
    private final static byte GENERATE_HASH = (byte)0x50;
    private final AESKey secretSeed;
    private final AESKey secretPRFKey;  /* PRFKEY */
    private final byte[] publicKey;  /* ROOT + PUBSEED */
    /* This implementation assumes H/D <= 15, to simplify the arithmetic */
    public static final short D = 1/*5*/;  /* Number of subtrees */
    /* This implementation assumes H <= 30; indices are two signed shorts */
    public static final short H = 4/*20*/;  /* Height of the full tree */
    /* This implementation assumes N = 32 in various (unexpected?) ways */
    public static final short N = 32;  /* Size of a hash output */
    public static final short WOTS_W = 16; //Winternitz parameter
    public static final short WOTS_LOG_W = 4;
    private static final short FLAGS_SIZE = (short)5;
    public static final short WOTS_LEN1 = 64;
    public static final short WOTS_LEN2 = 3;
    public static final short WOTS_LEN = 67;
    private InitializedMessageDigest sha256;
    public static final short single_cluster_array =23;
    
    public static final short ADDRESS_SIZE = 32;
    public static final short ADDRESS_WOTS = 0;
    public static final short ADDRESS_L_TREE= 1;
    public static final short ADDRESS_XMSS_TREE = 2;
    
    public static final short KAM_SET_KEY = 0;
    public static final short KAM_SET_BITMASK_LEAST = 1;
    public static final short KAM_SET_BITMASK_MOST = 2;
    public static final short MAX_MESSAGE_LENGTH = 480;

    //stuff because javacard is stupid:
    public static final short s0 = 0;
    public static final byte b0 = 0; 
    
    public short currentIndex;  //current active keypair index
    public short currentLayerAddressHigh;
    public short currentLayerAddressLow;
    public short currentTreeAddress;
    
    public short number_of_leaves;
    //Hash stuff
    private MessageDigest md256;
    private Cipher aes;
    private AESKey key;
    
    private RandomData randomData;
    private byte[] SEED;
    private short index; //Index in tree
    public short secSeedSize;
    public short secKeySize;
    public short signatureSize;
    RandomData randomness;
    private short prepared_authPath; //stores the index to which the authentication path is currently stored
    
    //Utility arrays
    private byte[] message_in;
    private short[] arr_key_indices;
    private byte[] arr_address1;
    private byte[] arr_address2;
    private byte[] arr_treeAddress_left;
    private byte[] arr_treeAddress_right;
    private byte[] arr_key_uncompressed;
    private byte[] arr_key_uncompressed2;   
    private byte[] arr_wots_signature;
    private byte[] arr_tmp;
    private byte[] arr_seed;
    private byte[] arr_N;
    private byte[] arr_N2;
    private byte[] arr_N3;
    private byte[] arr_N4;
    private byte[] arr_2N;
    private byte[] arr_3N;
    private byte[] arr_4N;
    private byte[] arr_R;
    private byte[] arr_bitmask1;
    private byte[] arr_bitmask2;
    private byte[] arr_auth_node_addr;
    private byte[] authPath;
    private short[] heights;
    byte[] signature;
    private boolean signing = false;
    private byte [] arr_stack;
    //private byte[] arr_root;
    //DEBUG ARRAY ROOT:
    private byte arr_root[];// = {(byte)(0x1), (byte)(0x58), (byte)(0x37), (byte)(0x77), (byte)(0xc0), (byte)(0x8b), (byte)(0x9b), (byte)(0xa5), (byte)(0x9c), (byte)(0x4), (byte)(0x11), (byte)(0xfc), (byte)(0x64), (byte)(0x37), (byte)(0xb8), (byte)(0x2), (byte)(0x52), (byte)(0xbf), (byte)(0x6f), (byte)(0x83), (byte)(0x2), (byte)(0xcf), (byte)(0xb8), (byte)(0x76), (byte)(0x34), (byte)(0x95), (byte)(0xad), (byte)(0xe6), (byte)(0xac), (byte)(0xb6), (byte)(0xd0), (byte)(0x30)};
    private byte[] S;
    /**
     * Only this class's install method should create the applet object.
     */
    protected HelloWorld() {
        echoBytes = new byte[LENGTH_ECHO_BYTES];
        publicKey = new byte[(short)64];
        secretSeed = (AESKey)KeyBuilder.buildKey(KeyBuilder.TYPE_AES, KeyBuilder.LENGTH_AES_256, false);
        secSeedSize = (short)(secretSeed.getSize()/8);
        secKeySize = N*WOTS_LEN;
        signatureSize = (short) (4 + N + secKeySize + H * N );
        md256 = MessageDigest.getInstance(MessageDigest.ALG_SHA_256, false);
        number_of_leaves =  1<<H ; // TODO: OVERFLOW ALARM
        //Mockup:
        SEED = new byte[secSeedSize];
        for(short i = 0; i< secSeedSize; i++) {
        	SEED[i] = (byte)i;
        }
       
        // Util arrays
        //arr_wots_nodes = new byte[(short)(secKeySize*WOTS_W)];
//        arr_key_indices = new short[WOTS_LEN];
//        arr_address1 = new byte[ADDRESS_SIZE];
        randomness = RandomData.getInstance(RandomData.ALG_PSEUDO_RANDOM);
        arr_key_uncompressed = new byte[secKeySize];
        arr_key_uncompressed2 = new byte[(short)(secKeySize+N)];  // holds additional N bytes in case of padding inside genLtree

        arr_wots_signature = new byte[secKeySize];
        signature =new byte[signatureSize];	//stores XMSS Signature
        authPath = new byte[N*H];
        arr_root = new byte[N]; // SEE DEBUG SECTION
        S = new byte[N];
        for(short s = 0; s<N; ++s) {
        	S[s] = (byte)0xFF;
        }
        arr_auth_node_addr = new byte[H*ADDRESS_SIZE];
        message_in = new byte[(short)(MAX_MESSAGE_LENGTH + 4*N)]; //for H_msg the message needs 4*N additionaly bytes
        
//        arr_workingArray = new byte[(short)((1<<H) * N)];
        
//        arr_wots_signature = JCSystem.makeTransientByteArray(secKeySize, JCSystem.CLEAR_ON_DESELECT);
        arr_stack = JCSystem.makeTransientByteArray((short)((H+1)*N), JCSystem.CLEAR_ON_DESELECT);
        arr_tmp = JCSystem.makeTransientByteArray((short)(ADDRESS_SIZE+N), JCSystem.CLEAR_ON_DESELECT);
        arr_key_indices = JCSystem.makeTransientShortArray(WOTS_LEN, JCSystem.CLEAR_ON_DESELECT);
        arr_address1 = JCSystem.makeTransientByteArray(ADDRESS_SIZE, JCSystem.CLEAR_ON_DESELECT);
        arr_address2 = JCSystem.makeTransientByteArray(ADDRESS_SIZE, JCSystem.CLEAR_ON_DESELECT);
        arr_treeAddress_left = JCSystem.makeTransientByteArray(ADDRESS_SIZE, JCSystem.CLEAR_ON_DESELECT);
        arr_treeAddress_right = JCSystem.makeTransientByteArray(ADDRESS_SIZE, JCSystem.CLEAR_ON_DESELECT);
        arr_seed = JCSystem.makeTransientByteArray(secSeedSize, JCSystem.CLEAR_ON_DESELECT);
        arr_R = JCSystem.makeTransientByteArray(N, JCSystem.CLEAR_ON_DESELECT);
        arr_N = JCSystem.makeTransientByteArray(N, JCSystem.CLEAR_ON_DESELECT);
        arr_N2 = JCSystem.makeTransientByteArray(N, JCSystem.CLEAR_ON_DESELECT);
        arr_N3 = JCSystem.makeTransientByteArray(N, JCSystem.CLEAR_ON_DESELECT);
        arr_N4 = JCSystem.makeTransientByteArray(N, JCSystem.CLEAR_ON_DESELECT);
        arr_2N = JCSystem.makeTransientByteArray((short)(2*N), JCSystem.CLEAR_ON_DESELECT);
        arr_3N = JCSystem.makeTransientByteArray((short)(3*N), JCSystem.CLEAR_ON_DESELECT);
        arr_4N = JCSystem.makeTransientByteArray((short)(4*N), JCSystem.CLEAR_ON_DESELECT);
        arr_bitmask1 = JCSystem.makeTransientByteArray(N, JCSystem.CLEAR_ON_DESELECT);
        arr_bitmask2 = JCSystem.makeTransientByteArray(N, JCSystem.CLEAR_ON_DESELECT);
        heights = JCSystem.makeTransientShortArray((short)(H+1), JCSystem.CLEAR_ON_DESELECT);
//        arr_key_uncompressed = JCSystem.makeTransientByteArray(secKeySize, JCSystem.CLEAR_ON_DESELECT);
//        arr_key_uncompressed2 = JCSystem.makeTransientByteArray(secKeySize, JCSystem.CLEAR_ON_DESELECT);
//        arr_tmp = JCSystem.makeTransientByteArray(single_cluster_array, JCSystem.CLEAR_ON_DESELECT);
   
        //DEBUG SECTION_____
        
        
        //DEBUG SECTION END____
        secretSeed.setKey(SEED, (short) 0);
        secretPRFKey = (AESKey)KeyBuilder.buildKey(KeyBuilder.TYPE_AES, KeyBuilder.LENGTH_AES_256, false);
        //flags = JCSystem.makeTransientByteArray(FLAGS_SIZE, JCSystem.CLEAR_ON_DESELECT);
        JCSystem.requestObjectDeletion();
        register();
    }

    /**
     * Installs this applet.
     *
     * @param bArray
     *            the array containing installation parameters
     * @param bOffset
     *            the starting offset in bArray
     * @param bLength 
     *            the length in bytes of the parameter data in bArray
     */
    public static void install(byte[] bArray, short bOffset, byte bLength) {
        new HelloWorld();
    }

    /**
     * Processes an incoming APDU.
     *
     * @see APDU
     * @param apdu
     *            the incoming APDU
     * @exception ISOException
     *                with the response bytes per ISO 7816-4
     */
   @Override 
   public void process(APDU apdu) {
        byte buffer[] = apdu.getBuffer(); 
//        short LC = apdu.setIncomingAndReceive();
//        short recvLen = apdu.setIncomingAndReceive();
//        short dataOffset = apdu.getOffsetCdata();
        // check SELECT APDU command
        if ((buffer[ISO7816.OFFSET_CLA] == 0) &&
                (buffer[ISO7816.OFFSET_INS] == (byte) (0xA4))) {
            return;
        }
        switch(buffer[ISO7816.OFFSET_INS]) {
        case GENERATE_PK:
        	//genAddress(ADDRESS_XMSS_TREE, (short)0, H, arr_address1);
        	arr_address1[0] = (byte)0; //layer address
        	arr_address1[1] = (byte)0;
        	arr_address1[2] = (byte)0;
        	arr_address1[3] = (byte)0;
        	
        	arr_address1[4] = (byte)0; //tree address
        	arr_address1[5] = (byte)0;
        	arr_address1[6] = (byte)0;
        	arr_address1[7] = (byte)0;
        	arr_address1[8] = (byte)0;
        	arr_address1[9] = (byte)0;
        	arr_address1[10] = (byte)0;
        	arr_address1[11] = (byte)0;
        	
        	arr_address1[12] = (byte)0;	//type
        	arr_address1[13] = (byte)0;
        	arr_address1[14] = (byte)((ADDRESS_XMSS_TREE >> 8) & 0xff); 
        	arr_address1[15] = (byte)(ADDRESS_XMSS_TREE & 0xff);
        	
        	arr_address1[16] = (byte)0; //padding
        	arr_address1[17] = (byte)0;
        	arr_address1[18] = (byte)0;
        	arr_address1[19] = (byte)0;
        	
        	arr_address1[20] = (byte)0; //tree height
        	arr_address1[21] = (byte)0;
        	arr_address1[22] = (byte)((H >> 8) & 0xff); 
        	arr_address1[23] = (byte)(H & 0xff);
        	
        	arr_address1[24] = (byte)0; //tree index
        	arr_address1[25] = (byte)0;
        	arr_address1[26] = (byte)0;
        	arr_address1[27] = (byte)0;
        	
        	arr_address1[28] = (byte)0; //keyAndMask
        	arr_address1[29] = (byte)0;
        	arr_address1[30] = (byte)0;
        	arr_address1[31] = (byte)0;
        	
        	
        	
        	
        	
        	
        	treehash(arr_address1, arr_N);
        	Util.arrayCopyNonAtomic(arr_N, (short)0, arr_root, (short)0, N); // save computed root 
        	send_response(apdu, arr_N, N);
        	break;
        case INIT:
        	helloTest(apdu, (short)0);
        	break;
        case SIGN_MESSAGE: 
        	Util.arrayFillNonAtomic(arr_auth_node_addr, (short)0, (short)(H*ADDRESS_SIZE), (byte)0);
            Util.arrayFillNonAtomic(arr_stack, (short)0, (short)(H*(2+N)), (byte)0);
            //Util.arrayFillNonAtomic(authPath, (short)0, (short)(N*H), (byte)0);
            Util.arrayFillNonAtomic(signature, (short)0, signatureSize, (byte)0);
            Util.arrayFillNonAtomic(arr_tmp, (short)0, single_cluster_array, (byte)0);
            Util.arrayFillNonAtomic(arr_key_uncompressed2, (short)0, (short)(secKeySize+N), (byte)0);
            Util.arrayFillNonAtomic(arr_key_uncompressed, (short)0, secKeySize, (byte)0);
        	sign_message(apdu, signature);				//XMSS Signature generation
        	sendSignature(apdu, signature);
        	break;
        	
        case PREPARE_NEXT_AUTH_PATH: // pre-calculate the authentication path for the next signature
        	gen_auth_path(authPath);
        	break;

        case DBG_MESSAGE_BASE:
        	short s = apdu.getIncomingLength();
        	byte[] message = new byte[s];
        	Util.arrayCopy(buffer, (short)ISO7816.OFFSET_CDATA, message, (short)0, s);
        	short[] result = new short[WOTS_LEN];
        	//baseFunction(message, (short)0, WOTS_LEN,(short)0, result);
        	chainLengths(message,arr_key_indices);
        	break;
        	
        
        default:
        	ISOException.throwIt(ISO7816.SW_INS_NOT_SUPPORTED);
        }
        	
     
    }
   
   //Debug Function, generates sk & pk for given index
   //TODO: Maybe use function similar to this to calculate pk for ltree construction. Optimize by not storing intermediate results
    private short wotsPairAtIDX(byte[] address, byte[] sk, byte[] pk){
    	//calculate Seed at index
    	//byte[] seed = new byte[secSeedSize];
    	//get_seed(address, secretSeed, arr_seed);
    	
    	//generate SK from seed
    	byte[] wotsSK = new byte[secKeySize];
    	genSk(arr_seed, wotsSK);
    	
    	//calculate hash values and PK
    	Util.arrayCopy(wotsSK, (short)0, sk, (short)0, secKeySize);
    	short sklen = (short)(secKeySize*WOTS_W);
    	byte[] results = new byte[sklen];
    	for(short s = 0; s<WOTS_LEN; ++s) {
    		arr_key_indices[s] = WOTS_W-1;
    	}
    	
    	chain(sk, address, arr_key_indices, results);
    	
    	//results are stored in format |chainNodes(W*len_n0)|chainNodes(W*len_n1)|...
    	//return results
    	Util.arrayCopy(results, (short)(N*WOTS_LEN*WOTS_W-N), pk, (short)0, N);
    	
		return 0;
    }
    
    
    //actual signature generation
    private void sign_message(APDU apdu, byte[] signature){
    	if(signing == true) { //Card tear or interrupt, error handling (verify functionality, maybe change process)
    		currentIndex+=2;	//increment index as a safety precaution
    	}
    	signing = true; // set flag to true for later completion check
    	gen_wots_signature(apdu, arr_wots_signature);		//generate signature and compressed pubKey
    	//arr_key_uncompressed & arr_key_uncompressed2 free again
    	//gen_auth_path(authPath);	// caluclates authentication Path for current Index
    	Util.arrayFillNonAtomic(arr_address1, s0, N, b0);
    	setAddressType(arr_address1, ADDRESS_XMSS_TREE);
    	setTreeHeight(arr_address1, H);
    	treehash(arr_address1, arr_root);
    	//currentIndex | R | arr_wots_signature | authPath
    	//	4 + N + (len * N) + (H * N ) =  2308 bytes
    	Util.setShort(signature, (short)2, currentIndex); 	//currentIndex
    	Util.arrayCopyNonAtomic(arr_R, s0, signature, (short)4, N);	// R
    	Util.arrayCopyNonAtomic(arr_wots_signature, (short)0, signature, (short)(4+N), secKeySize); //arr_wots_signature
    	Util.arrayCopyNonAtomic(authPath, (short)0, signature, (short)(secKeySize+4+N), (short)(N*H));
    	signing = false;
    	currentIndex+=1;
    	
    }

    
  //..............0
  //........../        \
  //.........1         __
  //......./  \       /   \
  //......__   4     5     6
  //...../ \  / \   / \   / \
  //....7  8 __  X 11 12 13 14
  //.............^
    //calculates the authentication path necessary to validate WOTS at the current index
    //auth_path neeeds to be of length N*H
    public void gen_auth_path(byte[] auth_path) {
    	/*
    	 * Calculate addresses of nodes needed for authentication path 
    	 */
    	//byte[] arr_auth_node_addr = new byte[H*ADDRESS_SIZE]; //Stores authentication path addresses, from leaf to rootLevel-1
    	
    	
    	if (currentIndex % 2 == 0 ) { //currentIndex = right leaf
    		
    	}else {	//currentIndex = left leaf
    		
    	}
    	//TODO: also currentIndex == 0 should be possible to pre-calculate
    	if(prepared_authPath == currentIndex && currentIndex!=0) { //authentication path was already pre-calculated
    		return; 
    	}
    	
    	//IDEA: start from root, left and right until at leaf
    	short currentLevel = H-1;	// level inside XMSS Tree that is currently handled, starting at root with height H
    	short middleAtLevel = (short)(number_of_leaves); //if all leaves < middleAtLevel children of left branch
    													 //first correct value is calculated in first while loop iteration
    	//byte[] parent = new byte[ADDRESS_SIZE];
    	//byte[] left_C = new byte[ADDRESS_SIZE]; now arr_treeAddress_left
    	//byte[] right_C = new byte[ADDRESS_SIZE]; now arr_treeAddress_right
    	
    	//genAddress(ADDRESS_XMSS_TREE, (short)0, H, arr_address1); // start at root
    	
    	arr_address1[0] = (byte)0; //layer address
    	arr_address1[1] = (byte)0;
    	arr_address1[2] = (byte)0;
    	arr_address1[3] = (byte)0;
    	
    	arr_address1[4] = (byte)0; //tree address
    	arr_address1[5] = (byte)0;
    	arr_address1[6] = (byte)0;
    	arr_address1[7] = (byte)0;
    	arr_address1[8] = (byte)0;
    	arr_address1[9] = (byte)0; 
    	arr_address1[10] = (byte)0;
    	arr_address1[11] = (byte)0;
    	
    	arr_address1[12] = (byte)0;	//type
    	arr_address1[13] = (byte)0;
    	arr_address1[14] = (byte)((ADDRESS_XMSS_TREE >> 8) & 0xff); 
    	arr_address1[15] = (byte)(ADDRESS_XMSS_TREE & 0xff);
    	
    	arr_address1[16] = (byte)0; //padding
    	arr_address1[17] = (byte)0;
    	arr_address1[18] = (byte)0;
    	arr_address1[19] = (byte)0; 
    	
    	arr_address1[20] = (byte)0; //tree height
    	arr_address1[21] = (byte)0;
    	arr_address1[22] = (byte)((H >> 8) & 0xff); 
    	arr_address1[23] = (byte)(H & 0xff);
    	
    	arr_address1[24] = (byte)0; //tree index
    	arr_address1[25] = (byte)0;
    	arr_address1[26] = (byte)0;
    	arr_address1[27] = (byte)0;
    	
    	arr_address1[28] = (byte)0; //keyAndMask
    	arr_address1[29] = (byte)0;
    	arr_address1[30] = (byte)0;
    	arr_address1[31] = (byte)0;
    	
    	//Util.arrayCopyNonAtomic(right_C, (short)0, auth_path, (short)(H*ADDRESS_SIZE-1-4), ADDRESS_SIZE); //store root at the end of authPath
    	short loopCount = 0;
    	short middleOffset = 0;
    	while(currentLevel >=0) { 
    		middleAtLevel = (short) (middleAtLevel /2+middleOffset);
    		getChildren(arr_address1, arr_treeAddress_left, arr_treeAddress_right);
    		short currentOffset =  (short)( ADDRESS_SIZE*(currentLevel)) ;
    		if (currentIndex < middleAtLevel) { // leaf to the left, store right sibling in authPath
    			//store sibling on authentication path, starting from the end
    			Util.arrayCopyNonAtomic(arr_treeAddress_right, (short)0, arr_auth_node_addr, currentOffset, ADDRESS_SIZE); 
    			Util.arrayCopyNonAtomic(arr_treeAddress_left, (short)0, arr_address1, (short)0, ADDRESS_SIZE); //next node on path to target leaf is new parent
    		}else {
    			//leaf to the right, store left sibling in authPath
    			//store sibling on authentication path, starting from the end
    			Util.arrayCopyNonAtomic(arr_treeAddress_left, (short)0, arr_auth_node_addr, currentOffset, ADDRESS_SIZE);
    			Util.arrayCopyNonAtomic(arr_treeAddress_right, (short)0, arr_address1, (short)0, ADDRESS_SIZE); //next node on path to target leaf is new parent
    			middleOffset += 1<<currentLevel; // whenever next parent is to the right, offset new middle by
    														  // the number of nodes of the subtree that was added to the authPath
    		}
    		loopCount++;
    		currentLevel--;
    	}//auth_node_addr now contains all authPath node addresses, each of length ADDRESS_SIZE, starting at lowest tree level to root
    	
    	/* 
    	 * Treehash algorithm calculates value for the specified address
    	 */
    	
    	
    	for(short s = 0; s < H; ++s) {
    		Util.arrayCopyNonAtomic(arr_auth_node_addr, (short)(s*ADDRESS_SIZE), arr_address1, (short)0, ADDRESS_SIZE);
    		treehash(arr_address1, arr_N);
    		Util.arrayCopyNonAtomic(arr_N, (short)0, auth_path, (short)(s*N), N);
    		prepared_authPath = currentIndex;
    	}

    }
    
  //..............0								 0							 1
  //........../        \					/         \				    /         \
  //.........1          2				    0	  	    1			   2	  	    3
  //......./  \       /   \			      /  \		  /   \			 /  \		  /   \
  //......3    4     5     6			 0    1      2     3	    4    5      6     7
  //...../ \  / \   / \   / \			/ \  / \    / \   / \	   / \  / \    / \   / \
  //....7  8  9 10 11 12 13 14		   0  1  2  3  4  5  6   7	  8  9 10  11 12 13 14  15
      
      //calculate value at tree node at xmss_address - pretty stupid version without any optimization 
    //xmss_address = valid xmss address generated by genAddress(ADDRESS_XMSS_TREE,...)
    //value = N-byte array 
    //LOCAL in use: arr_key_uncompressed
    public void treehash(byte[] xmss_address, byte[]result){
    	// calculate all xmss_nodes underneath xmss_address
    	boolean endloop = false;
    	//xmss_address = XMSS Tree address
    	short offset = 0;
    	short tree_idx; // used inside while loop
    	short index = getShortAtByte(xmss_address, (short)26);
    	short height = getShortAtByte(xmss_address, (short)22);
    	//short [] indices_wots_needed = new short[height*height]; //height*height == number of leaves underneath xmss_address
    	Util.arrayFillNonAtomic(arr_stack, s0, (short)(H*(N+2)), b0);
    	Util.setShort(arr_stack, s0, (short)-1);
    	short rangeLow = (short) (index*(1<<height));		// index of lowest WOTS key needed
    	short rangeHigh = (short) (rangeLow+(1<<height)-1); // index of highest WOTS key needed
    	secretSeed.getKey(arr_seed, s0);
    	//byte [] arr_workingArray = new byte[(short)((1<<height) * N)]; // stores the computed values during computation 	
    	
    	//initially fill working Array with all wots pk keys needed
    	//byte[] tempAddress = new byte[ADDRESS_SIZE];
    	//byte[] tempResult = new byte[N];
    	for(short wotsIndex = rangeLow, loopCount = 0;wotsIndex<=rangeHigh;++wotsIndex, ++loopCount) {
    		//genAddress(ADDRESS_WOTS, wotsIndex, (short)0, arr_address1);
    		//TODO: Clean up the following address fill section
    		endloop = false;
    		Util.arrayFillNonAtomic(arr_address2, (short)0, ADDRESS_SIZE, (byte)0);
    		arr_address2[0] = (short)0; //layer address
    		arr_address2[1] = (short)0;
    		arr_address2[2] = (short)0;
    		arr_address2[3] = (short)0;
    		
    		arr_address2[4] = (short)0;	//tree address
    		arr_address2[5] = (short)0;
    		arr_address2[6] = (short)0;
    		arr_address2[7] = (short)0;
    		arr_address2[8] = (short)0;
    		arr_address2[9] = (short)0;
    		arr_address2[10] = (short)0;
    		arr_address2[11] = (short)0;
    		
    		arr_address2[12] = (byte)0;	//type
        	arr_address2[13] = (byte)0;
        	arr_address2[14] = (byte)((ADDRESS_WOTS >> 8) & 0xff); 
        	arr_address2[15] = (byte)(ADDRESS_WOTS & 0xff);
        	
        	arr_address2[16] = (byte)0;	//OTS address - index
        	arr_address2[17] = (byte)0;
        	arr_address2[18] = (byte)((wotsIndex >> 8) & 0xff); 
        	arr_address2[19] = (byte)(wotsIndex & 0xff);
        	
        	arr_address2[20] = (byte)0;	//chain address
        	arr_address2[21] = (byte)0;
        	arr_address2[22] = (byte)0; 
        	arr_address2[23] = (byte)0;
        	
        	arr_address2[24] = (byte)0;	//hash address
        	arr_address2[25] = (byte)0;
        	arr_address2[26] = (byte)0; 
        	arr_address2[27] = (byte)0;
        	
        	arr_address2[28] = (byte)0;	//keyAndMask
        	arr_address2[29] = (byte)0;
        	arr_address2[30] = (byte)0; 
        	arr_address2[31] = (byte)0;
        	
     		get_xmss_leaf(arr_address2, arr_stack, (short)(offset*N)); //arr_addess1 could be reused here
     		// arr_N now cointains the most recently computed node
     		setAddressType(arr_address2, ADDRESS_XMSS_TREE);
     		setTreeHeight(arr_address2, s0);
     		setTreeIndex(arr_address2, wotsIndex); 
     		
     		offset++;
     		heights[(short)(offset-1)] = 0;
     		
     		if((currentIndex ^ 0x1) == wotsIndex) { //this node is part of the auth path
     			Util.arrayCopyNonAtomic(arr_stack, (short)((offset-1)*N), authPath, s0, N);
     		}
     		
     		//only nodes of height 0 are generated above. 
     		//nodes of higher level are constructed below
     		
     		//while (stackHeight == workingHeight && endloop == false/*&& topnode > -1*/) {//Check if top node on Stack has same height as node in arr_N
     		while(offset >=2 && heights[(short)(offset-1)] == heights[(short)(offset-2)]) {
     			tree_idx = (short) (wotsIndex >>(heights[(short)(offset-1)]+1));
     			setTreeHeight(arr_address2, heights[(short)(offset-1)]);
     			setTreeIndex(arr_address2, tree_idx);
     			
         		
     			Util.arrayCopyNonAtomic(arr_stack, (short)((offset-2)*N), arr_2N, s0, (short)(2*N));
     			
     			setTreePadding(arr_address2);
     			RAND_HASH(arr_2N, arr_seed, arr_address2, arr_N);
     			Util.arrayCopyNonAtomic(arr_N, s0, arr_stack, (short)((offset-2)*N), N);
     			offset--;

     			heights[(short)(offset-1)]++;
     			if(((currentIndex >> heights[(short)(offset-1)]) ^0x1)==tree_idx){
     				Util.arrayCopyNonAtomic(arr_stack, (short)((offset-1)*N), authPath, (short)(heights[(short)(offset-1)]*N), N);
     			}
     		}
    	}
    	Util.arrayCopyNonAtomic(arr_stack, s0, result, s0, N);
    }


    

    /*
     * calculates the L-tree root of the specified WOTS-Address 
     * result offset = offset within ltree_root, where result is written
     */
    public void get_xmss_leaf(byte[] WOTS_address, byte[]ltree_root, short result_offset) {
    	short nextIndex = Util.getShort(WOTS_address, (short)18); // save index for l-tree construction	
    	//calculate Seed at index
    	//get_seed(WOTS_address, secretSeed, arr_seed);
    	
    	//generate SK
    	genSk(WOTS_address, arr_key_uncompressed);
    	
    	//calculate hash values and PK
    	//chain(arr_key_uncompressed, arr_wots_nodes, WOTS_W);
    	
    	for(short s = 0; s<WOTS_LEN; ++s) {
    		arr_key_indices[s] = (short)(WOTS_W-1);
    	}
    	
    	
    	chain(arr_key_uncompressed, WOTS_address, arr_key_indices, arr_key_uncompressed2);
    	//arr_key_uncompressed2 = pk at wots_address
    	
    	//short pos = getAddressByte(arr_address1, (short)1);
        //genAddress(ADDRESS_WOTS, pos, (short)0, arr_address1);
    	Util.arrayFillNonAtomic(arr_N2, (short)0, N, (byte)0);
    	setOtsChainAddr(WOTS_address, s0);
    	setLTreeAddr(WOTS_address, nextIndex);
    	setAddressType(WOTS_address, ADDRESS_L_TREE);
    	setLTreeIdx(WOTS_address,s0);
    	//TODO:address needs to be properly created 
        gen_ltree(WOTS_address, arr_key_uncompressed2, ltree_root, result_offset);
    }
    


    
    
    /*
     * Calculates all nodes underneath the specified address and stores them in result.
     * address needs to be a valid XMSS Address, result needs to be of 2^(address.height+1)-1 * ADDRESS_SIZE bytes length
     * The nodes found can then be used to check weather they where already computed and have transiently stored results
     */
    public void nodes_underneath(byte[] address, byte[]result) {
     //TODO: Really necessary? Nodes should be computable from address alone, no need for expensive arraycopys etc.	
    }
    
    
    public void getChildren(byte[] parentAddr, byte[]leftChild, byte[] rightChild) {
    	
//    	short type = getAddressType(parentAddr);
//    	short levelIndex = getAddressByte(parentAddr, (short)1);
//    	short parentHeight = getAddressByte(parentAddr, (short)2);
    	Util.arrayCopyNonAtomic(parentAddr, (short)0, leftChild, (short)0, ADDRESS_SIZE);
    	Util.arrayCopyNonAtomic(parentAddr, (short)0, rightChild, (short)0, ADDRESS_SIZE);
    	short treeHeight = Util.getShort(parentAddr, (short)22);
    	short treeIndex = Util.getShort(parentAddr, (short)26);
    	
    	treeHeight = (short)(treeHeight-1);
    	leftChild[22] = (byte)(((treeHeight) >> 8) & 0xff); 
    	leftChild[23] = (byte)((treeHeight)& 0xff);
    	rightChild[22] = (byte)(((treeHeight) >> 8) & 0xff); 
    	rightChild[23] = (byte)((treeHeight)& 0xff);
    	
    	treeIndex = (short)(treeIndex*2);
    	leftChild[26] = (byte)(((treeIndex) >> 8) & 0xff); 
    	leftChild[27] = (byte)((treeIndex)& 0xff);
    	rightChild[26] = (byte)(((treeIndex+1) >> 8) & 0xff); 
    	rightChild[27] = (byte)((treeIndex+1)& 0xff);
    }
    /*
     * Simple Tree traversal algorithm.
     * Calculates all other pubKeys and nodes from scratch every time.
     * 
     */
    public void tt_simple(){
    	
    }
    
    
  
    /*
     * LOCAL: 	arr_key_uncompressed
     * 			arr_key_uncompressed2 
     */
    public void gen_wots_signature(APDU apdu, byte[] wots_signature) {
    	byte buffer[] = apdu.getBuffer(); 	
    	short msg_len = apdu.getIncomingLength();
    	//byte[] message_in = new byte[len];
    	Util.arrayCopy(buffer, (short)ISO7816.OFFSET_CDATA, message_in, (short)(4*N), msg_len);	// message_in = actual message to sign
    	//short[] bases = new short[WOTS_LEN];	//stores calculated bases for message 
    	//baseFunction(message, (short)0, WOTS_LEN, arr_key_indices);			//bases now contains WOTS_LEN short values

    	//r
    	//initialize r = arr_N2
    	//key:
    	Util.arrayFillNonAtomic(arr_N3, (short)0, N, (byte)0);
    	arr_N3[N-2] = (byte)((currentIndex >> 8) & 0xff); 
    	arr_N3[N-1] = (byte)(currentIndex & 0xff);
    	secretSeed.getKey(arr_N, (short)0);
    	//arr_N = secretSeed
    	//arr_N3 = currentIndex in bytes
    	//PRF(secretSeed, index )
    	PRF(arr_N, arr_N3, arr_N2); //r = arr_N2
    	Util.arrayCopyNonAtomic(arr_N2, s0, arr_R, s0, N); // save R for final signature 
    	
    	//root:
    	//key(3n) = r(n)   + root(n) + tobyte_tmp(n)
    	// here:	arr_N2	 arr_root  arr_N3
    	Util.arrayCopyNonAtomic(arr_N2, (short)0, arr_3N, (short)0, N);
    	Util.arrayCopyNonAtomic(arr_root, (short)0, arr_3N, (short)N, N);
    	Util.arrayCopyNonAtomic(arr_N3, (short)0, arr_3N, (short)(2*N), N);
    	H_msg(arr_3N, message_in, msg_len, arr_N2); // hash the input message to 32 byte len
    	chainLengths(arr_N2,arr_key_indices); 
        
        //genAddress(ADDRESS_WOTS, currentIndex, (short)0, arr_address1);//stores currently used WOTS-Signature
        Util.arrayFillNonAtomic(arr_address1, (short)0, ADDRESS_SIZE, (byte)0);
    	arr_address1[0] = (short)0; //layer address
		arr_address1[1] = (short)0;
		arr_address1[2] = (short)0;
		arr_address1[3] = (short)0;
		
		arr_address1[4] = (short)0;	//tree address
		arr_address1[5] = (short)0; 
		arr_address1[6] = (short)0;
		arr_address1[7] = (short)0;
		arr_address1[8] = (short)0;
		arr_address1[9] = (short)0;
		arr_address1[10] = (short)0;
		arr_address1[11] = (short)0;
		
		arr_address1[12] = (byte)0;	//type
    	arr_address1[13] = (byte)0;
    	arr_address1[14] = (byte)((ADDRESS_WOTS >> 8) & 0xff); 
    	arr_address1[15] = (byte)(ADDRESS_WOTS & 0xff);
    	
    	arr_address1[16] = (byte)0;	//OTS address - index
    	arr_address1[17] = (byte)0;
    	arr_address1[18] = (byte)((currentIndex >> 8) & 0xff); 
    	arr_address1[19] = (byte)(currentIndex & 0xff);
    	
    	arr_address1[20] = (byte)0;	//chain address
    	arr_address1[21] = (byte)0;
    	arr_address1[22] = (byte)0; 
    	arr_address1[23] = (byte)0;
    	
    	arr_address1[24] = (byte)0;	//hash address
    	arr_address1[25] = (byte)0;
    	arr_address1[26] = (byte)0; 
    	arr_address1[27] = (byte)0;
    	
    	arr_address1[28] = (byte)0;	//keyAndMask
    	arr_address1[29] = (byte)0;
    	arr_address1[30] = (byte)0; 
    	arr_address1[31] = (byte)0;
    	
         
        genSk(arr_address1, arr_key_uncompressed2);
        chain(arr_key_uncompressed2, arr_address1, arr_key_indices, wots_signature);
        //gen_ltree(arr_address1, wots_signature, pubKey);
    }
    
    //same as get_seed in xmss_reference - takes address and generates n-byte seed
    public void get_seed(byte[] addr_l, byte[] result){
    	//Make sure that chain, hash and kam bytes are zeroed
    	addr_l[20] = (byte)0;	//chain address
    	addr_l[21] = (byte)0;
    	addr_l[22] = (byte)0; 
    	addr_l[23] = (byte)0;
    	
    	addr_l[24] = (byte)0;	//hash address
    	addr_l[25] = (byte)0;
    	addr_l[26] = (byte)0; 
    	addr_l[27] = (byte)0;
    	
    	addr_l[28] = (byte)0;	//keyAndMask
    	addr_l[29] = (byte)0;
    	addr_l[30] = (byte)0; 
    	addr_l[31] = (byte)0;
    	Util.arrayFillNonAtomic(arr_N, (short)0, N, (byte)0);
    	secretSeed.getKey(arr_N, (short)0);
    	//  seed   address
    	PRF(arr_N, addr_l, result);
    }
    
     //L-Tree function
    //compress wots pk from (WOTS_LEN*N) to N bytes
    public void gen_ltree(byte[] addr ,byte[] uncompressed_pk, byte[] result, short result_offset){
    	
    	short l = WOTS_LEN;
    	short parent_nodes;
    	short height = 0;
    	
    	secretSeed.getKey(arr_N, (short)0);
    	setTreeHeight(addr, (short)0);
    	while(l > 1) {
    		parent_nodes = (short)(l >> 1);
    		for(short s = 0; s < parent_nodes; ++s) {
    			setTreeIndex(addr, s);
    			Util.arrayCopyNonAtomic(uncompressed_pk, (short)(2*s*N), arr_2N, (short)0, (short)(2*N));
    			RAND_HASH(arr_2N, arr_N, addr, arr_N3);
    			Util.arrayCopyNonAtomic(arr_N3, (short)0, uncompressed_pk, (short)(s*N), N);
    		}
    		if(l % 2 != 0) {	// odd number of nodes
    			Util.arrayCopyNonAtomic(uncompressed_pk, (short)((l-1)*N), uncompressed_pk, (short)((l >> 1)*N), N);
    			l = (short)((l >> 1) + 1);
    		}else {
    			l = (short)(l >> 1);
    		}
    		height++;
    		setTreeHeight(addr, height);
    	}
    	
    	Util.arrayCopyNonAtomic(uncompressed_pk, (short)0, result, (short)result_offset, N);
    	
    	
    	

    }
    
    //similar to RAND_HASH from RFC but using combined array for nodes 
    // Input: leftandRight = 2N bytes;  l_seed = n bytes;  l_addr = n bytes
    // Output: result = N bytes
    public void RAND_HASH(byte[] leftandRight, byte[] l_seed, byte[] l_addr, byte[] result ) {
    	setKeyAndMask(l_addr, KAM_SET_KEY);
    	PRF(l_seed, l_addr, arr_N4); // ==KEY
    	setKeyAndMask(l_addr, KAM_SET_BITMASK_LEAST);
    	PRF(l_seed, l_addr, arr_bitmask1);
    	setKeyAndMask(l_addr, KAM_SET_BITMASK_MOST);
    	PRF(l_seed, l_addr, arr_bitmask2);
    	
    	for(short s = 0; s<N; ++s) { // xor bitmasks with nodes
    		leftandRight[s] ^= arr_bitmask1[s];
    		leftandRight[(short)(s+N)] ^= arr_bitmask2[s];
    	}
    	H(arr_N4, leftandRight, result);
    }
    
    

    
    public void setAddressType(byte[] address, short type) {
    	address[12] = (byte)0;
    	address[13] = (byte)0;
    	address[14] = (byte)((type >> 8) & 0xff); 
    	address[15] = (byte)(type & 0xff);
    }
    public void setTreeHeight(byte[] address, short treeIndex) {
    	address[20] = (byte)0;
    	address[21] = (byte)0;
    	address[22] = (byte)((treeIndex >> 8) & 0xff); 
    	address[23] = (byte)(treeIndex & 0xff);
    }
    public void setTreeIndex(byte[] address, short treeIndex) {
    	address[24] = (byte)0;
    	address[25] = (byte)0;
    	address[26] = (byte)((treeIndex >> 8) & 0xff); 
    	address[27] = (byte)(treeIndex & 0xff);
    }
    public void setTreePadding(byte[] address) {
    	address[16] = (byte)0;
    	address[17] = (byte)0;
    	address[18] = (byte)0; 
    	address[19] = (byte)0;
    }
    public void setKeyAndMask(byte[] address, short keyAndMask) {
    	address[30] = (byte)0;
    	address[31] = (byte)0;
    	address[30] = (byte)((keyAndMask >> 8) & 0xff); 
    	address[31] = (byte)(keyAndMask & 0xff);
    }
    public void setLTreeIdx(byte[] address, short index) {
    	address[24] = (byte)0;
    	address[25] = (byte)0;
    	address[26] = (byte)((index >> 8) & 0xff); 
    	address[27] = (byte)(index & 0xff);
    }
    public void setLTreeAddr(byte[] address, short index) {
    	address[16] = (byte)0;
    	address[17] = (byte)0;
    	address[18] = (byte)((index >> 8) & 0xff); 
    	address[19] = (byte)(index & 0xff);
    }
    public void setLTreeLvl(byte[] address, short height) {
    	address[20] = (byte)0;
    	address[21] = (byte)0;
    	address[22] = (byte)((height >> 8) & 0xff); 
    	address[23] = (byte)(height & 0xff);
    }
    public void setOtsHashAddr(byte[] address, short hashNbr) {
    	address[26] = (byte)0;
    	address[27] = (byte)0;
    	address[26] = (byte)((hashNbr >> 8) & 0xff);
    	address[27] = (byte)(hashNbr & 0xFF);
    }
    public void setOtsChainAddr(byte[] address, short chainNbr) {
    	address[22] = (byte)0;
    	address[23] = (byte)0;
    	address[22] = (byte)((chainNbr >> 8) & 0xff);
    	address[23] = (byte)(chainNbr & 0xFF);
    }
    


 
    //Input: key = 3N bytes; input = arbitrary length;
    //Output: result = N bytes
    public void F(byte[] l_key, byte[] input, byte[] result) {
    	Util.arrayFillNonAtomic(arr_3N, (short)0, (short)(3*N), (byte)0);
    	Util.arrayCopyNonAtomic(l_key, (short)0, arr_3N, (short)N, N);
    	Util.arrayCopyNonAtomic(input, (short)0, arr_3N, (short)(2*N), N);
    	md256.reset();
        md256.doFinal(arr_3N, (short)0, (short)(3*N), result, (short) 0);
    }
    public void H(byte[] l_key, byte[] input, byte[] result) {
    	Util.arrayFillNonAtomic(arr_4N, (short)0, (short)(3*N), (byte)0);
    	arr_4N[N-2] = (byte)((1 >> 8) & 0xff); 
    	arr_4N[N-1] = (byte)(1 & 0xff);
    	Util.arrayCopyNonAtomic(l_key, (short)0, arr_4N, (short)N, N);
    	Util.arrayCopyNonAtomic(input, (short)0, arr_4N, (short)(2*N), (short)(2*N));
    	md256.reset();
        md256.doFinal(arr_4N, (short)0, (short)(4*N), result, (short) 0);
    }   
    
    
    //INPUT: key = 3byte  input = max MAX_MESSAGE_LENGTH
    //OUTPUT: result = N bytes
    //input here already contains the message, so only the leading 4*N bytes need to be filled
    public void H_msg(byte[] l_key, byte[] input, short msg_len, byte[] result) {
    	Util.arrayFillNonAtomic(input, (short)0, (short)(4*N), (byte)0);
    	input[N-2] = (byte)((2 >> 8) & 0xff); 
    	input[N-1] = (byte)(2 & 0xff);
    	Util.arrayCopyNonAtomic(l_key, (short)0, input, (short)N, (short)(3*N));
    	md256.reset();
        md256.doFinal(input, (short)0, (short)(msg_len + 4*N), result, (short) 0);
    }
    
    
    
    //PRF takes as input an n-byte key and a 32-byte index and generates pseudorandom outputs of length n
    //-> Input: key = n bytes, index = l_addr = 32byte
    public void PRF(byte[]l_key, byte[]l_addr, byte[] result) {
    	Util.arrayFillNonAtomic(arr_3N, (short)0, (short)(3*N), (byte)0);    	//internally, use a 2*N + 32 byte = 3N byte buffer 
    	arr_3N[N-2] = (byte)((3 >> 8) & 0xff); // XMSS_HASH_PADDING_PRF == 3 as domain seperator
    	arr_3N[N-1] = (byte)(3 & 0xff);
    	Util.arrayCopyNonAtomic(l_key, (short)0, arr_3N, (short)N, N); //concatenate key and address as PRF input 
    	Util.arrayCopyNonAtomic(l_addr, (short)0, arr_3N, (short)(2*N), N);
    	md256.reset();
        md256.doFinal(arr_3N, (short)0, (short)(3*N), result, (short) 0);	
    }
    
    

    /* Takes a message and derives the corresponding chain lengths.
    Required space in tmp is inherited from WOTSChecksum. */
    private void chainLengths(byte[] message,short[] indices){
    	baseW(indices, (short)0, message, (short)0, WOTS_LEN1);
    	WOTSChecksum(indices, indices);
    }
    /* Converts msg to baseW and writes results to lengths array, for the
purpose of creating WOTS chains. */
    private void baseW(short[] lengths, short offset,
    		byte[] msg, short o_msg, short msgLength) {
    	short in = 0;
    	short out = 0;
    	byte total = 0;
    	short bits = 0;
    	short consumed;

    	for (consumed = 0; consumed < msgLength; consumed++) {
    		if (bits == 0) {
    			total = msg[(short)(o_msg + in)];
    			in++;
    			bits += 8;
    		}
    		bits -= WOTS_LOG_W;
    		lengths[(short)(out + offset)] = (short)((total >>> bits) & (WOTS_W - 1));
    		out++;
    	}
    }

    /* Computes the WOTS+ checksum over a message (in base WOTS_W).
Requires 2 bytes of temporary data in tmp. */
    private void WOTSChecksum(short[] indices, short[] message) {
    	short csum = 0; /* At most WOTS_LEN1 * WOTS_W */
    	short i;

    	/* Compute checksum */
    	for (i = 0; i < WOTS_LEN1; i++) {
    		csum += (short)(WOTS_W - 1 - message[i]);
    	}

    	/* Ensure expected empty (zero) bits are the least significant bits. */
    	csum <<= (short)((8 - ((short)(WOTS_LEN2 * WOTS_LOG_W) % 8)));
    	/* For W = 16 N = 32, the checksum fits in 10 < 15 bits */
    	Util.setShort(arr_tmp, (short)0, csum);

    	/* Convert checksum to base W */
    	baseW(indices, WOTS_LEN1, arr_tmp, (short)0, WOTS_LEN2);
    }

    //maybe abandon use separate function 
    // gets hashes pointed at inside "bases" from "wots_nodes" 
    public void nodesFromHashes(byte[]wots_nodes, short[] bases, byte[] hashes){
        for(short i = 0; i<WOTS_LEN; ++i){
            //W*N = distance to next segment
            Util.arrayCopy(wots_nodes,(short)(N*bases[i]+i*WOTS_W*N), hashes, (short)(i*N) , N);
        }
    }
    
    private void addr_conv_toReference(byte[] own_addr, byte[] ref_addr) {
    	/*	+-------------------------+
    	 * 	| layer address  (32 bits)|	height of tree within multi tree structure, 0 in single tree
    	 *	+-------------------------+ 
    	 *	Leaving it at 0 for now for single tree variant
    	 */ 
    	short layer_address = 0;
    	ref_addr[0] = (byte)((layer_address >> 8) & 0xff);
    	ref_addr[1] = (byte)(layer_address & 0xff);
    	
    	/*	+-------------------------+
		 * 	| tree address   (64 bits)| position within layer of MT, starting at 0 for leftmost tree
		 *	+-------------------------+ 
		 *	Leaving it at 0 for now for single tree variant
		 */
    	ref_addr[2] = (byte)(0x00);
    	ref_addr[3] = (byte)(0x00);
    	ref_addr[4] = (byte)(0x00);
    	ref_addr[5] = (byte)(0x00);
    	
    	
    	/*
    	+-------------------------+
    	| type = 0       (32 bits)|
    	+-------------------------+
    	*/
    	short type = getAddressType(own_addr);
    	ref_addr[6] = (byte)((type >> 8) & 0xff);
    	ref_addr[7] = (byte)(type & 0xff);
    	
    	if(type == ADDRESS_WOTS) {
    		ref_addr[8] = (byte)0x00;
    		ref_addr[9] = (byte)0x00;
    		short index = getAddressByte(own_addr, (short)1);
    		ref_addr[10] = (byte)((index >> 8) & 0xff);
        	ref_addr[11] = (byte)(index & 0xff);
    	}
    	
    	
    	
    }
    
    public void genAddress(short dummy, short type, short byte1, short byte2, byte[]addr) {
    	/* WOTS							L-tree						Hash-Tree
     
        +-------------------------+	+-------------------------+	+-------------------------+  
        | layer address  (32 bits)|	| layer address  (32 bits)|	| layer address  (32 bits)| 	height of tree
        +-------------------------+ +-------------------------+ +-------------------------+	  
        | tree address   (64 bits)| | tree address   (64 bits)| | tree address   (64 bits)|		position within layer of MT
        +-------------------------+ +-------------------------+ +-------------------------+
        | type = 0       (32 bits)|	| type = 1       (32 bits)| | type = 2       (32 bits)|		type: 0=wots 1=l-tree 2=hash
        +-------------------------+ +-------------------------+ +-------------------------+
        | OTS address    (32 bits)| | L-tree address (32 bits)|	| Padding = 0    (32 bits)|		OTS: index within tree  
        +-------------------------+ +-------------------------+ +-------------------------+
        | chain address  (32 bits)|	| tree height    (32 bits)|	| tree height    (32 bits)|		
        +-------------------------+ +-------------------------+ +-------------------------+
        | hash address   (32 bits)| | tree index     (32 bits)| | tree index     (32 bits)|		
        +-------------------------+ +-------------------------+ +-------------------------+
        | keyAndMask     (32 bits)| | keyAndMask     (32 bits)| | keyAndMask     (32 bits)|		
        +-------------------------+ +-------------------------+ +-------------------------+
    	
    	*/
    	
    	//type
    	addr[0] = (byte)((type >> 8) & 0xff);
    	addr[1] = (byte)(type & 0xff);
    	
    	//nodeNbr
    	addr[2] = (byte)((byte1 >> 8) & 0xff);
    	addr[3] = (byte)(byte1 & 0xff);
    	
    	addr[4] = (byte)((byte2 >> 8) & 0xff);
    	addr[5] = (byte)(byte2 & 0xff);
    	
    	
    }

    // generate address, arguments depend on type
    // Avoid modifying or handling addresses directly and without using the functions below!
    // This should prevent bugs if the address scheme is modified later on
    public void genAddress(short type, short customByte1, short customByte2, short customByte3, short customByte4, byte[]addr) {
    	/* WOTS							L-tree						Hash-Tree
     
        +-------------------------+	+-------------------------+	+-------------------------+  
        | layer address  (32 bits)|	| layer address  (32 bits)|	| layer address  (32 bits)| 	height of tree within multi tree structure, 0 in single tree
        +-------------------------+ +-------------------------+ +-------------------------+	  
        | tree address   (64 bits)| | tree address   (64 bits)| | tree address   (64 bits)|		position within layer of MT, starting at 0 for leftmost tree
        +-------------------------+ +-------------------------+ +-------------------------+
        | type = 0       (32 bits)|	| type = 1       (32 bits)| | type = 2       (32 bits)|		type: 0=wots 1=l-tree 2=hash
        +-------------------------+ +-------------------------+ +-------------------------+
        | OTS address    (32 bits)| | L-tree address (32 bits)|	| Padding = 0    (32 bits)|		OTS: index within tree  
        +-------------------------+ +-------------------------+ +-------------------------+
        | chain address  (32 bits)|	| tree height    (32 bits)|	| tree height    (32 bits)|		
        +-------------------------+ +-------------------------+ +-------------------------+
        | hash address   (32 bits)| | tree index     (32 bits)| | tree index     (32 bits)|		
        +-------------------------+ +-------------------------+ +-------------------------+
        | keyAndMask     (32 bits)| | keyAndMask     (32 bits)| | keyAndMask     (32 bits)|		
        +-------------------------+ +-------------------------+ +-------------------------+
    	
    	
    	*/
    	
    	/*	+-------------------------+
    	 * 	| layer address  (32 bits)|	height of tree within multi tree structure, 0 in single tree
    	 *	+-------------------------+ 
    	 *	Leaving it at 0 for now for single tree variant
    	 */ 
    	short layer_address = 0;
    	addr[0] = (byte)((layer_address >> 8) & 0xff);
    	addr[1] = (byte)(layer_address & 0xff);
    	
		/*	+-------------------------+
		 * 	| tree address   (64 bits)| position within layer of MT, starting at 0 for leftmost tree
		 *	+-------------------------+ 
		 *	Leaving it at 0 for now for single tree variant
		 */
    	addr[2] = (byte)(0x00);
    	addr[3] = (byte)(0x00);
    	addr[4] = (byte)(0x00);
    	addr[5] = (byte)(0x00);
    	
    	/*
    	+-------------------------+
    	| type = 0       (32 bits)|
    	+-------------------------+
    	*/
    	addr[6] = (byte)((type >> 8) & 0xff);
    	addr[7] = (byte)(type & 0xff);
    	
		/*
		+-------------------------+  +-------------------------+  +-------------------------+
		|  OTS address    (32 bits)| | L-tree address (32 bits)|  | Padding = 0    (32 bits)|
		+-------------------------+  +-------------------------+  +-------------------------+
		 */
		addr[8] = (byte)((customByte1 >> 8) & 0xff);
    	addr[9] = (byte)(customByte1 & 0xff);
    	addr[10] = (byte)((customByte1 >> 8) & 0xff);
    	addr[11] = (byte)(customByte1 & 0xff);
    	
    	/*
		+-------------------------+  +-------------------------+  +-------------------------+
    	| chain address  (32 bits)|	| tree height    (32 bits)|	| tree height    (32 bits)|
    	+-------------------------+  +-------------------------+  +-------------------------+
		 */
		addr[12] = (byte)((customByte2 >> 8) & 0xff);
    	addr[13] = (byte)(customByte2 & 0xff);
    	addr[14] = (byte)((customByte2 >> 8) & 0xff);
    	addr[15] = (byte)(customByte2 & 0xff);
    	
    	/*
    	+-------------------------+ +-------------------------+ +-------------------------+
        | hash address   (32 bits)| | tree index     (32 bits)| | tree index     (32 bits)|		
        +-------------------------+ +-------------------------+ +-------------------------+
    	*/
    	addr[16] = (byte)((customByte3 >> 8) & 0xff);
    	addr[17] = (byte)(customByte3 & 0xff);
    	addr[18] = (byte)((customByte3 >> 8) & 0xff);
    	addr[19] = (byte)(customByte3 & 0xff);
    	
    	/*
    	+-------------------------+ +-------------------------+ +-------------------------+
    	| keyAndMask     (32 bits)| | keyAndMask     (32 bits)| | keyAndMask     (32 bits)|		
    	+-------------------------+ +-------------------------+ +-------------------------+
    	 */
    	addr[20] = (byte)((customByte4 >> 8) & 0xff);
    	addr[21] = (byte)(customByte4 & 0xff);
    	addr[22] = (byte)((customByte4 >> 8) & 0xff);
    	addr[23] = (byte)(customByte4 & 0xff);
        	    	
    }
    
    public short getAddressType(byte[] addr) {
    	return Util.getShort(addr, (short)0);	
    }
    
    //byteNbr ==1 means first byte after addr type 
    public short getAddressByte(byte[] addr, short byteNbr) {
    	return Util.getShort(addr, (short)(byteNbr*2));
    }
    
    public short getShortAtByte(byte[] addr, short byteNbr) {
    	return Util.getShort(addr, (short)(byteNbr));
    }
    
    
    //converts XMSS leaf address to WOTS index and vice versa
    //TODO: would be unnecessary if pos on level and height bytes in XMSS Address was swapped, consider doing so
    // UPDATE: bytes were swapped, type needs to be changed nonetheless. Keep function to ease later changes in addresses 
    public void convertAddress(short dummy, byte[] address){
    	
    	short type = getAddressType(address);
    	short byte0 = getAddressByte(address, (short)1);
    	if(type == ADDRESS_XMSS_TREE && byte0 == 0) { //only if leaf-node = byte1 == 0
    		//genAddress(ADDRESS_WOTS, byte0, (short)0, address);
    	}
    	else if(type == ADDRESS_WOTS) {
    		//genAddress(ADDRESS_XMSS_TREE, byte0, (short)0, address);
    	}else {
    		// TODO throw some error
    	}
    	
    }
    
    

    

    


    public void genBitmask(byte address[], short offset, byte[]bitmask){
    	//TODO:Seems the size of address is too small. 
    	// Implement Bitmask creation properly  
    	Util.arrayCopyNonAtomic(address, (short)0, arr_N, (short)0, ADDRESS_SIZE);
    	Util.arrayFillNonAtomic(arr_N, ADDRESS_SIZE, (short)(N-ADDRESS_SIZE), (byte)0);
//    	RandomData randomness = RandomData.getInstance(RandomData.ALG_PSEUDO_RANDOM);
    	randomness.setSeed(arr_N, (short)0, secSeedSize);
    	
        randomness.generateData(bitmask, (short)offset, (short)(N));	
    	
    }
    
    //implemented similar to wots_pkgen in xmss reference:
    private void genSk(byte[] address, byte[] sk) {
    	//MessageDigest secretKey = MessageDigest.getInstance(MessageDigest.ALG_SHA_256, false);
    	//RandomData randomness = RandomData.getInstance(RandomData.ALG_PSEUDO_RANDOM);
    	/*
    	 * The private key elements are computed as sk[i] = PRF(S, toByte(i, 32)) whenever needed.
    	 */
    	get_seed(address, arr_N); //arr_N = seed used for wots_pkgen
    	
    	//expand_seed
    	Util.arrayFillNonAtomic(arr_N2, (short)0, N, (byte)0);
    	for(short s = 0; s<WOTS_LEN; ++s) {
    		ull_to_bytes(s, (short)32, arr_N2);
    		PRF(arr_N, arr_N2, arr_N3);
    		Util.arrayCopyNonAtomic(arr_N3, (short)0, sk, (short)(s*N), N);
    	}
    }
    
    void ull_to_bytes(short in, short outlen, byte[] out)
    {
    	/* Iterate over out in decreasing order, for big-endianness. */
    	for (short i = (short)(outlen - 1); i >= 0; i--) {
    		out[i] = (byte) (in & 0xff);
    		in = (short) (in >> 8);
    	}
    }
    

    // CHAINING FUNCTION
    //computes a chain node pointed to by each element in index on secKey 
    //indices needs to be of length WOTS_LEN
    private void chain(byte[] secKey, byte[] addr_l, short[] indices, byte[] result) {
    	//byte[] input = new byte[N];
    	//Util.arrayCopy(sk,  (short)0, input, (short)0, N); //first N bytes as starting input
    	//byte[] temp = new byte[N];	//intermediate chain node
    	short offset;
    	for(short len = 0; len<WOTS_LEN; len++) {	 	//iterate over len segments
    		offset = (short)(len*N);
    		Util.arrayCopy(secKey, offset, arr_N2, (short)0, N);
    		setOtsChainAddr(addr_l, len);
    		if(indices[len]!=0) {
    			for(short w = 0; w<indices[len]; w++) {	//calc chains hash values
    				setOtsHashAddr(addr_l, w);
    				setKeyAndMask(addr_l, KAM_SET_KEY);
    				secretSeed.getKey(arr_N3, (short)0); //get SEED 
    				PRF(arr_N3, addr_l, arr_N); //result = KEY
    				setKeyAndMask(addr_l, KAM_SET_BITMASK_LEAST);
    				PRF(arr_N3, addr_l, arr_bitmask1);
    				
    		        /* Mask the input */
    		        for (short i = 0; i < ADDRESS_SIZE; i++) {
    		            arr_N2[i] ^= arr_bitmask1[i];
    		        }
    		        F(arr_N, arr_N2, arr_N2); // hash input message directly into output buffer
    		           		        
    				//hashIt(arr_N2, (short)(N*8), arr_N);
    				//Util.arrayCopyNonAtomic(arr_N, (short)0, arr_N2, (short)0, N); //result = next iteration input  			
    			}
    			Util.arrayCopy(arr_N2, (short)0, result, offset, N);//store node in result
    		}
    		else { // in case index==0 -> use sk segment directly
    			Util.arrayCopy(arr_N2, (short)0, result, offset, N);
    		}
    			
    		
    		
    	}
    }


    			
    

    //Function hashes input to output array of length hashLength
    //TODO: add error handling e.g. if output.length != hashLength
    private void hashIt(byte[] input, short hashLength, byte[]output) {
    	
    	 switch(hashLength) { 
         case 256:
        	 //MessageDigest md = MessageDigest.getInstance(MessageDigest.ALG_SHA_256, false);
        	 md256.reset();
        	 md256.update(input, (short)0, (short)input.length);
             md256.doFinal(input, (short)0, (short)input.length, output, (short) 0);
             //Util.arrayCopyNonAtomic(output, (short) 0, input, (short) 0, (short)(hashLength/8));
             md256.reset();
         	break;
         default:
        	 ISOException.throwIt(ISO7816.SW_UNKNOWN);
    	 }
    }
   
    private void sendSignature(APDU apdu, byte[] message) {
    	short maxData = APDU.getOutBlockSize();
        byte[] buffer = apdu.getBuffer();
        Util.arrayFillNonAtomic(buffer, (short) 0, maxData, (byte) 0);
        apdu.setOutgoing();
        short len = (short)message.length;
        apdu.setOutgoingLength(len);
        apdu.sendBytesLong(message, (short)0, len);
//        short NE = apdu.setOutgoing();
//        apdu.setOutgoingLength(NE);
        
        //apdu.setOutgoingAndSend((short) 0, maxData);
    }
    
    

    //generic sending
    private void send_response(APDU apdu, byte[] message, short len) {
    	apdu.setOutgoing();
    	apdu.setOutgoingLength(len);
    	apdu.sendBytesLong(message, (short)0, len);
    }
    
   private byte[] helloTest(APDU apdu, short index){
	   byte hash[] = new byte[N];
	   
	   MessageDigest md = MessageDigest.getInstance(MessageDigest.ALG_SHA_256, false);
	   
	   
	   byte buffer[] = apdu.getBuffer();      
	   short bytesRead = apdu.setIncomingAndReceive();
       short echoOffset = (short) 0;
       
       
       
    
       while (bytesRead > 0) {
           Util.arrayCopyNonAtomic(buffer, ISO7816.OFFSET_CDATA, echoBytes, echoOffset, bytesRead);
           echoOffset += bytesRead;
           bytesRead = apdu.receiveBytes(ISO7816.OFFSET_CDATA);
       }

       apdu.setOutgoing();
       apdu.setOutgoingLength((short) (echoOffset + 5));

       // echo header
       apdu.sendBytes((short) 0, (short) 5);
       // echo data
       apdu.sendBytesLong(echoBytes, (short) 0, echoOffset);
       return hash;
   }
   
	   
}
   
