/* Copyright (c) 1998, 2019, Oracle and/or its affiliates. All rights reserved. */

Steps to run this demo:

1. In a command prompt window run cref 
2. In another window: cd to applet directory 
3. Don't forget to set JC_HOME_SIMULATOR AND ANT_HOME environment variables (see UserGuide for details)
4. Run ant all. This will execute the apdu script.
3. To verify output is correct,compare with the expected output file(<demoname>.expected.out)