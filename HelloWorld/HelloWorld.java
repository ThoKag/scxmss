/**
 * Copyright (c) 1998, 2019, Oracle and/or its affiliates. All rights reserved.
 * 
 */

/*
 */

// /*
// Workfile:@(#)HelloWorld.java	1.7
// Version:1.7
// Date:01/03/06
//
// Archive:  /Products/Europa/samples/com/oracle/javacard/samples/HelloWorld/HelloWorld.java
// Modified:01/03/06 12:13:08
// */
package com.oracle.jcclassic.samples.helloworld;

import javacard.framework.APDU;
import javacard.framework.Applet;
import javacard.framework.ISO7816;
import javacard.framework.ISOException;
import javacard.framework.JCSystem;
import javacard.framework.SystemException;
import javacard.framework.Util;
import javacard.security.AESKey;
import javacard.security.InitializedMessageDigest;
//import javacard.security.AlgorithmParameterSpec;
//import javacard.security.InitializedMessageDigest;
import javacard.security.KeyBuilder;
//import javacard.security.KeyPair;
import javacard.security.MessageDigest;
import javacard.security.RandomData;
import javacard.security.SM4Key;
import javacard.security.SecretKey;
import javacardx.security.derivation.*;
import javacardx.crypto.*;


/*
0x80 0xa0 0x01 0x02 0x06 0x01 0x02 0xfa 0x03 0x04 0x05 0x7F;

0x80 0xA0 0x07 0x00 0x20 0x19 0xAD 0x0A 0x03 0xAD 0x0A 0x92 0x8B 0x00 0x32 0x7A 0x05 0x22 0xAD 0x0B 0x2D 0x10 0x06 0x32 0x7B 0x00 0x31 0x03 0x1A 0x03 0x10 0x06 0x8D 0x00 0x17 0x3B 0x19 0x7F;
 */

public class HelloWorld extends Applet {

    private byte[] echoBytes;
    private static final short LENGTH_ECHO_BYTES = 256;
    private byte[] flags;
    //MOCKUp STUFF
    private final byte[] sk_seed = {0x00, 0x01, 0x02, 0x04, 0x05, 0x06, 0x07, 0x08, 0x09, 
    		0x0a, 0x0b, 0x0c, 0x0d, 0x0e, 0x0f};
    private final static byte GENERATE_PK = (byte) 0xAA;
    private final static byte INIT = (byte) 0x60;
    private final static byte SIGN_MESSAGE = (byte) 0xa0;
    private final static byte DBG_GEN_KEYPAIR = (byte) 0xF0;
    private final static byte DBG_MESSAGE_BASE = (byte) 0xF1;
    private final static byte GENERATE_HASH = (byte)0x50;
    private final AESKey secretSeed;
    private final AESKey secretPRFKey;  /* PRFKEY */
    private final byte[] publicKey;  /* ROOT + PUBSEED */
    /* This implementation assumes H/D <= 15, to simplify the arithmetic */
    public static final short D = 1/*5*/;  /* Number of subtrees */
    /* This implementation assumes H <= 30; indices are two signed shorts */
    public static final short H = 3/*20*/;  /* Height of the full tree */
    /* This implementation assumes N = 32 in various (unexpected?) ways */
    public static final short N = 32;  /* Size of a hash output */
    public static final short W = 4; //Winternitz parameter
    private static final short FLAGS_SIZE = (short)5;
    public static final short WOTS_LEN1 = 64;
    public static final short WOTS_LEN2 = 3;
    public static final short WOTS_LEN = 67;
    private InitializedMessageDigest sha256;
    
    
    public static final short ADDRESS_SIZE = 6;
    public static final short ADDRESS_WOTS = 0;
    public static final short ADDRESS_L_TREE= 1;
    public static final short ADDRESS_XMSS_TREE = 2;

    public short currentIndex;  //current active keypair index
    public short number_of_leaves;
    //Hash stuff
    private MessageDigest md256;
    private Cipher aes;
    private AESKey key;
    private byte[] tmp;
    private RandomData randomData;
    private byte[] seed;
    private short index; //Index in tree
    public short secSeedSize;
    public short secKeySize;
    public short signatureSize;
    /**
     * Only this class's install method should create the applet object.
     */
    protected HelloWorld() {
        echoBytes = new byte[LENGTH_ECHO_BYTES];
        publicKey = new byte[(short)64];
        secretSeed = (AESKey)KeyBuilder.buildKey(KeyBuilder.TYPE_AES, KeyBuilder.LENGTH_AES_256, false);
        secSeedSize = (short)(secretSeed.getSize()/8);
        secKeySize = N*WOTS_LEN;
        signatureSize = (short) (secKeySize+H*N);
        md256 = MessageDigest.getInstance(MessageDigest.ALG_SHA_256, false);
        number_of_leaves = H * H; // TODO: OVERFLOW ALARM
        currentIndex = 0;      //TODO: make transient and update 
        //Mockup:
        seed = new byte[secSeedSize];
        for(short i = 0; i< secSeedSize; i++) {
        	seed[i] = (byte)i;
        }
       
        secretSeed.setKey(seed, (short) 0);
        secretPRFKey = (AESKey)KeyBuilder.buildKey(KeyBuilder.TYPE_AES, KeyBuilder.LENGTH_AES_256, false);
        flags = JCSystem.makeTransientByteArray(FLAGS_SIZE, JCSystem.CLEAR_ON_DESELECT);
        JCSystem.requestObjectDeletion();
        register();
    }

    /**
     * Installs this applet.
     *
     * @param bArray
     *            the array containing installation parameters
     * @param bOffset
     *            the starting offset in bArray
     * @param bLength 
     *            the length in bytes of the parameter data in bArray
     */
    public static void install(byte[] bArray, short bOffset, byte bLength) {
        new HelloWorld();
    }

    /**
     * Processes an incoming APDU.
     *
     * @see APDU
     * @param apdu
     *            the incoming APDU
     * @exception ISOException
     *                with the response bytes per ISO 7816-4
     */
   @Override 
   public void process(APDU apdu) {
        byte buffer[] = apdu.getBuffer(); 
        short len = apdu.setIncomingAndReceive();
        // check SELECT APDU command
        if ((buffer[ISO7816.OFFSET_CLA] == 0) &&
                (buffer[ISO7816.OFFSET_INS] == (byte) (0xA4))) {
            return;
        }
        switch(buffer[ISO7816.OFFSET_INS]) {
        case GENERATE_PK:
        	helloTest(apdu, (short)0);
        	break;
        case INIT:
        	helloTest(apdu, (short)0);
        	break;
        case SIGN_MESSAGE:
        	byte[] signature = new byte[signatureSize];	//stores XMSS Signature
        	sign_message(apdu, signature);				//XMSS Signature generation
        	send_array(signature, (short)0, signatureSize); //send back signature
        	break;
//        case DBG_GEN_KEYPAIR:
//        	byte[] sk = new byte[secKeySize];
//        	byte[] pk = new byte[secKeySize];
//        	byte[] index = new byte[ADDRESS_SIZE];
//        	if(wotsPairAtIDX(buffer[ISO7816.OFFSET_P1], sk, pk)==0) {
//        		send_array(pk, (short)0, (short) (N*WOTS_LEN));
//        		send_array(sk, (short)0, (short) (N*WOTS_LEN));
//        	}
//        	break;
        case DBG_MESSAGE_BASE:
        	short s = apdu.getIncomingLength();
        	byte[] message = new byte[s];
        	Util.arrayCopy(buffer, (short)ISO7816.OFFSET_CDATA, message, (short)0, s);
        	short[] result = new short[WOTS_LEN];
        	baseFunction(message, WOTS_LEN, result);
        	break;
        	
        default:
        	ISOException.throwIt(ISO7816.SW_INS_NOT_SUPPORTED);
        }
        	
     
    }
   
   //Debug Function, generates sk & pk for given index
   //TODO: Maybe use function similar to this to calculate pk for ltree construction. Optimize by not storing intermediate results
    private short wotsPairAtIDX(byte[] address, byte[] sk, byte[] pk){
    	//calculate Seed at index
    	byte[] seed = new byte[secSeedSize];
    	get_seed(address, secretSeed, seed);
    	
    	//generate SK from seed
    	byte[] wotsSK = new byte[secKeySize];
    	genSk(seed, wotsSK);
    	
    	//calculate hash values and PK
    	Util.arrayCopy(wotsSK, (short)0, sk, (short)0, secKeySize);
    	short sklen = (short)(secKeySize*W);
    	byte[] results = new byte[sklen];
    	chain(sk, results, W);
    	
    	//results are stored in format |chainNodes(W*len_n0)|chainNodes(W*len_n1)|...
    	//return results
    	Util.arrayCopy(results, (short)(N*WOTS_LEN*W-N), pk, (short)0, N);
    	
		return 0;
    }
    
    /*
     * CURRENT SIGNATURE SCHEME
     * ------------------------------------------
     * |WOTS signature ("secSeedSize" bytes) | AUTH-Path ("secSeedSize")
     * ------------------------------------------
     */
    
    
    //actual signature generation
    private void sign_message(APDU apdu, byte[] signature){
    	byte[] wots_signature = new byte[secKeySize]; //TODO - maybe directly use signature array (save mem)
    	byte[] pubKey = new byte[N];
    	gen_wots_signature(apdu,wots_signature, pubKey);		//generate signature and compressed pubKey
    	
    	byte[] authPath = new byte[N*H];
    	gen_auth_path(authPath);	// caluclates authentication Path for current Index
    	
    	//TODO: add other required data to signature
    	//For now, this is the structure:
    	// || signature | authPath ||
    	Util.arrayCopyNonAtomic(wots_signature, (short)0, signature, (short)0, secKeySize);
    	Util.arrayCopyNonAtomic(authPath, (short)0, signature, secKeySize, (short)(N*H));
    	
    	
    }

    //calculate nodes at index
    // nodes needs to be of length secKeySize*W
    private short wotsNodesAtIDX(byte[] index, byte[] nodes){
    	//calculate Seed at index
    	byte[] seed = new byte[secSeedSize];
    	get_seed(index, secretSeed, seed);
    	
    	//generate SK from seed
    	byte[] wotsSK = new byte[secKeySize];
    	genSk(seed, wotsSK);
    	
    	//calculate hash values and PK
    	short sklen = (short)(secKeySize*W);
    	byte[] results = new byte[sklen];
    	chain(wotsSK, nodes, W);
    	
    	//results are stored in format |chainNodes(W*len_n0)|chainNodes(W*len_n1)|...
		return 0;
    }
    
    private void get_seed(byte[] index, AESKey secretSeed, byte[] result){
    	short test = KeyBuilder.LENGTH_AES_256;
    	test = (short)256; //Debugger shows wrong values????
    	//byte[] input = new byte[KeyBuilder.LENGTH_AES_256+1];
    	byte[] input = new byte[(short)(secSeedSize+index.length)];
    	//byte[] buffer = new byte[secretSeed.getSize()];
    	byte[] buffer = new byte[secSeedSize];
    	secretSeed.getKey(buffer, (short)0);
    	
    	Util.arrayCopy(buffer, (short)0, input, (short)0, secSeedSize);
    	Util.arrayCopy(index, (short)0, input,(short)secSeedSize, (short)index.length);
    	hashIt(input, secretSeed.getSize(), result);
    }
    
  //..............0
  //........../        \
  //.........1         __
  //......./  \       /   \
  //......__   4     5     6
  //...../ \  / \   / \   / \
  //....7  8 __  X 11 12 13 14
  //.............^
    //calculates the authentication path necessary to validate WOTS at the current index
    public void gen_auth_path(byte[] auth_path) {
    	/*
    	 * Calculate addresses of nodes needed for authentication path 
    	 */
    	byte[] auth_node_addr = new byte[H*ADDRESS_SIZE];
    	
    	
    	if (currentIndex % 2 == 0 ) { //currentIndex = right leaf
    		
    	}else {	//currentIndex = left leaf
    		
    	}
    	//IDEA: start from root, left and right until at leaf
    	
    	
    	short currentLevel = H;	// level inside XMSS Tree that is currently handled, starting at root with height H
    	short middleAtLevel = (short)(number_of_leaves/2); //all leaves < middleAtLevel children of left branch
    	byte[] parent = new byte[ADDRESS_SIZE];
    	byte[] left_C = new byte[ADDRESS_SIZE];
    	byte[] right_C = new byte[ADDRESS_SIZE];
    	genAddress(ADDRESS_XMSS_TREE, (short)0, H, parent); // start at root
    	Util.arrayCopyNonAtomic(right_C, (short)0, auth_path, (short)(H*ADDRESS_SIZE-1-4), ADDRESS_SIZE); //store root at the end of authPath
    	short loopCount = 0;
    	while(currentLevel !=0) {
    		middleAtLevel = (short) (middleAtLevel /2);
    		getChildren(parent, left_C, right_C);
    		if (currentIndex < middleAtLevel) { // leaf to the left, store right sibling in authPath
    			//store sibling on authentication path, starting from the end
    			Util.arrayCopyNonAtomic(right_C, (short)0, auth_path, (short)((H*ADDRESS_SIZE-1)-4*H*loopCount), ADDRESS_SIZE); 
    			Util.arrayCopyNonAtomic(left_C, (short)0, parent, (short)0, ADDRESS_SIZE); //next node on path to target leaf is new parent
    		}else { 
    			//store sibling on authentication path, starting from the end
    			Util.arrayCopyNonAtomic(left_C, (short)0, auth_path, (short)((H*ADDRESS_SIZE-1)-4*H*loopCount), ADDRESS_SIZE);
    			Util.arrayCopyNonAtomic(right_C, (short)0, parent, (short)0, ADDRESS_SIZE); //next node on path to target leaf is new parent
    		}
    		loopCount++;
    		currentLevel--;
    	}//auth_node_addr now contains all authPath node addresses, each of length ADDRESS_SIZE, starting at lowest tree level to root
    	
    	/*
    	 * Treehash algorithm calculates value for the specified address
    	 */
    	byte[] current_node_address = new byte[ADDRESS_SIZE];
    	byte[] calculated_value = new byte[N];
    	for(short s = 0; s < H; ++s) {
    		Util.arrayCopyNonAtomic(auth_node_addr, (short)(s*ADDRESS_SIZE), current_node_address, (short)0, ADDRESS_SIZE);
    		treehash(current_node_address, calculated_value);
    		Util.arrayCopyNonAtomic(calculated_value, (short)0, auth_path, (short)(s*N), N);
    	}

    }
    
  //..............0								 0							 1
  //........../        \					/         \				    /         \
  //.........1          2				    0	  	    1			   2	  	    3
  //......./  \       /   \			      /  \		  /   \			 /  \		  /   \
  //......3    4     5     6			 0    1      2     3	    4    5      6     7
  //...../ \  / \   / \   / \			/ \  / \    / \   / \	   / \  / \    / \   / \
  //....7  8  9 10 11 12 13 14		   0  1  2  3  4  5  6   7	  8  9 10  11 12 13 14  15
      
      //calculate value at tree node at xmss_address - pretty stupid version without any optimization 
    //xmss_address = valid xmss address generated by genAddress(ADDRESS_XMSS_TREE,...)
    //value = N-byte array 
    public void treehash(byte[] xmss_address, byte[]value){
    	// calculate all xmss_nodes underneath xmss_address
    	
    	short index = getAddressByte(xmss_address, (short)1);
    	short height = getAddressByte(xmss_address, (short)2);
    	//short [] indices_wots_needed = new short[height*height]; //height*height == number of leaves underneath xmss_address
    	short rangeLow = (short) (index*(2^height));		// index of lowest WOTS key needed
    	short rangeHigh = (short) (rangeLow+height*height); // index of highest WOTS key needed
    	byte [] workingArray = new byte[(short)(height*height * N)]; // stores the computed values during computation 	
    	
    	//initially fill working Array with all wots pk keys needed
    	byte[] tempAddress = new byte[ADDRESS_SIZE];
    	byte[] tempResult = new byte[N];
    	for(short wotsIndex = rangeLow, loopCount = 0;wotsIndex<=rangeHigh;++wotsIndex, ++loopCount) {
    		genAddress(ADDRESS_WOTS, wotsIndex, (short)0, tempAddress);
    		get_xmss_leaf(tempAddress, tempResult);
    		Util.arrayCopyNonAtomic(tempResult, (short)0, workingArray, (short) (loopCount*N), N);
    	}
    	
    	//workingArray is now filled with all necessary compressed WOTS pubKeys
    	short current_num_nodes = (short) (rangeHigh-rangeLow); // number of currently relevant nodes
    	byte[] addr_left = new byte[ADDRESS_SIZE];
    	byte[] addr_right = new byte[ADDRESS_SIZE];
    	byte[] node_left = new byte[N];
    	byte[] node_right = new byte[N];
    	for(short xmss_height = 0; xmss_height < height; ++xmss_height) { //iterate over the height of the subtree, bottom to top
    		for(short xmss_index =0; xmss_index < current_num_nodes; xmss_index+=2) { //iterate over currently relevant nodes
    			genAddress(ADDRESS_XMSS_TREE, xmss_index, xmss_height, addr_left);						//left address
    			genAddress(ADDRESS_XMSS_TREE, (short) (xmss_index+1), xmss_height, addr_right);			//right address
    			Util.arrayCopyNonAtomic(workingArray, (short) (xmss_index*N), node_left, (short)0, N);		//left node value
    			Util.arrayCopyNonAtomic(workingArray, (short) ((xmss_index+1)*N), node_right, (short)0, N);	//right node value
    			hashNodes(addr_left, addr_right, node_left, node_right, tempResult);
    			Util.arrayCopyNonAtomic(tempResult, (short)0, workingArray, (short)(xmss_index/2), N);//collect results at first half of tempResult		
    		}
    		//the first half of tempResult now contains all relevant node values
    		current_num_nodes =(short) (current_num_nodes/2); 
    		}
    	Util.arrayCopyNonAtomic(workingArray, (short)0, value, (short)0, N);

    }
    
    
    //addr and output need all to be of length N
    //TODO: Test: slower than doing all in place inside inner loop of treehash? 
    public void hashNodes(byte[] addr1, byte[] addr2, byte[] node1, byte[] node2, byte[]output) {
    	
    	//gen and apply bitmasks
    	byte[] bitmask1 = new byte[N];
    	byte[] bitmask2 = new byte[N];
    	genBitmask(addr1, bitmask1); //gen bitmask for parent
    	genBitmask(addr2, bitmask2); //gen bitmask for parent
    	for(short s= 0; s<N; ++s) {
    		node1[s] = (byte) (node1[s] | bitmask1[s]);
    		node2[s] = (byte) (node2[s] | bitmask2[s]);
    	}

    	//concat masked arrays
    	byte[] concat = new byte[(short)(2*N)];
    	Util.arrayCopyNonAtomic(node1, (short)0, concat, (short)0, N);
    	Util.arrayCopyNonAtomic(node2, (short)0, concat, N, N); 
    	
    	//hash it
    	hashIt(concat, (short)(N*8), output);
    }
    
    /*
     * calculates the L-tree root of the specified WOTS-Address 
     */
    public void get_xmss_leaf(byte[] WOTS_address, byte[]ltree_root) {
    	
    	
    	//calculate Seed at index
    	byte[] seed = new byte[secSeedSize];
    	get_seed(WOTS_address, secretSeed, seed);
    	
    	//generate SK from seed
    	byte[] wotsSK = new byte[secKeySize];
    	genSk(seed, wotsSK);
    	
    	//calculate hash values and PK
    	short sklen = (short)(secKeySize*W);
    	byte[] results = new byte[sklen];
    	chain(wotsSK, results, W);
    	
    	//results are stored in format |chainNodes(W*len_n0)|chainNodes(W*len_n1)|...
    	//extract pk and compress it using L-Tree
        byte[] uncompressed_pk = new byte[secKeySize];
        for(short s = 0; s < secKeySize; ++s) {
        	uncompressed_pk[s] = wotsSK[(short)(s*W*N)]; //extract pk nodes
        }
        byte[] Ltree_addr = new byte[ADDRESS_SIZE];
        genAddress(ADDRESS_L_TREE, currentIndex, (short)0, Ltree_addr);
        gen_ltree(Ltree_addr, uncompressed_pk, ltree_root);
    	
    }
    
    /*
     * Calculates all nodes underneath the specified address and stores them in result.
     * address needs to be a valid XMSS Address, result needs to be of 2^(address.height+1)-1 * ADDRESS_SIZE bytes length
     * The nodes found can then be used to check weather they where already computed and have transiently stored results
     */
    public void nodes_underneath(byte[] address, byte[]result) {
     //TODO: Really necessary? Nodes should be computable from address alone, no need for expensive arraycopys etc.	
    }
    
    
    public void getChildren(byte[] parentAddr, byte[]leftChild, byte[] rightChild) {
    	
    	short type = getAddressType(parentAddr);
    	short levelIndex = getAddressByte(parentAddr, (short)1);
    	short parentHeight = getAddressByte(parentAddr, (short)2);
    	
    	genAddress(ADDRESS_XMSS_TREE, (short)(levelIndex*2), (short)(parentHeight-1), leftChild);
    	genAddress(ADDRESS_XMSS_TREE, (short)(levelIndex*2+1), (short)(parentHeight-1), rightChild);
    }
    /*
     * Simple Tree traversal algorithm.
     * Calculates all other pubKeys and nodes from scratch every time.
     * 
     */
    public void tt_simple(){
    	
    }
    
    
    /*generates WOTS signature of transmitted message for WOTS Keypair at currentIndex
     *wots_signature must be secKeySize bytes long
     *pubKey must be N bytes long
     *
     *      TODO: 	In standard Java the costs of if cases with static conditions are negligible due to trace caches.
     *      		If this is the case for JC as well, use gen_wots_signature for treehash as well and omit unnecessary copy instructions et      c.
     */
        
    public void gen_wots_signature(APDU apdu, byte[] wots_signature, byte[] pubKey) {
    	byte buffer[] = apdu.getBuffer(); 	
    	short len = apdu.getIncomingLength();
    	byte[] message = new byte[len];
    	Util.arrayCopy(buffer, (short)ISO7816.OFFSET_CDATA, message, (short)0, len);	// message = actual message to sign
    	short[] bases = new short[WOTS_LEN];	//stores calculated bases for message 
    	baseFunction(message, WOTS_LEN, bases);			//bases now contains WOTS_LEN short values
    	
    	byte[] sk = new byte[secKeySize];		//
        byte[] wots_nodes = new byte[(short)(secKeySize*W)];	//stores all nodes and private key
        byte[] wots_Index = new byte[ADDRESS_SIZE];				//stores currently used WOTS-Signature
        genAddress(ADDRESS_WOTS, currentIndex, (short)0, wots_Index);
        
        //calculate all chain nodes and pk at the current WOTS-Index
        wotsNodesAtIDX(wots_Index, wots_nodes);	//results are stored in format |chainNodes(W*len_n0)|chainNodes(W*len_n1)|...
        											//=> pk is fragmemted with a offset of W*N within array
        
        //extract the chain nodes specified at bases from nodes and store them in wots_signature
        nodesFromHashes(wots_nodes, bases, wots_signature);
    	// wots_signature now contains the WOTS signature

        //extract pk and compress it using L-Tree
        byte[] uncompressed_pk = new byte[secKeySize];
        for(short s = 0; s<WOTS_LEN; ++s) {
        	bases[s] = (short)(W-1); //repurpose bases to now store pk locations
        }
        nodesFromHashes(wots_nodes, bases, uncompressed_pk); // extract pk from nodes to uncompressed_pk
        
        byte[] Ltree_addr = new byte[ADDRESS_SIZE];
        genAddress(ADDRESS_L_TREE, currentIndex, (short)0, Ltree_addr);
        gen_ltree(Ltree_addr, uncompressed_pk, pubKey);
    }
    
     //L-Tree function
    //compress wots pk from (WOTS_LEN*N) to N bytes
    public void gen_ltree(byte[] addr ,byte[] uncompressed_pk, byte[] result){
        // generate Bitmask
    	byte[] bitmask = new byte[N];
    	genBitmask(addr, bitmask); //gen single bitmask that is used in the whole l_tree
    	
    	//TODO maybe directly work on existing arrays
    	byte[] toHash = new byte[(short)(2*N)]; // stores input to hash function
		byte[] hashResult = new byte[N];		// stores hash function return value
    	
    	short current_compressed_len = secKeySize; //N*WOTS_LEN in bytes -> reduced to N bytes during iteration of while loop
    	if((current_compressed_len/N)%2!=0) {
    		current_compressed_len +=N;	//add 0s to end to even out 
    	}
    	short current_wots_segments = WOTS_LEN;	//current number of segments
    	if(current_wots_segments % 2 != 0) {
    		current_wots_segments +=1;
    	}
    	
		byte[] tmp_result = new byte[current_compressed_len]; 
		Util.arrayCopyNonAtomic(uncompressed_pk, (short)0, tmp_result, (short)0, secKeySize);
		Util.arrayFillNonAtomic(tmp_result, secKeySize, (short)(current_compressed_len-secKeySize), (byte)0); //zero out padding
		
    	while(current_wots_segments>1){ // current_wots_segments == 1 means the compressed key is of length N
    		
    		//bitmask on current working buffer
    		for(short segment_counter = 0; segment_counter < current_wots_segments; ++segment_counter) {//only apply bitmask on the still relevant bytes
    			for(short bitmask_index = 0; bitmask_index < N; ++bitmask_index) { 
    				tmp_result[(short)(segment_counter*bitmask_index)] = (byte) (tmp_result[(short)(segment_counter* bitmask_index)] | bitmask[(short)(bitmask_index)]);
    			}
    		}
    		
    		//iterate in N*2 size steps over buffer and hash them to a single output
    		

    		//hash 2 N byte segments into one N byte segment and copy hashes into result.
    		//after this loop, the relevant bytes in result are halved 
    		for(short pair_counter = 0; pair_counter < current_wots_segments/2; ++pair_counter) {
    			Util.arrayCopyNonAtomic(tmp_result, (short)(pair_counter*2*N), toHash, (short)0, (short)(N*2));
    			hashIt(toHash, (short)(N*8), hashResult);
    			Util.arrayCopyNonAtomic(hashResult, (short)0, tmp_result, pair_counter, N);
    		}
    		current_compressed_len = (short) (current_compressed_len/2);
    		current_wots_segments = (short) (current_compressed_len/N);
    	}
    	Util.arrayCopyNonAtomic(tmp_result, (short)0, result, (short)0, N);
    }
    

    

    
    //divides message into len segments and maps them into range [0..W]
    //TODO!!!: implement checksum
    /*
    public void baseFunction(byte[] message, short[] result) {
    	

    	short bytes_left = (short)(message.length%WOTS_LEN); // DEBUG VARIABLE
    	byte [] message_padded = new byte[(short)(message.length + message.length%WOTS_LEN)];
    	Util.arrayCopy(message, (short)0, message_padded, (short)0, (short)message.length);
    	short segment_length = (short)(message_padded.length/WOTS_LEN);
    	
    	short sum = 0;
    	short char_counter = 0;
    	for(short len_counter = 0; len_counter<WOTS_LEN; ++len_counter) {

    		for(char_counter = 0; char_counter<WOTS_LEN-2; char_counter+=2) {
    			sum += Util.getShort(message_padded, (short)(len_counter*segment_length + char_counter));
    		}
    		if(char_counter!=segment_length) { //uneven segments leave one byte left
    			byte[] last = {message_padded[(short)(len_counter*segment_length + char_counter+1)], 0x00};
    			sum += Util.getShort(last, (short)0);
    		}

    		result[len_counter] = (short)(sum%W);
    		sum = 0;
    	}
    }
    */	
    
   /* Converts msg to baseW and writes results to lengths array, for the
   purpose of creating WOTS chains. */
    
    public static final short WOTS_LOG_W = 4;
    public static final short WOTS_W = 16;
    private void baseFunction(byte[] message, short msgLength, short[] result) {
    	short in = 0;
    	short out = 0;
    	byte total = 0;
    	short bits = 0;
    	short consumed;

    	for (consumed = 0; consumed < msgLength; consumed++) {
    		if (bits == 0) {
    			if(in < message.length) {
    				total = message[(short)in];
    			}else {
    				total = 0;
    				
    			}
    			in++;
    			bits += 8;
    		}
    		bits -= WOTS_LOG_W;
    		result[(short)out] = (short)((total >>> bits) & (WOTS_W - 1));
    		out++;
    	}
    }
    
    
    //maybe abandon use separate function 
    // gets hashes pointed at inside "bases" from "wots_nodes" 
    public void nodesFromHashes(byte[]wots_nodes, short[] bases, byte[] hashes){
        for(short i = 0; i<WOTS_LEN; ++i){
            //W*N = distance to next segment
        	short test = (short)(N*bases[i]+i*W*N);
            Util.arrayCopy(wots_nodes,(short)(N*bases[i]+i*W*N), hashes, (short)(i*N) , N);
        }
    }
    
    // generate address, arguments depend on type
    // Avoid modifying or handling addresses directly and without using the functions below!
    // This should prevent bugs if the address scheme is modified later on
    public void genAddress(short type, short byte1, short byte2, byte[]addr) {
    	/* WOTS							L-tree						Hash-Tree
     
        +-------------------------+	+-------------------------+	+-------------------------+  
        | layer address  (32 bits)|	| layer address  (32 bits)|	| layer address  (32 bits)| 	height of tree
        +-------------------------+ +-------------------------+ +-------------------------+	  
        | tree address   (64 bits)| | tree address   (64 bits)| | tree address   (64 bits)|		position within layer of MT
        +-------------------------+ +-------------------------+ +-------------------------+
        | type = 0       (32 bits)|	| type = 1       (32 bits)| | type = 2       (32 bits)|		type: 0=wots 1=l-tree 2=hash
        +-------------------------+ +-------------------------+ +-------------------------+
        | OTS address    (32 bits)| | L-tree address (32 bits)|	| Padding = 0    (32 bits)|		OTS: index within tree  
        +-------------------------+ +-------------------------+ +-------------------------+
        | chain address  (32 bits)|	| tree height    (32 bits)|	| tree height    (32 bits)|		
        +-------------------------+ +-------------------------+ +-------------------------+
        | hash address   (32 bits)| | tree index     (32 bits)| | tree index     (32 bits)|		
        +-------------------------+ +-------------------------+ +-------------------------+
        | keyAndMask     (32 bits)| | keyAndMask     (32 bits)| | keyAndMask     (32 bits)|		
        +-------------------------+ +-------------------------+ +-------------------------+
    	
    	
		own temporary scheme:
		WOTS						XMSS
		+-------------------------+	+-------------------------+	
		|type of addr	(2 byte)  |	| type of addr (2 byte)   | 	type of address - 0 = wots, 1=ltree, 2=hash
		+-------------------------+	+-------------------------+	
		|node / wots number		  |	| pos on level (2 byte)   |	   	wots index, xmss index on tree level
		+-------------------------+	+-------------------------+	
		|						  | | height (2 byte)		  |   	XMSS: index on level indicated at height
		+-------------------------+ +-------------------------+
    	*/
    	
    	//type
    	addr[0] = (byte)((type >> 8) & 0xff);
    	addr[1] = (byte)(type & 0xff);
    	
    	//nodeNbr
    	addr[2] = (byte)((byte1 >> 8) & 0xff);
    	addr[3] = (byte)(byte1 & 0xff);
    	
    	addr[4] = (byte)((byte2 >> 8) & 0xff);
    	addr[5] = (byte)(byte2 & 0xff);
    	
    	
    }
    
    public short getAddressType(byte[] addr) {
    	return Util.getShort(addr, (short)0);	
    }
    
    public short getAddressByte(byte[] addr, short byteNbr) {
    	return Util.getShort(addr, (short)(byteNbr*2));
    }
    
    
    //converts XMSS leaf address to WOTS index and vice versa
    //TODO: would be unnecessary if pos on level and height bytes in XMSS Address was swapped, consider doing so
    // UPDATE: bytes were swapped, type needs to be changed nonetheless. Keep function to ease later changes in addresses 
    public void convertAddress(byte[] address){
    	
    	short type = getAddressType(address);
    	short byte0 = getAddressByte(address, (short)1);
    	if(type == ADDRESS_XMSS_TREE && byte0 == 0) { //only if leaf-node = byte1 == 0
    		genAddress(ADDRESS_WOTS, byte0, (short)0, address);
    	}
    	else if(type == ADDRESS_WOTS) {
    		genAddress(ADDRESS_XMSS_TREE, byte0, (short)0, address);
    	}else {
    		// TODO throw some error
    	}
    	
    }
    
    

    

    

    
    public void genBitmask(byte address[], byte[]bitmask){
    	//TODO:Seems the size of address is too small. 
    	// Implement Bitmask creation properly
    	byte[] whyIsThisNecessary = new byte[N];  
    	Util.arrayCopyNonAtomic(address, (short)0, whyIsThisNecessary, (short)0, ADDRESS_SIZE);
    	Util.arrayFillNonAtomic(whyIsThisNecessary, ADDRESS_SIZE, (short)(N-ADDRESS_SIZE), (byte) 0xaa);
    	RandomData randomness = RandomData.getInstance(RandomData.ALG_PSEUDO_RANDOM);
    	randomness.setSeed(whyIsThisNecessary, (short)0, secSeedSize);
    	if(getAddressType(address) == 0){ //wots	array bitmask needs to be of len N*WOTS_LEN
        	// buffer, offset, length
        	byte[] wotsSK = new byte[N];
        	randomness.generateData(bitmask, (short)0, (short)(N*WOTS_LEN));	
    	}
    	if(getAddressType(address) == 1) { //l_tree  array bitmask needs to be of len N
	    	// buffer, offset, length
	    	byte[] wotsSK = new byte[N];
	    	randomness.generateData(bitmask, (short)0, (short)(N));
    	}
    }
    
    
    private void genSk(byte[] seed, byte[] sk) {
    	MessageDigest secretKey = MessageDigest.getInstance(MessageDigest.ALG_SHA_256, false);
    	RandomData randomness = RandomData.getInstance(RandomData.ALG_PSEUDO_RANDOM);
    	// buffer, offset, length
    	randomness.setSeed(seed, (short)0, secSeedSize);
    	byte[] wotsSK = new byte[N];
    	randomness.generateData(sk, (short)0, (short)(N*WOTS_LEN));
    }
    
    // CHAINING FUNCTION
    //computes chains number of hash calls an stores values in result
    //result has to be of length N*WOTS_LEN*W
    private void chain(byte[] sk, byte[]result , short chains) {
    	byte[] input = new byte[N];
    	//Util.arrayCopy(sk,  (short)0, input, (short)0, N); //first N bytes as starting input
    	byte[] temp = new byte[N];	//intermediate chain node
    	for(short len = 0; len<WOTS_LEN; len++) {	 	//iterate over len segments
    		short offset = (short)(len*N);
    		Util.arrayCopy(sk, (short)(len*N), input, (short)0, N);
    		for(short w = 0; w<chains; w++) {	//calc chains hash values 
    			hashIt(input, (short)(N*8), temp);
    			Util.arrayCopy(temp, (short)0, result, (short)(w*len*N), N); //store nodes in result
    			Util.arrayCopy(temp, (short)0, input, (short)0, N); //store nodes in result
    			Util.arrayFill(temp, (short)0, N, (byte)0);
    		}
    		Util.arrayFill(input, (short)0, N, (byte)0);
    		
    	}
    }
    
    public static void send_array(byte[] array, short offset, short len) {
    	// get buffer
    	APDU apdu = APDU.getCurrentAPDU();
    	// This method is failsafe.
    	if ((short)(offset + len) > (short)array.length)
    		len = (short) (array.length - offset);
    	// Copy data
    	Util.arrayCopyNonAtomic(array, offset, apdu.getBuffer(), (short)0, len);
    	// Check if setOutgoing() has already been called
    	if (apdu.getCurrentState() == APDU.STATE_OUTGOING) {
    		apdu.setOutgoingLength(len);
    		apdu.sendBytes((short)0, len);
    	} else {
    		apdu.setOutgoingAndSend((short)0, len);
    	}
    	// Exit normal code flow
    	ISOException.throwIt(ISO7816.SW_NO_ERROR);
    }
    
    //Function hashes input to output array of length hashLength
    //TODO: add error handling e.g. if output.length != hashLength
    private void hashIt(byte[] input, short hashLength, byte[]output) {
    	
    	 switch(hashLength) { 
         case 256:
        	 //MessageDigest md = MessageDigest.getInstance(MessageDigest.ALG_SHA_256, false);
        	 md256.reset();
        	 md256.update(input, (short)0, (short)input.length);
             md256.doFinal(input, (short)0, (short)input.length, output, (short) 0);
             Util.arrayCopyNonAtomic(output, (short) 0, input, (short) 0, (short)(hashLength/8));
             md256.reset();
         	break;
         default:
        	 ISOException.throwIt(ISO7816.SW_UNKNOWN);
    	 }
    }
   
    
   private byte[] helloTest(APDU apdu, short index){
	   byte hash[] = new byte[N];
	   
	   MessageDigest md = MessageDigest.getInstance(MessageDigest.ALG_SHA_256, false);
	   
	   
	   byte buffer[] = apdu.getBuffer();      
	   short bytesRead = apdu.setIncomingAndReceive();
       short echoOffset = (short) 0;
       
       
       
    
       while (bytesRead > 0) {
           Util.arrayCopyNonAtomic(buffer, ISO7816.OFFSET_CDATA, echoBytes, echoOffset, bytesRead);
           echoOffset += bytesRead;
           bytesRead = apdu.receiveBytes(ISO7816.OFFSET_CDATA);
       }

       apdu.setOutgoing();
       apdu.setOutgoingLength((short) (echoOffset + 5));

       // echo header
       apdu.sendBytes((short) 0, (short) 5);
       // echo data
       apdu.sendBytesLong(echoBytes, (short) 0, echoOffset);
       return hash;
   }
   
	   
}
   
