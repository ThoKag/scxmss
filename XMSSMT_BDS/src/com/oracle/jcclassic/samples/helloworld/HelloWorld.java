package com.oracle.jcclassic.samples.xmssmtbds;

import javacard.framework.APDU;
import javacard.framework.Applet;
import javacard.framework.ISO7816;
import javacard.framework.ISOException;
import javacard.framework.JCSystem;
import javacard.framework.SystemException;
import javacard.framework.Util;
import javacard.security.AESKey;
import javacard.security.InitializedMessageDigest;
//import javacard.security.AlgorithmParameterSpec;
//import javacard.security.InitializedMessageDigest;
import javacard.security.KeyBuilder;
//import javacard.security.KeyPair;
import javacard.security.MessageDigest;
import javacard.security.RandomData;
//import javacard.security.SM4Key;
import javacard.security.SecretKey;
//import javacardx.security.derivation.*;
import javacardx.crypto.*;
import javacardx.apdu.ExtendedLength;



//GENERAL TODOS:
/*
TODO: Make arrays/object transient where possible/
		Some Arrays are too large for the simulator. 
		-Problem on real card as well?
		-Optimization? 

TODO: Implement transactions
TODO: Multi-Tree

*/
/*
0x80 0xa0 0x01 0x02 0x06 0x01 0x02 0xfa 0x03 0x04 0x05 0x7F;

0x80 0xA0 0x07 0x00 0x20 0x19 0xAD 0x0A 0x03 0xAD 0x0A 0x92 0x8B 0x00 0x32 0x7A 0x05 0x22 0xAD 0x0B 0x2D 0x10 0x06 0x32 0x7B 0x00 0x31 0x03 0x1A 0x03 0x10 0x06 0x8D 0x00 0x17 0x3B 0x19 0x7F;
0x80 0xB0 0x07 0x00 0x02 0x7f 0xff 0x7F;

80 A0 07 00 20 19 AD 0A 03 AD 0A 92 8B 00 32 7A 05 22 AD 0B 2D 10 06 32 7B 00 31 03 1A 03 10 06 8D 00 17 3B 19 7F
 */

public class xmssmtbds extends Applet implements ExtendedLength{

    private byte[] echoBytes;
    private static final short LENGTH_ECHO_BYTES = 256;
    private byte[] flags;
    //MOCKUp STUFF
//    private final byte[] sk_seed = {0x00, 0x01, 0x02, 0x04, 0x05, 0x06, 0x07, 0x08, 0x09, 
//    		0x0a, 0x0b, 0x0c, 0x0d, 0x0e, 0x0f};
    private final static byte GENERATE_PK = (byte) 0xAA;
    private final static byte SET_AND_GEN_PK = (byte) 0xAB;
    private final static byte BENCHMARK_HASH = (byte) 0x70;
    private final static byte BENCHMARK_HASH_EEPROM = (byte) 0x71;
    private final static byte BENCHMARK_COPY_RAM = (byte) 0x72;
    private final static byte BENCHMARK_COPY_EEPROM = (byte) 0x73;
    private final static byte BENCHMARK_COPY_RAM_to_EEPROM = (byte) 0x74;
    private final static byte BENCHMARK_COPY_EEPROM_to_RAM = (byte) 0x75;
    private final static byte BENCHMARK_HASH_RAM_to_EEPROM = (byte) 0x76;
    private final static byte BENCHMARK_HASH_EEPROM_to_RAM = (byte) 0x77;
    private final static byte TEST_GEN_SK = (byte) 0x80;
    private final static byte TEST_WOTS_SIGN = (byte) 0x81;
    private final static byte TEST_WOTS_SIGN_OVERHEAD = (byte) 0x91;
    private final static byte TEST_GET_XMSS_LEAF = (byte) 0x82;
    private final static byte TEST_CHAIN = (byte) 0x83;
    private final static byte TEST_GEN_LTREE = (byte) 0x84;
    private final static byte GET_RESPONSE = (byte) 0xA2;
    private final static byte INIT = (byte) 0x60;
    private final static byte SIGN_MESSAGE = (byte) 0xa0;
    private final static byte SIGN_MESSAGE_short = (byte) 0xa1;
    private final static byte DBG_GEN_KEYPAIR = (byte) 0xF0;
    private final static byte DBG_MESSAGE_BASE = (byte) 0xF1;
    private final static byte PREPARE_NEXT_AUTH_PATH = (byte) 0xF2;
    private final static byte GENERATE_HASH = (byte)0x50;
    //private final AESKey secretSeed;
    //private final AESKey secretPRFKey;  /* PRFKEY */
    private final byte[] publicKey;  /* ROOT + PUBSEED */
    /* This implementation assumes H/D <= 15, to simplify the arithmetic */
    public static final short D = 4/*5*/;  /* Number of subtrees */
    /* This implementation assumes H <= 30; indices are two signed shorts */
    public static final short H = 6 ;
    /* This implementation assumes N = 32 in various (unexpected?) ways */
    public static final short N = 32;  /* Size of a hash output */
    public static final short H_full_tree = H*D;
    
//	public static final short WOTS_W = 16; //Winternitz parameter
//  public static final short WOTS_LOG_W = 4; 
//  public static final short WOTS_LEN1 = 64;
//  public static final short WOTS_LEN2 = 3;
    public static final short WOTS_W = 4; //Winternitz parameter
    public static final short WOTS_LOG_W = 2; 
    public static final short WOTS_LEN1 = (short)(8 * N / WOTS_LOG_W);
    public static final short WOTS_LEN2 = 5;
    
    public static final short WOTS_LEN = WOTS_LEN1+WOTS_LEN2;
    private InitializedMessageDigest sha256;
    public static final short single_cluster_array =23;
    public short secKeySize = N*WOTS_LEN;
    short indexBytes = (short)((H_full_tree + 7) / 8);
    
    
    
    public static final short ADDRESS_SIZE = 32;
    public static final short ADDRESS_WOTS = 0;
    public static final short ADDRESS_L_TREE= 1;
    public static final short ADDRESS_XMSS_TREE = 2;
    
    public static final short KAM_SET_KEY = 0;
    public static final short KAM_SET_BITMASK_LEAST = 1;
    public static final short KAM_SET_BITMASK_MOST = 2;
    public static final short MAX_MESSAGE_LENGTH = 480;

    //stuff because javacard is stupid:
    public static final short s0 = 0;
    public static final byte b0 = 0; 
    
    public short currentIndex;  //current active keypair index
    public short currentLayerAddressHigh;
    public short currentLayerAddressLow;
    public short currentTreeAddress;
    
    public short number_of_leaves;
    //Hash stuff
    private MessageDigest md256;
    private Cipher aes;
    private AESKey key;
    
    private RandomData randomData;
    //private byte[] SEED;
    private byte[] PUB_SEED;
    private byte[] sk_seed;
    private byte[] sk_prf;
    private short index; //Index in tree
    public short secSeedSize;
    public short signatureSize;
    RandomData randomness;
    private short prepared_authPath; //stores the index to which the authentication path is currently stored
    
    //Utility arrays
    //BDS********************************************
//    private byte[] state; 
//    private byte[] treehash_inst;
//    private byte[] sk;
//    private short sk_offset;
    private short params_bds_k = 6;
    private short params_tree_height = H;

	private byte[]treehash;
	
	
	short num_treehash_instances = (short)(H-params_bds_k);  //number of treehash instances per bds state
	short treehash_instance_length = 48;		// size of one bds treehash instance
	short states_offset = (short)(treehash_instance_length*num_treehash_instances);	//offset between bds states on treehash array
	short treehash_node_offset=16;


	

//	private byte[] bds_state_stack;			//160 bytes aktuell, (H+1)*N bytes konkret, verwende hier arr_stack
//	private short  bds_state_stackoffset;		//4 byte
//	private byte[] bds_state_stacklevels;		//?, vermutlich heights 
//	private byte[] bds_state_auth;			//128 bytes aktuell, N*H bytes konkret
//	private byte[] bds_state_keep;			//32 bytes? 
//	//treehash_inst *treehash;		//? bytes, wird in ref als state.treehash = treehash_inst definiert, sollte also auf JC geschachtelt werden
//	private byte[] bds_state_retain;			//? bytes
//	private short bds_state_next_leaf;			//4 bytes, erst mal als short
		
    //BDS states in single array for MT***********************************
	private short num_bds_states = (short)(2*D - 1);
	private byte[] bds_states;
	//private byte[] arr_SubtreeRoots;
	private byte [] wots_sigs;
	private short bds_off_stackOff = 0;
	private short bds_off_next_leaf = 4;
	private short bds_off_stacklvls = 8;
	private short bds_off_stack = (short)(bds_off_stacklvls+(H+1)*2); 
	private short bds_off_auth = (short)(bds_off_stack+(H+1)*N);
	private short bds_off_keep = (short)(bds_off_auth + (H*N));
	private short bds_off_retain = (short)(bds_off_keep+((H >> 1)*N));
	private short bds_size = (short)(bds_off_retain+(((1<<params_bds_k)-params_bds_k-1)*N));
	private byte[] bds_swap;
	private void bds_set_stackoffset(short state, short value) {
		Util.setShort(bds_states, (short)(state*bds_size+bds_off_stackOff+2), value);
	}
	private short bds_get_stackoffset(short state) {
		return Util.getShort(bds_states, (short)(state*bds_size+bds_off_stackOff+2));
	}
	private void bds_set_next_leaf(short state, short value) {
		Util.setShort(bds_states, (short)(state*bds_size+bds_off_next_leaf+2), value);
	}
	private short bds_get_next_leaf(short state) {
		return Util.getShort(bds_states, (short)(state*bds_size+bds_off_next_leaf+2));
	}
	private void bds_set_stacklevels(short state, short index, short value) {
		short l = (short)(state*bds_size+bds_off_stacklvls+index*2);
		Util.setShort(bds_states, (short)(state*bds_size+bds_off_stacklvls+index*2), value);
	}
	private short bds_get_stacklevels(short state, short index) {
		return Util.getShort(bds_states, (short)(state*bds_size+bds_off_stacklvls+index*2));
	}
	
	
	
	
	//***********************************
	
    //additional params defines, only here for better readability inside the setter functions
    short params_index_bytes = 4;
    
    private byte[] ots_addr;
    private byte[] ltree_addr;
    private byte[] node_addr;
    
    //***********************************************
    
    private byte[] message_in;
    private short[] arr_key_indices;
    private byte[] arr_address1;
    private byte[] arr_address2;
    private byte[] arr_key_uncompressed;
    private byte[] arr_key_uncompressed2;   
    private byte[] arr_wots_signature;
    //private byte[] arr_tmp;
    //private byte[] arr_seed;
    private byte[] arr_pk_idx;
    private byte[] arr_seed_OTS;
    private byte[] arr_N;
    private byte[] arr_N2;
    private byte[] arr_N3;
    private byte[] arr_N4;
    private byte[] arr_NBackup;
    private byte[] arr_2N;
    private byte[] arr_3N;
    private byte[] arr_4N;
    private byte[] arr_R;
    private byte[] arr_bitmask1;
    private byte[] arr_bitmask2;
    private short[] heights;
    byte[] signature;
    short[] response_offset;
    private boolean signing = false;
    private byte [] arr_stack;
    private short remaining_ram = (short)((H+1)*N + 2*N);
    //private byte[] arr_root;
    //DEBUG ARRAY ROOT:
    private byte arr_root[];// = {(byte)(0x1), (byte)(0x58), (byte)(0x37), (byte)(0x77), (byte)(0xc0), (byte)(0x8b), (byte)(0x9b), (byte)(0xa5), (byte)(0x9c), (byte)(0x4), (byte)(0x11), (byte)(0xfc), (byte)(0x64), (byte)(0x37), (byte)(0xb8), (byte)(0x2), (byte)(0x52), (byte)(0xbf), (byte)(0x6f), (byte)(0x83), (byte)(0x2), (byte)(0xcf), (byte)(0xb8), (byte)(0x76), (byte)(0x34), (byte)(0x95), (byte)(0xad), (byte)(0xe6), (byte)(0xac), (byte)(0xb6), (byte)(0xd0), (byte)(0x30)};
    //private byte[] S;
    /**
     * Only this class's install method should create the applet object.
     */
    protected xmssmtbds() {
        echoBytes = new byte[LENGTH_ECHO_BYTES];
        publicKey = new byte[(short)64];
//        secretSeed = (AESKey)KeyBuilder.buildKey(KeyBuilder.TYPE_AES, KeyBuilder.LENGTH_AES_256, false);
//        secSeedSize = (short)(secretSeed.getSize()/8);
        
        //signatureSize = (short) (4 + N + secKeySize + H * N );
        signatureSize = (short)( indexBytes +N + D * secKeySize + H_full_tree * N+N);
        md256 = MessageDigest.getInstance(MessageDigest.ALG_SHA_256, false);
        number_of_leaves =  1<<H ; // TODO: OVERFLOW ALARM
        //Mockup:
//        SEED = new byte[secSeedSize];
//        for(short i = 0; i< secSeedSize; i++) {
//        	SEED[i] = (byte)i;
//        }
        randomness = RandomData.getInstance(RandomData.ALG_PSEUDO_RANDOM);
        PUB_SEED = new byte[N];
        for(short i = 0; i< N; i++) {
        	//PUB_SEED[i] = (byte)3;
        	randomness.generateData(PUB_SEED, s0, N);
        }
        sk_seed = new byte[N];
        for(short i = 0; i< N; i++) {
//        	sk_seed[i] = (byte)1;
        	randomness.generateData(sk_seed, s0, N);
        }
        sk_prf = new byte[N];
        for(short i = 0; i< N; i++) {
//        	sk_prf[i] = (byte)2;
        	randomness.generateData(sk_prf, s0, N);
        }
        
       
        // Util arrays
        //arr_wots_nodes = new byte[(short)(secKeySize*WOTS_W)];
//        arr_key_indices = new short[WOTS_LEN];
//        arr_address1 = new byte[ADDRESS_SIZE];
        
        arr_key_uncompressed = new byte[secKeySize];
        //arr_key_uncompressed = JCSystem.makeTransientByteArray(secKeySize, JCSystem.CLEAR_ON_DESELECT);
        arr_key_uncompressed2 = new byte[(short)(secKeySize+N)];  // holds additional N bytes in case of padding inside genLtree
        //arr_key_uncompressed2 = JCSystem.makeTransientByteArray((short)(secKeySize+N), JCSystem.CLEAR_ON_DESELECT);
        arr_wots_signature = new byte[secKeySize];
        signature =new byte[signatureSize];	//stores XMSS Signature
        response_offset = new short[1];
        arr_root = new byte[N]; // SEE DEBUG SECTION
//        S = new byte[N];
//        for(short s = 0; s<N; ++s) {
//        	S[s] = (byte)0xFF;
//        }
        
        message_in = new byte[(short)(MAX_MESSAGE_LENGTH + 4*N)]; //for H_msg the message needs 4*N additionaly bytes
        treehash = new byte[(short)(num_treehash_instances*treehash_instance_length*num_bds_states)];
        
        
        
 
        arr_key_indices = JCSystem.makeTransientShortArray(WOTS_LEN, JCSystem.CLEAR_ON_DESELECT);
        arr_address1 = JCSystem.makeTransientByteArray(ADDRESS_SIZE, JCSystem.CLEAR_ON_DESELECT);
        arr_address2 = JCSystem.makeTransientByteArray(ADDRESS_SIZE, JCSystem.CLEAR_ON_DESELECT);
        
        //arr_seed = JCSystem.makeTransientByteArray(secSeedSize, JCSystem.CLEAR_ON_DESELECT);
        arr_seed_OTS = JCSystem.makeTransientByteArray(N, JCSystem.CLEAR_ON_DESELECT);
        arr_R = JCSystem.makeTransientByteArray(N, JCSystem.CLEAR_ON_DESELECT);
        arr_N = JCSystem.makeTransientByteArray(N, JCSystem.CLEAR_ON_DESELECT);
//        arr_N = new byte[N];
        arr_N2 = JCSystem.makeTransientByteArray(N, JCSystem.CLEAR_ON_DESELECT);
        arr_N3 = JCSystem.makeTransientByteArray(N, JCSystem.CLEAR_ON_DESELECT);
        arr_N4 = JCSystem.makeTransientByteArray(N, JCSystem.CLEAR_ON_DESELECT);
        arr_NBackup =JCSystem.makeTransientByteArray(N, JCSystem.CLEAR_ON_DESELECT); 
        arr_2N = JCSystem.makeTransientByteArray((short)(2*N), JCSystem.CLEAR_ON_DESELECT);
        arr_3N = JCSystem.makeTransientByteArray((short)(3*N), JCSystem.CLEAR_ON_DESELECT);
        arr_4N = JCSystem.makeTransientByteArray((short)(4*N), JCSystem.CLEAR_ON_DESELECT);
//        arr_3N = new byte[(short)(3*N)];
//        arr_4N = new byte[(short)(4*N)];
        arr_bitmask1 = JCSystem.makeTransientByteArray(N, JCSystem.CLEAR_ON_DESELECT);
        arr_bitmask2 = JCSystem.makeTransientByteArray(N, JCSystem.CLEAR_ON_DESELECT);
        heights = JCSystem.makeTransientShortArray((short)(H+1), JCSystem.CLEAR_ON_DESELECT);
        arr_stack = JCSystem.makeTransientByteArray(remaining_ram, JCSystem.CLEAR_ON_DESELECT);  //use remaining ram to speed up l-tree construction
        																											//at least H+1 are needed
        //arr_tmp = JCSystem.makeTransientByteArray((short)(  N*16-(((H+1)*N)+2*(H+1))  ), JCSystem.CLEAR_ON_DESELECT);
        bds_swap = new byte[bds_size];
        arr_pk_idx = new byte[(short)(68)];
        /*
        
        arr_stack = new byte[(short)((H+1)*N)];
        arr_tmp = new byte[(short)(ADDRESS_SIZE+N)];
        arr_key_indices = new short[WOTS_LEN];
        arr_address1 = new byte[ADDRESS_SIZE];
        arr_address2 = new byte[ADDRESS_SIZE];
        arr_seed = new byte[secSeedSize];
        arr_seed_OTS = new byte[secSeedSize];
        arr_R = new byte[N];
        arr_N = new byte[N];
        arr_N2 = new byte[N];
        arr_N3 = new byte[N];
        arr_N4 = new byte[N];
        arr_NBackup = new byte[N]; 
        arr_2N = new byte[(short)(2*N)];
        arr_3N = new byte[(short)(3*N)];
        arr_4N = new byte[(short)(4*N)];
        arr_bitmask1 = new byte[N];
        arr_bitmask2 = new byte[N];
        heights = new short[(short)(H+1)];
        bds_swap = new byte[bds_size];
//        arr_key_uncompressed = JCSystem.makeTransientByteArray(secKeySize, JCSystem.CLEAR_ON_DESELECT);
//        arr_key_uncompressed2 = JCSystem.makeTransientByteArray(secKeySize, JCSystem.CLEAR_ON_DESELECT);
//        arr_tmp = JCSystem.makeTransientByteArray(single_cluster_array, JCSystem.CLEAR_ON_DESELECT);
   
*/
        //BDS********************************************
        
//        state = new byte[1000]; //TODO: whats the actual size?
//        treehash_inst = new byte[1000]; //TODO: whats the actual size?
//        sk = new byte[657]; //TODO
        bds_states = new byte[(short)(num_bds_states*bds_size)];
        //arr_SubtreeRoots = [(ADDR 32 byte | OTS SEED 32 byte| subtree Root 32 byte)| ...]
        //arr_SubtreeRoots = new byte[(short)(6*D*N)]; // holds root of subtrees alongside an OTS seed that is used for WOTS computation
        wots_sigs = new byte[(short)(D*WOTS_LEN*N)];
//      bds_state_stack = new byte[(short)((H+1)*N)];			//160 bytes aktuell, (H+1)*N bytes konkret, verwende hier arr_stack
//    	bds_state_stacklevels = new byte[H+1];		//==heights 
//    	bds_state_auth = new byte[H*N];			//128 bytes aktuell, N*H bytes konkret
//    	bds_state_keep = new byte[(short)((H >> 1)*N)];	//((H >> 1)*N) bytes 
//    	bds_state_retain = new byte[(short)(((1<<params_bds_k)-params_bds_k-1)*N)];			//(((1<<params_bds_k)-params_bds_k-1)*N) bytes
    	//treehash_node = new byte[N]; 			//N bytes	
    	ots_addr = JCSystem.makeTransientByteArray(ADDRESS_SIZE, JCSystem.CLEAR_ON_DESELECT);
        ltree_addr = JCSystem.makeTransientByteArray(ADDRESS_SIZE, JCSystem.CLEAR_ON_DESELECT);
        node_addr = JCSystem.makeTransientByteArray(ADDRESS_SIZE, JCSystem.CLEAR_ON_DESELECT);
        
        //***********************************************
        
//        secretSeed.setKey(SEED, (short) 0);
//        secretPRFKey = (AESKey)KeyBuilder.buildKey(KeyBuilder.TYPE_AES, KeyBuilder.LENGTH_AES_256, false);
        //flags = JCSystem.makeTransientByteArray(FLAGS_SIZE, JCSystem.CLEAR_ON_DESELECT);
        JCSystem.requestObjectDeletion();
        register();
    }

    /**
     * Installs this applet.
     *
     * @param bArray
     *            the array containing installation parameters
     * @param bOffset
     *            the starting offset in bArray
     * @param bLength 
     *            the length in bytes of the parameter data in bArray
     */
    public static void install(byte[] bArray, short bOffset, byte bLength) {
        new xmssmtbds();
    }

    /**
     * Processes an incoming APDU.
     *
     * @see APDU
     * @param apdu
     *            the incoming APDU
     * @exception ISOException
     *                with the response bytes per ISO 7816-4
     */
   //@Override 
   public void process(APDU apdu) {
        byte buffer[] = apdu.getBuffer(); 
//        short LC = apdu.setIncomingAndReceive();
//        short recvLen = apdu.setIncomingAndReceive();
//        short dataOffset = apdu.getOffsetCdata();
        // check SELECT APDU command
        if ((buffer[ISO7816.OFFSET_CLA] == 0) &&
                (buffer[ISO7816.OFFSET_INS] == (byte) (0xA4))) {
            return;
        }
        switch(buffer[ISO7816.OFFSET_INS]) {
        case GENERATE_PK:
        	//genAddress(ADDRESS_XMSS_TREE, (short)0, H, arr_address1);
        	xmssMT_core_keypair();
        	send_response(apdu, arr_root, N);
        	break;
        case INIT:
        	helloTest(apdu, (short)0);
        	break;
        case SIGN_MESSAGE_short: 
            //Util.arrayFillNonAtomic(authPath, (short)0, (short)(N*H), (byte)0);
            //Util.arrayFillNonAtomic(signature, (short)0, signatureSize, (byte)0);
           // Util.arrayFillNonAtomic(arr_tmp, (short)0, single_cluster_array, (byte)0);
//            Util.arrayFillNonAtomic(arr_key_uncompressed2, (short)0, (short)(secKeySize+N), (byte)0);
//            Util.arrayFillNonAtomic(arr_key_uncompressed, (short)0, secKeySize, (byte)0);
        	//sign_message(apdu, signature);				//XMSS Signature generation
        	response_offset[0] = s0;
            xmssMT_core_sign(apdu, signature);
        	//sendSignature(apdu, signature);
            sendSignatureShort(apdu);
        	break;
        case SIGN_MESSAGE: 
            //Util.arrayFillNonAtomic(authPath, (short)0, (short)(N*H), (byte)0);
//            Util.arrayFillNonAtomic(signature, (short)0, signatureSize, (byte)0);
            //Util.arrayFillNonAtomic(arr_tmp, (short)0, single_cluster_array, (byte)0);
//            Util.arrayFillNonAtomic(arr_key_uncompressed2, (short)0, (short)(secKeySize+N), (byte)0);
//            Util.arrayFillNonAtomic(arr_key_uncompressed, (short)0, secKeySize, (byte)0);
        	//sign_message(apdu, signature);				//XMSS Signature generation
            xmssMT_core_sign(apdu, signature);
        	sendSignature(apdu, signature);
            //sendSignatureShort(apdu, signature);
        	break;
        case PREPARE_NEXT_AUTH_PATH: // pre-calculate the authentication path for the next signature
        //	gen_auth_path(authPath);
        	break;

        case DBG_MESSAGE_BASE:
        	short s = apdu.getIncomingLength();
        	byte[] message = new byte[s];
        	Util.arrayCopy(buffer, (short)ISO7816.OFFSET_CDATA, message, (short)0, s);
        	short[] result = new short[WOTS_LEN];
        	//baseFunction(message, (short)0, WOTS_LEN,(short)0, result);
        	chainLengths(message,arr_key_indices);
        	break;
        
        case BENCHMARK_HASH:
        case BENCHMARK_HASH_EEPROM:
        case BENCHMARK_COPY_RAM:
        case BENCHMARK_COPY_EEPROM:
        case BENCHMARK_COPY_EEPROM_to_RAM:
        case BENCHMARK_COPY_RAM_to_EEPROM:
        case BENCHMARK_HASH_RAM_to_EEPROM:
        case BENCHMARK_HASH_EEPROM_to_RAM:
        	benchmark(buffer);
        	sendSignature(apdu, arr_N);
        	break;
        case GET_RESPONSE:
        	getResponse(apdu);
        	break;
        case TEST_GEN_SK:
        	genSk(arr_address1, arr_key_uncompressed);
        	sendSignature(apdu, arr_N);
        	break;
        case TEST_WOTS_SIGN:
        	Util.arrayFillNonAtomic(ots_addr, s0, N, b0);
        	Util.setShort(ots_addr, (short)2, (short)1);
        	randomness.generateData(arr_seed_OTS, s0, N);
        	randomness.generateData(arr_NBackup, s0, N);
        	wots_sign(ots_addr, arr_seed_OTS, arr_NBackup, arr_wots_signature, s0);
        	sendSignature(apdu, arr_N);
        	break;
        case TEST_WOTS_SIGN_OVERHEAD:
        	Util.arrayFillNonAtomic(ots_addr, s0, N, b0);
        	Util.setShort(ots_addr, (short)2, (short)1);
        	randomness.generateData(arr_seed_OTS, s0, N);
        	randomness.generateData(arr_NBackup, s0, N);
        	//wots_sign(ots_addr, arr_seed_OTS, arr_NBackup, arr_wots_signature, s0);
        	sendSignature(apdu, arr_N);
        	break;
        case TEST_GET_XMSS_LEAF:
        	get_xmss_leaf(ots_addr, arr_key_uncompressed, s0);
        	sendSignature(apdu, arr_N);
        	break;
        case TEST_CHAIN:
        	for(short x = 0; x<WOTS_LEN; ++x) {
        		arr_key_indices[x] = (short)(WOTS_W-1);
        	}
        	chain(arr_key_uncompressed, arr_address1, arr_key_indices, arr_key_uncompressed2, s0);
        	break;
        case TEST_GEN_LTREE:
        	gen_ltree(ots_addr, arr_key_uncompressed2, arr_key_uncompressed, s0);
        	sendSignature(apdu, arr_N);
        	break;
        
        default:
        	ISOException.throwIt(ISO7816.SW_INS_NOT_SUPPORTED);
        }
        	
     
    }
   
   //BDS state setter and getter stuff:************************

   //sk[ 4bytes OID | 4bytes Index | ((H+1)*N) (160 now) bytes Stack   ]

   
//   private void prepareBDS(short in){
//	   //xmss::xmss_keypair:
//		   	//add OID - here static set
//   	sk[3] = 0x1;
//   	sk_offset +=4;
//   	
//    //xmss_core_fast::xmss_core_keypair:
//   			//sk here has an offset of 4 in ref
//   			//state and treehash are initialized
//
//   		//xmss_core_fast::xmssmt_deserialize_state   -  copy state info into sk:
//   			//skip past the 'regular' sk 
//   			sk_offset += (params_index_bytes + 4*N);
//   			for(short i = 0; i < 2*D-1;i++) { //only actually used for MT, keeping it for now
//   				//STACK:
//   				//sk bytes 8-(8+((H+1)*N) are used as stack
//   					// states[i].stack = sk;
//   					//sk += (params->tree_height + 1) * params->n; 
//   				sk_offset+= (short)((H+1)*N);
//   				//STACK OFFSET:
//   				sk
//   				
//   			}
//   }
   private void benchmark(byte[] buffer) {
	   short end = (short)(Util.getShort(buffer, (short)ISO7816.OFFSET_CDATA)/2);
	   short bytesTimesN = Util.getShort(buffer, (short)(ISO7816.OFFSET_CDATA+2));
	   short s;
	   switch(buffer[ISO7816.OFFSET_INS]) {
       case BENCHMARK_HASH:
    	   for(s = 0; s<end; ++s) {
    		   md256.doFinal(arr_4N, s0, (short)(N*bytesTimesN), arr_stack, s0);
    		   md256.doFinal(arr_stack, s0, (short)(N*bytesTimesN), arr_4N, s0);
    	   }
    	   break;
       case BENCHMARK_HASH_EEPROM:
    	   for(s = 0; s<end; ++s) {
    		   md256.doFinal(arr_key_uncompressed, s0, (short)(N*bytesTimesN), arr_key_uncompressed2, s0);
    		   md256.doFinal(arr_key_uncompressed2, s0, (short)(N*bytesTimesN), arr_key_uncompressed, s0);
    	   }
    	   break;
       case BENCHMARK_COPY_RAM:
    	   for(s = 0; s<end; ++s) {
    		   Util.arrayCopyNonAtomic(arr_4N, s0, arr_stack, s0, (short)(bytesTimesN*N));
    		   Util.arrayCopyNonAtomic(arr_stack, s0, arr_4N, s0, (short)(bytesTimesN*N));
    	   }
    	   break;
       case BENCHMARK_COPY_EEPROM:
    	   for(s = 0; s<end; ++s) {
    		   Util.arrayCopyNonAtomic(arr_key_uncompressed, s0, arr_key_uncompressed2, s0, (short)(bytesTimesN*N));
    		   Util.arrayCopyNonAtomic(arr_key_uncompressed2, s0, arr_key_uncompressed, s0, (short)(bytesTimesN*N));
    	   }
    	   break;
       case BENCHMARK_COPY_EEPROM_to_RAM:
    	   for(s = 0; s<end; ++s) {
    		   Util.arrayCopyNonAtomic(arr_key_uncompressed, s0, arr_4N, s0, (short)(bytesTimesN*N));
    		   Util.arrayCopyNonAtomic(arr_key_uncompressed2, s0, arr_stack, s0, (short)(bytesTimesN*N));
    	   }
    	   break;
       case BENCHMARK_COPY_RAM_to_EEPROM:
    	   for(s = 0; s<end; ++s) {
    		   Util.arrayCopyNonAtomic(arr_4N, s0, arr_key_uncompressed2, s0, (short)(bytesTimesN*N));
    		   Util.arrayCopyNonAtomic(arr_stack, s0, arr_key_uncompressed, s0, (short)(bytesTimesN*N));
    		   
    	   }
    	   break;
       case BENCHMARK_HASH_RAM_to_EEPROM:
    	   for(s = 0; s<end; ++s) {
    		   md256.doFinal(arr_4N, s0, (short)(N*bytesTimesN), arr_key_uncompressed, s0);
    		   md256.doFinal(arr_stack, s0, (short)(N*bytesTimesN), arr_key_uncompressed2, s0);
    	   }
    	   break;
       case BENCHMARK_HASH_EEPROM_to_RAM:
    	   for(s = 0; s<end; ++s) {
    		   md256.doFinal(arr_key_uncompressed, s0, (short)(N*bytesTimesN), arr_4N, s0);
    		   md256.doFinal(arr_key_uncompressed2, s0, (short)(N*bytesTimesN), arr_stack, s0);
    	   }
    	   break;
	   }

   }


   
	private void treehash_set_h(short state, short th_instance, short val) {
		Util.setShort(treehash, (short)(state*states_offset+th_instance*treehash_instance_length+2), val);
	}
	private short treehash_get_h(short state, short th_instance) {
		return Util.getShort(treehash, (short)(state*states_offset+th_instance*treehash_instance_length+2));
	}
	private void treehash_set_next_idx(short state, short th_instance, short val) {
		Util.setShort(treehash, (short)(state*states_offset+th_instance*treehash_instance_length+6), val);
	}
	private short treehash_get_next_idx(short state, short th_instance) {
		return Util.getShort(treehash, (short)(state*states_offset+th_instance*treehash_instance_length+6));
	}
	private void treehash_set_stackusage(short state, short th_instance, short val) {
		Util.setShort(treehash, (short)(state*states_offset+th_instance*treehash_instance_length+10), val);
	}
	private short treehash_get_stackusage(short state, short th_instance) {
		return Util.getShort(treehash, (short)(state*states_offset+th_instance*treehash_instance_length+10));
	}
	private void treehash_set_completed(short state, short th_instance, short val) {
		Util.setShort(treehash, (short)(state*states_offset+th_instance*treehash_instance_length+14), val);
	}
	private short treehash_get_completed(short state, short th_instance) {
		return Util.getShort(treehash, (short)(state*states_offset+th_instance*treehash_instance_length+14));
	}
//   private void xmss_core_keypair(){
//	   //deserialize_state();
//	   bds_set_stackoffset(s0, s0);
//	   bds_set_next_leaf(s0, s0);
//	   Util.arrayFillNonAtomic(sk, s0, (short)4, b0);
//	   for(short s = 0; s<N; ++s) {
//		   sk[(short)(s+params_index_bytes)] = (byte) s;
//		   sk[(short)(s+params_index_bytes+N)] = (byte) s;
//	   }
//	   Util.arrayFillNonAtomic(arr_address1, s0, N, b0);
//	   treehash_init(arr_address1,s0, arr_root);
//	   Util.arrayCopyNonAtomic(arr_root, s0, sk, (short)(params_index_bytes+2*N), N);
//	   //serialize_state();
//   }
   
   private void xmssMT_core_keypair() {
	   short bds_count;
	   for(bds_count= 0; bds_count<num_bds_states; ++bds_count) {
		   bds_set_stackoffset(bds_count, s0);
		   bds_set_next_leaf(bds_count, s0);
	   }
	   Util.arrayFillNonAtomic(arr_address1, s0, N, b0);
	   setLayerAddr(arr_address1, s0);
	    for (bds_count = 0; bds_count < D - 1; bds_count++) {
	        // Compute seed for OTS key pair
	        treehash_init(arr_address1, bds_count, arr_root);
	        setLayerAddr(arr_address1, (short)(bds_count+1));
	        get_seed(arr_address1, arr_N);
//	        Util.arrayCopyNonAtomic(arr_address1, s0, arr_SubtreeRoots, (short)(bds_count*N), N);
//	        Util.arrayCopyNonAtomic(arr_N, s0, arr_SubtreeRoots, (short)(bds_count*N+N), N);
//	        Util.arrayCopyNonAtomic(arr_root, s0, arr_SubtreeRoots, (short)(bds_count*N+2*N), N);
	        //get_xmss_leaf(ots_addr, arr_2N, s0);
	        wots_sign(arr_address1, arr_N, arr_root, wots_sigs, (short)(bds_count*secKeySize));
	        //wots_sign(params, wots_sigs + i*params->wots_sig_bytes, pk, ots_seed, pk+params->n, addr);
	    }
	    treehash_init(arr_address1, bds_count, arr_root);
	    
   }
   

   
   /*
    * static void treehash_init(const xmss_params *params,
                          unsigned char *node, int height, int index,
                          bds_state *state, const unsigned char *sk_seed,
                          const unsigned char *pub_seed, const uint32_t addr[8])
    */
   private void treehash_init(byte[] address_l, short bds_state_instance, byte[] result){
	   short bdsOffset = (short)(bds_state_instance*bds_size);
	   //secretSeed.getKey(arr_seed, s0);
	   short offset_sk_seed = params_index_bytes; // actually used to get SEED 
	   short offset_pub_seed = (short)(params_index_bytes + 3*N);
	   short idx = currentIndex;	//TODO: Cleanup unnecessary assignments/variables
	    // use three different addresses because at this point we use all three formats in parallel
	   Util.arrayFillNonAtomic(ots_addr, s0, N, b0);
	   Util.arrayFillNonAtomic(ltree_addr, s0, N, b0);
	   Util.arrayFillNonAtomic(node_addr, s0, N, b0);
 	    // only copy layer and tree address parts
	   Util.arrayCopyNonAtomic(address_l, s0, ots_addr, s0, N);
	   setAddressType(ots_addr, ADDRESS_WOTS);
	    // type = ots
	   Util.arrayCopyNonAtomic(address_l, s0, ltree_addr, s0, N);
	   setAddressType(ltree_addr, ADDRESS_L_TREE); 
	   Util.arrayCopyNonAtomic(address_l, s0, node_addr, s0, N);
	   setAddressType(node_addr, ADDRESS_XMSS_TREE);
	   
	   
	   short lastnode, i;
	    //unsigned char stack[(height+1)*params->n];  arr stack = arr_stack
	    //unsigned int stacklevels[height+1];		  arr stacklevels = heights
	    short stackoffset=0;
	    short nodeh;

	    lastnode = (short) (idx+(1<<H));

	    for (i = 0; i < H-params_bds_k; i++) {
	    	treehash_set_h(bds_state_instance, i, i);
	    	treehash_set_completed(bds_state_instance, i, (short)(1));
	        treehash_set_stackusage(bds_state_instance, i, s0);
	    }

	    i = 0;
	    for (; idx < lastnode; idx++) {
	    	setLTreeAddr(ltree_addr, idx);
	    	setOTSAddr(ots_addr, idx); 
	    	get_xmss_leaf(ots_addr, arr_stack, (short)(stackoffset*N));
	        //gen_leaf_wots(params, stack+stackoffset*params->n, sk_seed, pub_seed, ltree_addr, ots_addr);
	    	heights[stackoffset] = 0;
	        stackoffset++;
	        if (H - params_bds_k > 0 && i == 3) {
	        	Util.arrayCopyNonAtomic(arr_stack, (short)(stackoffset*N), treehash, (short)(states_offset*bds_state_instance+treehash_node_offset), N);
	            //memcpy(state->treehash[0].node, stack+stackoffset*params->n, params->n);
	        }
	        while (stackoffset>1 && heights[(short)(stackoffset-1)] == heights[(short)(stackoffset-2)]) {
	            nodeh = heights[(short)(stackoffset-1)];
	            if (i >> nodeh == 1) {
	                //memcpy(state->auth + nodeh*params->n, stack+(stackoffset-1)*params->n, params->n);
	                Util.arrayCopyNonAtomic(arr_stack, (short)((stackoffset-1)*N), bds_states, (short)(bdsOffset+bds_off_auth+nodeh*N), N);
	                }
	            else {
	                if (nodeh < H - params_bds_k && i >> nodeh == 3) {
	                    //memcpy(state->treehash[nodeh].node, stack+(stackoffset-1)*params->n, params->n);
	                    Util.arrayCopyNonAtomic(arr_stack, (short)((stackoffset-1)*N), treehash, (short)(bds_state_instance*states_offset+treehash_instance_length*nodeh+treehash_node_offset) , N);
	                }
	                else if (nodeh >= H - params_bds_k) {
	                    //memcpy(state->retain + ((1 << (params->tree_height - 1 - nodeh)) + nodeh - params->tree_height + (((i >> nodeh) - 3) >> 1)) * params->n, stack+(stackoffset-1)*params->n, params->n);
	                	//offset = (bds_state_instance*bds_size+bds_off_retain+(((1<<(H-1-nodeh))+nodeh-H+(((i>>nodeh)-3)>>1))* N))
	                    Util.arrayCopyNonAtomic(arr_stack, (short)((stackoffset-1)*N), bds_states, (short)(bdsOffset+bds_off_retain+(((1<<(H-1-nodeh))+nodeh-H+(((i>>nodeh)-3)>>1))* N)), N);
	                }
	            }
	            setTreeHeight(node_addr, heights[(short)(stackoffset-1)]);
	            setTreeIndex(node_addr, (short)(idx>>(heights[(short)(stackoffset-1)]+1)));
	            
	            //thash_h(params, stack+(stackoffset-2)*params->n, stack+(stackoffset-2)*params->n, pub_seed, node_addr);
	            Util.arrayCopyNonAtomic(arr_stack, (short)((stackoffset-2)*N), arr_2N, s0, (short)(2*N));
     			//setTreePadding(arr_address2); //NEEDED? 
     			RAND_HASH(arr_2N, PUB_SEED, node_addr, arr_stack, (short)((stackoffset-2)*N));
     			//Util.arrayCopyNonAtomic(arr_N, s0, arr_stack, (short)((stackoffset-2)*N), N);
	            
	            
	            heights[(short)(stackoffset-2)]++;
	            stackoffset--;
	        }
	        i++;
	    }
//	    for (i = 0; i < N; i++) {
//	        node[i] = stack[i];
//	    }
	    Util.arrayCopyNonAtomic(arr_stack, s0, result, s0, N)	;   
   }
   
   
   private void bds_round(byte[] addr_l, short idx, short bds_instance){
	   short bds_offset = (short)(bds_instance*bds_size);
	   short i;
	   short tau = H;
	   short startidx,offset,rowidx;
	   //buf = arr_2N
	   Util.arrayFillNonAtomic(arr_2N, s0, (short)(2*N),b0);
	   Util.arrayFillNonAtomic(ots_addr, s0, N, b0);
	   Util.arrayFillNonAtomic(ltree_addr, s0, N, b0);
	   Util.arrayFillNonAtomic(node_addr, s0, N, b0);
	   Util.arrayCopyNonAtomic(addr_l, s0, ots_addr, s0, (short)12);
	   Util.arrayCopyNonAtomic(addr_l, s0, ltree_addr, s0, (short)12);
	   Util.arrayCopyNonAtomic(addr_l, s0, node_addr, s0, (short)12);
	   setAddressType(ots_addr, ADDRESS_WOTS);
	   setAddressType(ltree_addr, ADDRESS_L_TREE);
	   setAddressType(node_addr, ADDRESS_XMSS_TREE);
	   for (i = 0; i < H; i++) {
		   if ( 0==((idx >> i) & 1)) {
			   tau = i;
			   break;
		   }
	   }
	    if (tau > 0) {
	        //memcpy(buf, state->auth + (tau-1) * params->n, params->n);
	        Util.arrayCopyNonAtomic(bds_states, (short)(bds_offset+bds_off_auth+(tau-1)*N), arr_2N, s0, N);
	        // we need to do this before refreshing state->keep to prevent overwriting
	        //memcpy(buf + params->n, state->keep + ((tau-1) >> 1) * params->n, params->n);
	        Util.arrayCopyNonAtomic(bds_states, (short)(bds_offset+bds_off_keep+((tau-1)>>1)*N), arr_2N, N, N);
	    }
	    if (0==((idx >> (tau + 1)) & 1) && (tau < H - 1)) {
	        //memcpy(state->keep + (tau >> 1)*params->n, state->auth + tau*params->n, params->n);
	        Util.arrayCopyNonAtomic(bds_states, (short)(bds_offset+bds_off_auth+tau*N), bds_states, (short)(bds_offset+bds_off_keep+(tau>>1)*N), N);
	    }
	    if (tau == 0) {
	        //set_ltree_addr(ltree_addr, leaf_idx);
	        setLTreeAddr(ltree_addr, idx);
	        //set_ots_addr(ots_addr, leaf_idx);
	        setOTSAddr(ots_addr, idx);
	        //gen_leaf_wots(params, state->auth, sk_seed, pub_seed, ltree_addr, ots_addr);
	        get_xmss_leaf(ots_addr, bds_states, (short)(bds_offset+bds_off_auth));
	    }else {
	        //set_tree_height(node_addr, (tau-1));
	    	setTreeHeight(node_addr, (short)(tau-1));
	        //set_tree_index(node_addr, leaf_idx >> tau);
	    	setTreeIndex(node_addr, (short)(idx>>tau));
	        //thash_h(params, state->auth + tau * params->n, buf, pub_seed, node_addr);
	    	RAND_HASH(arr_2N, PUB_SEED, node_addr, bds_states,(short)(bds_offset+bds_off_auth+tau*N));
	    	//Util.arrayCopyNonAtomic(arr_N, s0, bds_states, (short)(bds_offset+bds_off_auth+tau*N), N);
	    	    	
	        for (i = 0; i < tau; i++) {
	            if (i < H - params_bds_k) {
	                //memcpy(state->auth + i * params->n, state->treehash[i].node, params->n);
	            	Util.arrayCopyNonAtomic(treehash, (short)(bds_instance*states_offset+i*treehash_instance_length+treehash_node_offset), bds_states, (short)(bds_offset+bds_off_auth+i*N), N);
	            }
	            else {
	                offset = (short) ((1 << (H - 1 - i)) + i - H);
	                rowidx = (short) (((idx >> i) - 1) >> 1);
	                //memcpy(state->auth + i * params->n, state->retain + (offset + rowidx) * params->n, params->n);
	                Util.arrayCopyNonAtomic(bds_states, (short)(bds_offset+bds_off_retain+(offset+rowidx)*N), bds_states, (short)(bds_offset+bds_off_auth+i*N), N);
	            }
	        }

	        for (i = 0; i < ((tau < H - params_bds_k) ? tau : (H - params_bds_k)); i++) {
	            startidx = (short)(idx + 1 + 3 * (1 << i));
	            if (startidx < (short)(1 << H)) {
	            	treehash_set_h(bds_instance, i, i);
	            	treehash_set_next_idx(bds_instance, i, startidx);
	            	treehash_set_completed(bds_instance, i, s0);
	            	treehash_set_stackusage(bds_instance, i, s0);
	            }
	        }
	    }
   }
   private short treehash_minheight_on_stack(short bds_instance, short treehash_instance) {
	    short r = H, i;
	    short bds_offset = (short)(bds_instance*bds_size);
	    short stackusage = treehash_get_stackusage(bds_instance, treehash_instance);
	    for (i = 0; i < stackusage; i++) {
	    
	    	if(bds_get_stacklevels(bds_instance, (short)(bds_get_stackoffset(bds_instance)-i-1)) < r){
	            r = bds_get_stacklevels(bds_instance, (short)(bds_get_stackoffset(bds_instance)-i-1));
	        }
	    }
	    return r;
   }
private void treehash_update(byte[] addr_l, short bds_instance, short treehash_instance) {
	short bds_offset = (short)(bds_instance*bds_size);
	 Util.arrayFillNonAtomic(ots_addr, s0, N, b0);
	   Util.arrayFillNonAtomic(ltree_addr, s0, N, b0);
	   Util.arrayFillNonAtomic(node_addr, s0, N, b0);
	   Util.arrayCopyNonAtomic(addr_l, s0, ots_addr, s0, (short)12);
	   Util.arrayCopyNonAtomic(addr_l, s0, ltree_addr, s0, (short)12);
	   Util.arrayCopyNonAtomic(addr_l, s0, node_addr, s0, (short)12);
	   setAddressType(ots_addr, ADDRESS_WOTS);
	   setAddressType(ltree_addr, ADDRESS_L_TREE);
	   setAddressType(node_addr, ADDRESS_XMSS_TREE);
	   
	   // set_ltree_addr(ltree_addr, treehash->next_idx);
	   setLTreeAddr(ltree_addr, treehash_get_next_idx(bds_instance, treehash_instance));
	    //set_ots_addr(ots_addr, treehash->next_idx);
	   setOTSAddr(ots_addr, treehash_get_next_idx(bds_instance, treehash_instance));
	   
	    //unsigned char nodebuffer[2 * params->n]; == arr_2N
	    short nodeheight = 0;
	    //gen_leaf_wots(params, nodebuffer, sk_seed, pub_seed, ltree_addr, ots_addr);
	    get_xmss_leaf(ots_addr, arr_2N, s0);
	    
	    
	    while ((treehash_get_stackusage(bds_instance, treehash_instance) > 0) &&  bds_get_stacklevels(bds_instance, (short)(bds_get_stackoffset(bds_instance)-1))==nodeheight) {	
	    //	        //memcpy(nodebuffer + params->n, nodebuffer, params->n);
	    	Util.arrayCopyNonAtomic(arr_2N, s0, arr_2N, N, N);
//	        //memcpy(nodebuffer, state->stack + (state->stackoffset-1)*params->n, params->n);
	    	
	    	Util.arrayCopyNonAtomic(bds_states, (short)(bds_offset+bds_off_stack+(bds_get_stackoffset(bds_instance)-1)*N), arr_2N, s0, N);
//	        //set_tree_height(node_addr, nodeheight);
	    	setTreeHeight(node_addr, nodeheight);
//	        //set_tree_index(node_addr, (treehash->next_idx >> (nodeheight+1)));
	    	setTreeIndex(node_addr, (short)(treehash_get_next_idx(bds_instance, treehash_instance)>> (nodeheight+1)));
	    	
//	        thash_h(params, nodebuffer, nodebuffer, pub_seed, node_addr);
	    	
 			//setTreePadding(arr_address2); //NEEDED? 
 			RAND_HASH(arr_2N, PUB_SEED, node_addr, arr_2N, s0);
 			//Util.arrayCopyNonAtomic(arr_N, s0, arr_2N, s0, N);
	    	
	    	
	        nodeheight++;
	        treehash_set_stackusage(bds_instance, treehash_instance, (short)(treehash_get_stackusage(bds_instance, treehash_instance)-1));
	        bds_set_stackoffset(bds_instance, (short) (bds_get_stackoffset(bds_instance)-1));
	    }
	    if (nodeheight == treehash_get_h(bds_instance, treehash_instance)) { // this also implies stackusage == 0
	        //memcpy(treehash->node, nodebuffer, params->n);
	    	Util.arrayCopyNonAtomic(arr_2N, s0, treehash, (short)(states_offset*bds_instance+treehash_instance*treehash_instance_length+treehash_node_offset), N);
	        treehash_set_completed(bds_instance, treehash_instance, (short)1);
	    }
	    else {
	        //memcpy(state->stack + state->stackoffset*params->n, nodebuffer, params->n);
	    	Util.arrayCopyNonAtomic(arr_2N, s0, bds_states, (short)(bds_offset+bds_off_stack+bds_get_stackoffset(bds_instance)*N), N);
	        treehash_set_stackusage(bds_instance, treehash_instance, (short)(treehash_get_stackusage(bds_instance, treehash_instance)+1));
	        bds_set_stacklevels(bds_instance, bds_get_stackoffset(bds_instance), nodeheight);
	        bds_set_stackoffset(bds_instance, (short)(bds_get_stackoffset(bds_instance)+1));
	        treehash_set_next_idx(bds_instance, treehash_instance, (short)(treehash_get_next_idx(bds_instance, treehash_instance)+1));
	    }
}
   private short bds_treehash_update(byte[] addr_l, short bds_instance, short updates) {
	    short i, j, level, l_min, low, used =0;

	    for (j = 0; j < updates; j++) {
	        l_min = H;
	        level = (short)(H - params_bds_k);
	        for (i = 0; i < (short)(H - params_bds_k); i++) {
	            if (treehash_get_completed(bds_instance, i)==1) {
	                low = H;
	            }
	            else if (treehash_get_stackusage(bds_instance, i) == 0) {
	                low = i;
	            }
	            else {
	                low = treehash_minheight_on_stack(bds_instance, i);
	            }
	            if (low < l_min) {
	                level = i;
	                l_min = low;
	            }
	        }
	        if (level == H - params_bds_k) {
	            break;
	        }
	        treehash_update(addr_l, bds_instance, level);
	        used++;
	    }
	    return (short) (updates-used);
   }


   
   private void bds_state_update(byte[] addr_l, short bds_instance, byte[]seed, byte[]pubSeed)
   {
	   short bdsOffset = (short)(bds_instance*bds_size);
	   Util.arrayFillNonAtomic(ltree_addr, s0, N, b0);
	   Util.arrayFillNonAtomic(node_addr, s0, N, b0);
	   Util.arrayFillNonAtomic(ots_addr, s0, N, b0);
	   

	   short nodeh, temp;
	   short idx = bds_get_next_leaf(bds_instance);
	   if (idx == 1 << H) {
		   //TODO: error handling
	   }

	   // only copy layer and tree address parts
	   Util.arrayCopyNonAtomic(addr_l, s0, ots_addr, s0, (short)12);
	   
	   // type = ots
	   setAddressType(ots_addr, ADDRESS_WOTS);
	   Util.arrayCopyNonAtomic(addr_l, s0, ltree_addr, s0, (short)12);
	   setAddressType(ltree_addr, ADDRESS_L_TREE);
	   Util.arrayCopyNonAtomic(addr_l, s0, node_addr, s0, (short)12);
	   setAddressType(node_addr, ADDRESS_XMSS_TREE);

	   setOTSAddr(ots_addr, idx);
	   setLTreeAddr(ltree_addr, idx);
	   get_xmss_leaf(ots_addr, bds_states, (short)(bdsOffset+bds_off_stack+bds_get_stackoffset(bds_instance)*N));
	   
	   bds_set_stacklevels(bds_instance, bds_get_stackoffset(bds_instance), s0);
	   bds_set_stackoffset(bds_instance, (short)(bds_get_stackoffset(bds_instance)+1));
	   
	   if (H - params_bds_k > 0 && idx == 3) {
		   Util.arrayCopyNonAtomic(bds_states, (short)(bdsOffset+(bds_get_stackoffset(bds_instance)*N)), treehash, (short)(states_offset*bds_instance+treehash_node_offset), N);
	   }
	   short test1 = bds_get_stackoffset(bds_instance);
	   short test2 = bds_get_stacklevels(bds_instance, (short)(bds_get_stackoffset(bds_instance)-1));
	   while (bds_get_stackoffset(bds_instance) > 1 && bds_get_stacklevels(bds_instance, (short)(bds_get_stackoffset(bds_instance)-1)) == bds_get_stacklevels(bds_instance, (short)(bds_get_stackoffset(bds_instance)-2))) {
		   nodeh = bds_get_stacklevels(bds_instance, (short)(bds_get_stackoffset(bds_instance)-1));
		   if (idx >> nodeh == 1) {
			   Util.arrayCopyNonAtomic(bds_states, (short)(bdsOffset+bds_off_stack+(bds_get_stackoffset(bds_instance)-1)*N), bds_states, (short)(bdsOffset+bds_off_auth+nodeh*N), N);
		   }
		   else {
			   if (nodeh < H - params_bds_k && idx >> nodeh == 3) {
				   Util.arrayCopyNonAtomic(bds_states, (short)(bdsOffset+bds_off_stack+(bds_get_stackoffset(bds_instance)-1)*N), treehash, (short)(states_offset*bds_instance+nodeh*treehash_instance_length+treehash_node_offset), N);
			   }
			   else if (nodeh >= H - params_bds_k) {
				   Util.arrayCopyNonAtomic(bds_states, (short)(bdsOffset+bds_off_stack+(bds_get_stackoffset(bds_instance)-1)*N), bds_states, (short)(bdsOffset+bds_off_retain+((1 << (H-1-nodeh))+nodeh-H + (((idx >> nodeh)-3) >> 1))*N), N);
			   }
		   }
		   setTreeHeight(node_addr, bds_get_stacklevels(bds_instance, (short)(bds_get_stackoffset(bds_instance)-1)));
		   setTreeIndex(node_addr, (short) (idx >> (bds_get_stacklevels(bds_instance, (short)(bds_get_stackoffset(bds_instance)-1) )+1)));
		   
		   
           //thash_h(params, stack+(stackoffset-2)*params->n, stack+(stackoffset-2)*params->n, pub_seed, node_addr);
           Util.arrayCopyNonAtomic(bds_states, (short)(bdsOffset+bds_off_stack+(bds_get_stackoffset(bds_instance)-2)*N), arr_2N, s0, (short)(2*N));
			//setTreePadding(arr_address2); //NEEDED? 
			RAND_HASH(arr_2N, pubSeed, node_addr, bds_states, (short)(bdsOffset+bds_off_stack+(bds_get_stackoffset(bds_instance)-2)*N));
			//Util.arrayCopyNonAtomic(arr_N, s0, bds_states, (short)(bdsOffset+bds_off_stack+(bds_get_stackoffset(bds_instance)-2)*N), N);
		   
		   //bds_states[(short)(bdsOffset+bds_off_stacklvls+bds_get_stackoffset(bds_instance)-2)]+=1;
		   
		   temp = (short)(bds_get_stacklevels(bds_instance, (short)(bds_get_stackoffset(bds_instance)-2))+1);
		   bds_set_stacklevels(bds_instance, (short)(bds_get_stackoffset(bds_instance)-2), temp);
		   
		   bds_set_stackoffset(bds_instance, (short)(bds_get_stackoffset(bds_instance)-1));
	   }
	   
	   bds_set_next_leaf(bds_instance, (short)(bds_get_next_leaf(bds_instance)+1));
   }

   private void xmssMT_core_sign(APDU apdu, byte[] signature){
	   if(signing == true) { //Card tear or interrupt, error handling (verify functionality, maybe change process)
		   currentIndex+=2;	//increment index as a safety precaution
	   } 
	   //secretSeed.getKey(arr_seed, s0);
	   byte buffer[] = apdu.getBuffer(); 	
	   short msg_len = apdu.getIncomingLength();
	   //byte[] message_in = new byte[len];
	   Util.arrayCopy(buffer, (short)ISO7816.OFFSET_CDATA, message_in, (short)(4*N), msg_len);	// message_in = actual message to sign
	   //short[] bases = new short[WOTS_LEN];	//stores calculated bases for message 
	   //baseFunction(message, (short)0, WOTS_LEN, arr_key_indices);			//bases now contains WOTS_LEN short values

	   //r
	   //initialize r = arr_N2
	   //key:
	   Util.arrayFillNonAtomic(arr_N3, (short)0, N, (byte)0);
	   arr_N3[N-2] = (byte)((currentIndex >> 8) & 0xff); 
	   arr_N3[N-1] = (byte)(currentIndex & 0xff);
	   //arr_N = secretSeed
	   //arr_N3 = currentIndex in bytes
	   //PRF(secretSeed, index )
	   PRF(sk_prf, arr_N3, arr_N2, s0); //r = arr_N2
	   Util.arrayCopyNonAtomic(arr_N2, s0, arr_R, s0, N); // save R for final signature 

	   //root:
	   //key(3n) = r(n)   + root(n) + tobyte_tmp(n)
	   // here:	arr_N2	 arr_root  arr_N3
	   Util.arrayCopyNonAtomic(arr_N2, (short)0, arr_3N, (short)0, N);
	   Util.arrayCopyNonAtomic(arr_root, (short)0, arr_3N, (short)N, N);
	   Util.arrayCopyNonAtomic(arr_N3, (short)0, arr_3N, (short)(2*N), N);
	   H_msg(arr_3N, message_in, msg_len, arr_NBackup); // hash the input message to 32 byte len



	   short needswap_upto = -1;
	   signing = true;
	   //deserialize_state(); // pretty unnecessary here
	   //TODO: following section needs major cleanup
	   short idx = currentIndex;//Util.getShort(sk, (short)2);
	   short signatureOffset = 0; //smlen in reference
	   //gen_wots_signature(apdu, arr_wots_signature);

	   setAddressType(ots_addr, ADDRESS_WOTS);
	   short idx_tree = (short)(currentIndex >> H);
	   short idx_leaf = (short)(currentIndex & ((1 << H)-1));
	   setLayerAddr(ots_addr, s0);
	   setTreeAddr(ots_addr, idx_tree);
	   setOTSAddr(ots_addr, idx_leaf);
	   get_seed(ots_addr, arr_seed_OTS);
	   Util.arrayCopyNonAtomic(message_in, (short)(4*N), signature, (short)(signatureSize-N), N); // copy original input to the end of signature

	   wots_sign(ots_addr, arr_seed_OTS, arr_NBackup, arr_wots_signature, s0);
	   for (short i = 0; i < indexBytes; i++) {
		   signature[i] = (byte) ((currentIndex >> 8*(indexBytes - 1 - i)) & 255);
	   }
	   signatureOffset+=indexBytes;
	   arr_pk_idx[0] = s0;
	   arr_pk_idx[1] = s0;
	   arr_pk_idx[2] = s0;
	   arr_pk_idx[3] = (short)1;
	   Util.arrayCopyNonAtomic(arr_root, s0, arr_pk_idx, (short)4, N);
	   Util.arrayCopyNonAtomic(PUB_SEED, s0, arr_pk_idx, (short)(4+N), N);
	   //signatureOffset+=indexBytes;
	   Util.arrayCopyNonAtomic(arr_R, s0, signature, (short)signatureOffset, N);
	   signatureOffset+=N;
	   Util.arrayCopyNonAtomic(arr_wots_signature, (short)0, signature, signatureOffset, secKeySize); //arr_wots_signature
	   signatureOffset+=secKeySize;
	   Util.arrayCopyNonAtomic(bds_states, bds_off_auth, signature, signatureOffset, (short)(N*H)); //auth path was already computed in previous round
	   signatureOffset+= (short)(N*H);


	   short i, j;
	   Util.arrayFillNonAtomic(arr_address1, s0, N, b0);

		   for (i = 1; i < D; i++) {
			   Util.arrayCopyNonAtomic(wots_sigs, (short)((i-1)*secKeySize), signature, signatureOffset, secKeySize);
			   signatureOffset+=secKeySize;
			   Util.arrayCopyNonAtomic(bds_states, (short)(i*bds_size+bds_off_auth), signature, signatureOffset,(short)(H*N));
			   signatureOffset+=(short)(H*N);
		   }
		   short updates = (short)((H-params_bds_k)>>1);
		   Util.arrayFillNonAtomic(arr_address1, s0, N, b0);
		   setTreeAddr(arr_address1, (short)(idx_tree+1));
		    if ((1 + idx_tree) * (1 << H) + idx_leaf < (1 << H_full_tree)) {
		    	Util.arrayCopyNonAtomic(ots_addr, s0, arr_address2, s0, N); //backup, as ots_addr is altered inside bds_state_update
		        bds_state_update(arr_address1, D, sk_seed, PUB_SEED);// TODO: correct seeds!
		    }
		    for (i = 0; i < D; i++) {
		    	if (false == (((idx + 1) & ((1 << ((i+1)*H)) - 1)) == 0)) {
		            idx_leaf = (short) ((idx >> (H * i)) & ((1 << H)-1));
		            idx_tree = (short) (idx >> (H * (i+1)));
		            setLayerAddr(arr_address1, i);
		            setTreeAddr(arr_address1, idx_tree);
		            if (i ==  (needswap_upto + 1)) {
		                //bds_round(params, &states[i], idx_leaf, sk_seed, pub_seed, addr);
		                bds_round(arr_address1, idx_leaf, i);
		            }
		            //updates = bds_treehash_update(params, &states[i], updates, sk_seed, pub_seed, addr);
		            updates = bds_treehash_update(arr_address1, i, updates);
		            setTreeAddr(arr_address1, (short)(idx_tree + 1));
		            // if a NEXT-tree exists for this level;
		            short test1 = (short)((1 + idx_tree) * (1 << H) + idx_leaf);
		            short test2 = (short)((1 << (H_full_tree - H * i)));
		            if ((1 + idx_tree) * (1 << H) + idx_leaf < (1 << (H_full_tree - H * i))) {
		            	
		                if (i > 0 && updates > 0 && bds_get_next_leaf((short)(D+i)) < (1 << H_full_tree)) {
		                    bds_state_update(arr_address1, (short)(D+i), sk_seed, PUB_SEED); 
		                    updates--;
		                }
		            }
		        }
		    	else if (idx < (1 << H_full_tree) - 1) {
		            deep_state_swap((short)(D + i), i);

		            setLayerAddr(ots_addr, (short)(i+1));
		            setTreeAddr(ots_addr, (short) ((idx + 1) >> ((i+2) * H)));
		            setOTSAddr(ots_addr, (short)((((idx >> ((i+1) * H)) + 1) & ((1 << H)-1))));

		            //get_seed(params, ots_seed, sk+params->index_bytes, ots_addr);
		            get_seed(ots_addr, arr_seed_OTS);
		            //wots_sign(params, wots_sigs + i*params->wots_sig_bytes, states[i].stack, ots_seed, pub_seed, ots_addr);
		            
		            //Util.arrayCopyNonAtomic(bds_states, (short)(i*bds_size+bds_off_stack), arr_N2, s0, N);
//		            Util.arrayCopyNonAtomic(ots_addr, s0, arr_SubtreeRoots, (short)(i*N), N);
//		            Util.arrayCopyNonAtomic(arr_seed_OTS, s0, arr_SubtreeRoots, (short)(i*N+N), N);
//		            Util.arrayCopyNonAtomic(bds_states, (short)(i*bds_size+bds_off_stack), arr_SubtreeRoots, (short)(i*N+2*N), N);
		            
		            Util.arrayCopyNonAtomic(bds_states, (short)(i*bds_size+bds_off_stack), arr_N, s0, N);
		            wots_sign(ots_addr, arr_seed_OTS, arr_N, wots_sigs, (short)(i*secKeySize));
		            //wots_sign(ots_addr, arr_seed_OTS, arr_N2 ,arr_wots_signature);
		            //Util.arrayCopyNonAtomic(arr_wots_signature, s0, signature, signatureOffset, secKeySize);
		            bds_set_stackoffset((short)(D+i), s0);
		            bds_set_next_leaf((short)(D+i), s0);

		            updates--; // WOTS-signing counts as one update
		            needswap_upto = i;
		            for (j = 0; j < H-params_bds_k; j++) {
		            	treehash_set_completed(i, j, (short)1);
		            }
		        }
		    	
		    	
		    }

		    
//	    }
		//Util.arrayCopyNonAtomic(message_in, s0, signature, signatureOffset, N);
	   	signing = false;
	   	currentIndex+=1;
	   }
   private void deep_state_swap(short bds_instance_from, short bds_instance_to ) {
	   Util.arrayCopyNonAtomic(bds_states, (short)(bds_size*bds_instance_to), bds_swap, s0, bds_size);
	   Util.arrayCopyNonAtomic(bds_states, (short)(bds_size*bds_instance_from), bds_states, (short)(bds_size*bds_instance_to), bds_size);
	   Util.arrayCopyNonAtomic(bds_swap, s0, bds_states, (short)(bds_size*bds_instance_from), bds_size);
	   
//	   Util.arrayCopyNonAtomic(arr_SubtreeRoots, (short)(N*bds_instance_to), arr_N, s0, N);
//	   Util.arrayCopyNonAtomic(arr_SubtreeRoots, (short)(N*bds_instance_from), arr_SubtreeRoots, (short)(N*bds_instance_to), N);
//	   Util.arrayCopyNonAtomic(arr_N, s0, arr_SubtreeRoots, (short)(N*bds_instance_from), N);
	   
	   //treehash = new byte[(short)(num_treehash_instances*treehash_instance_length*num_bds_states)];
	   Util.arrayCopyNonAtomic(treehash, (short)(bds_instance_to*states_offset), bds_swap, s0, states_offset);
	   Util.arrayCopyNonAtomic(treehash, (short)(bds_instance_from*states_offset), treehash, (short)(bds_instance_to*states_offset), states_offset);
	   Util.arrayCopyNonAtomic(bds_swap, s0, treehash,(short)(bds_instance_from*states_offset), states_offset);
   }

   
   //BDS SECTION OVER
   //**********************************************************
   private void wots_sign(byte[] addr_l, byte[] seed, byte[]msg, byte[]result, short offset) {

	   chainLengths(msg,arr_key_indices);	//base_W
	   expand_seed(seed, arr_key_uncompressed2);
	   chain(arr_key_uncompressed2, addr_l, arr_key_indices, result, offset);
	   
   }
   
   
   private void expand_seed(byte[] seed, byte[] sk) {
      	
   	//expand_seed
   	Util.arrayFillNonAtomic(arr_N2, (short)0, N, (byte)0);
   	for(short s = 0; s<WOTS_LEN; ++s) {
   		ull_to_bytes(s, (short)32, arr_N2);
   		PRF(seed, arr_N2, sk, (short)(s*N));
   		//Util.arrayCopyNonAtomic(arr_N3, (short)0, sk, (short)(s*N), N);
   	}
   }
   
   
   private void wots_sign_fast(byte[] addr_l, byte[] seed, byte[]msg, byte[]result, short offset) {

	   chainLengths(msg,arr_key_indices);	//base_W
	   expand_seed_and_chain(seed, addr_l, result, offset);
	   //chain(arr_key_uncompressed2, addr_l, arr_key_indices, result, offset);
	   
   }
   private void expand_seed_and_chain(byte[] seed, byte[] addr_l, byte[] sk, short offset) {
     	short index,internal_offset,i,j;
	   	//expand_seed
	   	Util.arrayFillNonAtomic(arr_N2, (short)0, N, (byte)0);
	   	for(short s = 0; s<WOTS_LEN; ++s) {
	   		ull_to_bytes(s, (short)32, arr_N2);
	   		PRF(seed, arr_N2, arr_N3, s0);
	   		index = arr_key_indices[s];
	   		internal_offset = (short)(s*N);
	   		//chain sk directly
	   		if(index!=0) {
        		Util.arrayCopy(arr_N3, s0, arr_4N, (short)(2*N), N);
        		setOtsChainAddr(addr_l, s);
    			for(short w = 0; w<index; w++) {	//calc chains hash values
    				setOtsHashAddr(addr_l, w);
    				setKeyAndMask(addr_l, KAM_SET_KEY);
//    				secretSeed.getKey(arr_N3, (short)0); //get SEED 
    				PRF(PUB_SEED, addr_l, arr_4N, N); //result = KEY
    				setKeyAndMask(addr_l, KAM_SET_BITMASK_LEAST);
    				PRF(PUB_SEED, addr_l, arr_bitmask1, s0);
    				
    		        /* Mask the input */
    		        for ( i = (short)(2*N), j=0; j < ADDRESS_SIZE; i++, j++) {
    		            arr_4N[i] ^= arr_bitmask1[j];
    		        }
    		        md256.doFinal(arr_4N, s0, (short)(3*N), arr_4N, (short)(2*N));
    		        //F(arr_N, arr_N2, arr_N2); // hash input message directly into output buffer
    		           		        
    				//hashIt(arr_N2, (short)(N*8), arr_N);
    				//Util.arrayCopyNonAtomic(arr_N, (short)0, arr_N2, (short)0, N); //result = next iteration input  			
    			}
    			Util.arrayCopy(arr_4N, (short)(2*N), sk, (short)(internal_offset+offset), N);//store node in result
    		}
    		else { // in case index==0 -> use sk segment directly
    			Util.arrayCopy(arr_N3, s0, sk, (short)(s*N+offset), N);
    		}
	   		//Util.arrayCopyNonAtomic(arr_N3, (short)0, sk, (short)(s*N), N);
	   	}
}
   

   
   //Debug Function, generates sk & pk for given index
   //TODO: Maybe use function similar to this to calculate pk for ltree construction. Optimize by not storing intermediate results
    private short wotsPairAtIDX(byte[] address, byte[] sk, byte[] pk){
    	//calculate Seed at index
    	//byte[] seed = new byte[secSeedSize];
    	//get_seed(address, secretSeed, arr_seed);
    	
    	//generate SK from seed
    	byte[] wotsSK = new byte[secKeySize];
    	//genSk(arr_seed, wotsSK);
    	
    	//calculate hash values and PK
    	Util.arrayCopy(wotsSK, (short)0, sk, (short)0, secKeySize);
    	short sklen = (short)(secKeySize*WOTS_W);
    	byte[] results = new byte[sklen];
    	for(short s = 0; s<WOTS_LEN; ++s) {
    		arr_key_indices[s] = WOTS_W-1;
    	}
    	
    	chain(sk, address, arr_key_indices, results, s0);
    	
    	//results are stored in format |chainNodes(W*len_n0)|chainNodes(W*len_n1)|...
    	//return results
    	Util.arrayCopy(results, (short)(N*WOTS_LEN*WOTS_W-N), pk, (short)0, N);
    	
		return 0;
    }
    
    

    /*
     * calculates the L-tree root of the specified WOTS-Address 
     * result offset = offset within ltree_root, where result is written
     */
    //similar to gen_leaf_wots
    public void get_xmss_leaf(byte[] WOTS_address, byte[]ltree_root, short result_offset) {
    	short nextIndex = Util.getShort(WOTS_address, (short)18); // save index for l-tree construction	
    	//calculate Seed at index
    	//get_seed(WOTS_address, secretSeed, arr_seed);
    	
    	//generate SK
    	genSk(WOTS_address, arr_key_uncompressed);
    	
    	//calculate hash values and PK
    	//chain(arr_key_uncompressed, arr_wots_nodes, WOTS_W);
    	
    	for(short s = 0; s<WOTS_LEN; ++s) {
    		arr_key_indices[s] = (short)(WOTS_W-1);
    	}
    	
    	
    	chain(arr_key_uncompressed, WOTS_address, arr_key_indices, arr_key_uncompressed2, s0);
    	//arr_key_uncompressed2 = pk at wots_address
    	
    	//short pos = getAddressByte(arr_address1, (short)1);
        //genAddress(ADDRESS_WOTS, pos, (short)0, arr_address1);
    	Util.arrayFillNonAtomic(arr_N2, (short)0, N, (byte)0);
    	setOtsChainAddr(WOTS_address, s0);
    	setLTreeAddr(WOTS_address, nextIndex);
    	setAddressType(WOTS_address, ADDRESS_L_TREE);
    	setLTreeIdx(WOTS_address,s0);
    	setKeyAndMask(WOTS_address, KAM_SET_KEY);
    	//TODO:address needs to be properly created 
        gen_ltree(WOTS_address, arr_key_uncompressed2, ltree_root, result_offset);
        setAddressType(WOTS_address, ADDRESS_WOTS);
    }
    


    
    
    /*
     * Calculates all nodes underneath the specified address and stores them in result.
     * address needs to be a valid XMSS Address, result needs to be of 2^(address.height+1)-1 * ADDRESS_SIZE bytes length
     * The nodes found can then be used to check weather they where already computed and have transiently stored results
     */
    public void nodes_underneath(byte[] address, byte[]result) {
     //TODO: Really necessary? Nodes should be computable from address alone, no need for expensive arraycopys etc.	
    }
    
    
    public void getChildren(byte[] parentAddr, byte[]leftChild, byte[] rightChild) {
    	
//    	short type = getAddressType(parentAddr);
//    	short levelIndex = getAddressByte(parentAddr, (short)1);
//    	short parentHeight = getAddressByte(parentAddr, (short)2);
    	Util.arrayCopyNonAtomic(parentAddr, (short)0, leftChild, (short)0, ADDRESS_SIZE);
    	Util.arrayCopyNonAtomic(parentAddr, (short)0, rightChild, (short)0, ADDRESS_SIZE);
    	short treeHeight = Util.getShort(parentAddr, (short)22);
    	short treeIndex = Util.getShort(parentAddr, (short)26);
    	
    	treeHeight = (short)(treeHeight-1);
    	leftChild[22] = (byte)(((treeHeight) >> 8) & 0xff); 
    	leftChild[23] = (byte)((treeHeight)& 0xff);
    	rightChild[22] = (byte)(((treeHeight) >> 8) & 0xff); 
    	rightChild[23] = (byte)((treeHeight)& 0xff);
    	
    	treeIndex = (short)(treeIndex*2);
    	leftChild[26] = (byte)(((treeIndex) >> 8) & 0xff); 
    	leftChild[27] = (byte)((treeIndex)& 0xff);
    	rightChild[26] = (byte)(((treeIndex+1) >> 8) & 0xff); 
    	rightChild[27] = (byte)((treeIndex+1)& 0xff);
    }
    /*
     * Simple Tree traversal algorithm.
     * Calculates all other pubKeys and nodes from scratch every time.
     * 
     */
    public void tt_simple(){
    	
    }
    
    
  
    /*
     * LOCAL: 	arr_key_uncompressed
     * 			arr_key_uncompressed2 
     */
    public void gen_wots_signature(APDU apdu, byte[] wots_signature) {
    	byte buffer[] = apdu.getBuffer(); 	
    	short msg_len = apdu.getIncomingLength();
    	//byte[] message_in = new byte[len];
    	Util.arrayCopy(buffer, (short)ISO7816.OFFSET_CDATA, message_in, (short)(4*N), msg_len);	// message_in = actual message to sign
    	//short[] bases = new short[WOTS_LEN];	//stores calculated bases for message 
    	//baseFunction(message, (short)0, WOTS_LEN, arr_key_indices);			//bases now contains WOTS_LEN short values

    	//r
    	//initialize r = arr_N2
    	//key:
    	Util.arrayFillNonAtomic(arr_N3, (short)0, N, (byte)0);
    	arr_N3[N-2] = (byte)((currentIndex >> 8) & 0xff); 
    	arr_N3[N-1] = (byte)(currentIndex & 0xff);
    	//arr_N = secretSeed
    	//arr_N3 = currentIndex in bytes
    	//PRF(secretSeed, index )
    	PRF(arr_N, arr_N3, arr_N2, s0); //r = arr_N2
    	Util.arrayCopyNonAtomic(arr_N2, s0, arr_R, s0, N); // save R for final signature 
    	
    	//root:
    	//key(3n) = r(n)   + root(n) + tobyte_tmp(n)
    	// here:	arr_N2	 arr_root  arr_N3
    	Util.arrayCopyNonAtomic(arr_N2, (short)0, arr_3N, (short)0, N);
    	Util.arrayCopyNonAtomic(arr_root, (short)0, arr_3N, (short)N, N);
    	Util.arrayCopyNonAtomic(arr_N3, (short)0, arr_3N, (short)(2*N), N);
    	H_msg(arr_3N, message_in, msg_len, arr_N2); // hash the input message to 32 byte len
    	chainLengths(arr_N2,arr_key_indices); 
        
        //genAddress(ADDRESS_WOTS, currentIndex, (short)0, arr_address1);//stores currently used WOTS-Signature
        Util.arrayFillNonAtomic(arr_address1, (short)0, ADDRESS_SIZE, (byte)0);

		
		//type
    	arr_address1[14] = (byte)((ADDRESS_WOTS >> 8) & 0xff); 
    	arr_address1[15] = (byte)(ADDRESS_WOTS & 0xff);
    	
    	//OTS address - index
    	arr_address1[18] = (byte)((currentIndex >> 8) & 0xff); 
    	arr_address1[19] = (byte)(currentIndex & 0xff);
    	
         
        genSk(arr_address1, arr_key_uncompressed2);
        chain(arr_key_uncompressed2, arr_address1, arr_key_indices, wots_signature, s0);
        //gen_ltree(arr_address1, wots_signature, pubKey);
    }
    
    //same as get_seed in xmss_reference - takes address and generates n-byte seed
    public void get_seed(byte[] addr_l, byte[] result){
    	//Make sure that chain, hash and kam bytes are zeroed
    	Util.arrayFillNonAtomic(addr_l, (short)20, (short)12, b0); //Null chain-, hash address and KAM
    	Util.arrayFillNonAtomic(arr_N, (short)0, N, (byte)0);
    	
    	//  seed   address
    	PRF(sk_seed, addr_l, result, s0);
    }
    
     //L-Tree function
    //compress wots pk from (WOTS_LEN*N) to N bytes
    public void gen_ltree(byte[] addr ,byte[] uncompressed_pk, byte[] result, short result_offset){
    	short l = WOTS_LEN;
    	short parent_nodes, s;
    	short height = 0;
    	
    	
    	setTreeHeight(addr, (short)0);
    	parent_nodes = (short)(l >> 1);
    	//while((parent_nodes*2*N) > remaining_ram) {
    	short counter = 0;
    	while(l > 1) {
    		
    		for(s = 0; s < parent_nodes; ++s) {
    			setTreeIndex(addr, s);
    			Util.arrayCopyNonAtomic(uncompressed_pk, (short)(2*s*N), arr_2N, (short)0, (short)(2*N));
    			RAND_HASH(arr_2N, PUB_SEED, addr, uncompressed_pk, (short)(s*N));
    			counter +=1; 
    			//Util.arrayCopyNonAtomic(arr_N3, (short)0, uncompressed_pk, (short)(s*N), N);
    		}
    		if(l % 2 != 0) {	// odd number of nodes
    			Util.arrayCopyNonAtomic(uncompressed_pk, (short)((l-1)*N), uncompressed_pk, (short)((l >> 1)*N), N);
    			l = (short)((l >> 1) + 1);
    		}else {
    			l = (short)(l >> 1);
    		}
    		height++;
    		setTreeHeight(addr, height);
    		parent_nodes = (short) (l >> 1);
    	}
    Util.arrayCopyNonAtomic(uncompressed_pk, s0, result, result_offset, N);
//    	Util.arrayCopyNonAtomic(uncompressed_pk, s0, arr_stack, s0, (short)((parent_nodes*2*N)));
//    	while(l > 1) {
//    		parent_nodes = (short)(l >> 1);
//    		for (s = 0; s < parent_nodes; ++s) {
//    			setTreeIndex(addr, s); 
//    			Util.arrayCopyNonAtomic(arr_stack, (short)(2*s*N), arr_2N, (short)0, (short)(2*N));
//    			RAND_HASH(arr_2N, arr_N, addr, arr_stack, (short)(s*N));
//    			//Util.arrayCopyNonAtomic(arr_N3, (short)0, uncompressed_pk, (short)(s*N), N);
//    		}
//    		if(l % 2 != 0) {	// odd number of nodes
//    			Util.arrayCopyNonAtomic(arr_stack, (short)((l-1)*N), arr_stack, (short)((l >> 1)*N), N);
//    			l = (short)((l >> 1) + 1);
//    		}else {
//    			l = (short)(l >> 1);
//    		}
//    		height++;
//    		setTreeHeight(addr, height);
//    	} 
//    	Util.arrayCopyNonAtomic(arr_stack, (short)0, result, (short)result_offset, N);
    	
    	
    	

    }
    
    //similar to RAND_HASH from RFC but using combined array for nodes 
    // Input: leftandRight = 2N bytes;  l_seed = n bytes;  l_addr = n bytes
    // Output: result = N bytes
    public void RAND_HASH(byte[] leftandRight, byte[] l_seed, byte[] l_addr, byte[] result, short offset ) {
    	setKeyAndMask(l_addr, KAM_SET_KEY);
    	PRF(l_seed, l_addr, arr_N4, s0); // ==KEY
    	setKeyAndMask(l_addr, KAM_SET_BITMASK_LEAST);
    	PRF(l_seed, l_addr, arr_bitmask1, s0);
    	setKeyAndMask(l_addr, KAM_SET_BITMASK_MOST);
    	PRF(l_seed, l_addr, arr_bitmask2, s0);
    	
    	for(short s = 0; s<N; ++s) { // xor bitmasks with nodes
    		leftandRight[s] ^= arr_bitmask1[s];
    		leftandRight[(short)(s+N)] ^= arr_bitmask2[s];
    	}
    	H(arr_N4, leftandRight, result, offset);
    }
    
    public void setLayerAddr(byte[] address, short layer) {
    	
    	address[2] = (byte)((layer >> 8) & 0xff); 
    	address[3] = (byte)(layer & 0xff);
    }
    public void setTreeAddr(byte[] address, short value) {
    	Util.arrayFillNonAtomic(address, (short)4, (short)8, b0);
    	address[10] = (byte)((value >> 8) & 0xff);
    	address[11] = (byte)(value & 0xff); 
    }
    public void setAddressType(byte[] address, short type) {
    	address[14] = (byte)((type >> 8) & 0xff); 
    	address[15] = (byte)(type & 0xff);
    }
    public void setTreeHeight(byte[] address, short treeIndex) {
    	address[22] = (byte)((treeIndex >> 8) & 0xff); 
    	address[23] = (byte)(treeIndex & 0xff);
    }
    public void setTreeIndex(byte[] address, short treeIndex) {
    	address[26] = (byte)((treeIndex >> 8) & 0xff); 
    	address[27] = (byte)(treeIndex & 0xff);
    }
    public void setTreePadding(byte[] address) {
    	address[16] = (byte)0;
    	address[17] = (byte)0;
    	address[18] = (byte)0; 
    	address[19] = (byte)0;
    }
    public void setKeyAndMask(byte[] address, short keyAndMask) {
    	address[30] = (byte)((keyAndMask >> 8) & 0xff); 
    	address[31] = (byte)(keyAndMask & 0xff);
    }
    public void setLTreeIdx(byte[] address, short index) {
    	address[26] = (byte)((index >> 8) & 0xff); 
    	address[27] = (byte)(index & 0xff);
    }
    public void setLTreeAddr(byte[] address, short index) {
    	address[18] = (byte)((index >> 8) & 0xff); 
    	address[19] = (byte)(index & 0xff);
    }
    public void setLTreeLvl(byte[] address, short height) {
    	address[22] = (byte)((height >> 8) & 0xff); 
    	address[23] = (byte)(height & 0xff);
    }
    public void setOtsHashAddr(byte[] address, short hashNbr) {
    	address[26] = (byte)((hashNbr >> 8) & 0xff);
    	address[27] = (byte)(hashNbr & 0xFF);
    }
    public void setOtsChainAddr(byte[] address, short chainNbr) {
    	address[22] = (byte)((chainNbr >> 8) & 0xff);
    	address[23] = (byte)(chainNbr & 0xFF);
    }
    public void setOTSAddr(byte[] address, short index) {
    	address[18] = (byte)((index >> 8) & 0xff); 
    	address[19] = (byte)(index & 0xff);
    }
    private short getLayerAddress(byte[] addr_l) {
 	   return Util.getShort(addr_l, (short) 2);
    }


 
    //Input: key = 3N bytes; input = arbitrary length;
    //Output: result = N bytes
    public void F(byte[] l_key, byte[] input, byte[] result) {
    	Util.arrayFillNonAtomic(arr_3N, (short)0, (short)(3*N), (byte)0);
    	Util.arrayCopyNonAtomic(l_key, (short)0, arr_3N, (short)N, N);
    	Util.arrayCopyNonAtomic(input, (short)0, arr_3N, (short)(2*N), N);
        md256.doFinal(arr_3N, (short)0, (short)(3*N), result, (short) 0);
    }
    public void H(byte[] l_key, byte[] input, byte[] result, short offset) {
    	Util.arrayFillNonAtomic(arr_4N, (short)0, (short)(3*N), (byte)0);
    	arr_4N[N-2] = (byte)((1 >> 8) & 0xff); 
    	arr_4N[N-1] = (byte)(1 & 0xff);
    	Util.arrayCopyNonAtomic(l_key, (short)0, arr_4N, (short)N, N);
    	Util.arrayCopyNonAtomic(input, (short)0, arr_4N, (short)(2*N), (short)(2*N));
        md256.doFinal(arr_4N, (short)0, (short)(4*N), result,  offset);
    }   
    
    
    //INPUT: key = 3byte  input = max MAX_MESSAGE_LENGTH
    //OUTPUT: result = N bytes
    //input here already contains the message, so only the leading 4*N bytes need to be filled
    //similar to hash_message in ref
	//l_key(3n) = r(n)   + arr_root(n) + tobyte_tmp(n)
    public void H_msg(byte[] l_key, byte[] input, short msg_len, byte[] result) {
    	Util.arrayFillNonAtomic(input, (short)0, (short)(4*N), (byte)0);
    	input[N-2] = (byte)((2 >> 8) & 0xff); 
    	input[N-1] = (byte)(2 & 0xff);
    	Util.arrayCopyNonAtomic(l_key, (short)0, input, (short)N, (short)(3*N));
        md256.doFinal(input, (short)0, (short)(msg_len + 4*N), result, (short) 0);
    }
    
    
    
    //PRF takes as input an n-byte key and a 32-byte index and generates pseudorandom outputs of length n
    //-> Input: key = n bytes, index = l_addr = 32byte
    public void PRF(byte[]l_key, byte[]l_addr, byte[] result, short offset) {
    	Util.arrayFillNonAtomic(arr_3N, (short)0, (short)(3*N), (byte)0);    	//internally, use a 2*N + 32 byte = 3N byte buffer 
    	arr_3N[N-2] = (byte)((3 >> 8) & 0xff); // XMSS_HASH_PADDING_PRF == 3 as domain seperator
    	arr_3N[N-1] = (byte)(3 & 0xff);
    	Util.arrayCopyNonAtomic(l_key, (short)0, arr_3N, (short)N, N); //concatenate key and address as PRF input 
    	Util.arrayCopyNonAtomic(l_addr, (short)0, arr_3N, (short)(2*N), N);
    	//md256.reset();
        md256.doFinal(arr_3N, (short)0, (short)(3*N), result, (short) offset);	
    }
    //arr_3N as input[N byte nulled| Nbyte key|Nbyte addr]
    private void PRF_fast(byte[]input, byte[] result, short offset) {
    	Util.arrayFillNonAtomic(arr_3N, (short)0, (short)(3*N), (byte)0);    	//internally, use a 2*N + 32 byte = 3N byte buffer 
    	arr_3N[N-2] = (byte)((3 >> 8) & 0xff); // XMSS_HASH_PADDING_PRF == 3 as domain seperator
    	arr_3N[N-1] = (byte)(3 & 0xff);
    }
    

    /* Takes a message and derives the corresponding chain lengths.
    Required space in tmp is inherited from WOTSChecksum. */
    private void chainLengths(byte[] message,short[] indices){
    	baseW(indices, (short)0, message, (short)0, WOTS_LEN1);
    	WOTSChecksum(indices, indices);
    }
    /* Converts msg to baseW and writes results to lengths array, for the
purpose of creating WOTS chains. */
    private void baseW(short[] lengths, short offset,
    		byte[] msg, short o_msg, short msgLength) {
    	short in = 0;
    	short out = 0;
    	byte total = 0;
    	short bits = 0;
    	short consumed;

    	for (consumed = 0; consumed < msgLength; consumed++) {
    		if (bits == 0) {
    			total = msg[(short)(o_msg + in)];
    			in++;
    			bits += 8;
    		}
    		bits -= WOTS_LOG_W;
    		lengths[(short)(out + offset)] = (short)((total >>> bits) & (WOTS_W - 1));
    		out++;
    	}
    }

    /* Computes the WOTS+ checksum over a message (in base WOTS_W).
Requires 2 bytes of temporary data in tmp. */
    private void WOTSChecksum(short[] indices, short[] message) {
    	short csum = 0; /* At most WOTS_LEN1 * WOTS_W */
    	short i;

    	/* Compute checksum */
    	for (i = 0; i < WOTS_LEN1; i++) {
    		csum += (short)(WOTS_W - 1 - message[i]);
    	}

    	/* Ensure expected empty (zero) bits are the least significant bits. */
    	csum <<= (short)((8 - ((short)(WOTS_LEN2 * WOTS_LOG_W) % 8)));
    	/* For W = 16 N = 32, the checksum fits in 10 < 15 bits */
    	Util.setShort(arr_stack, (short)0, csum);

    	/* Convert checksum to base W */
    	baseW(indices, WOTS_LEN1, arr_stack, (short)0, WOTS_LEN2);
    }

    //maybe abandon use separate function 
    // gets hashes pointed at inside "bases" from "wots_nodes" 
    public void nodesFromHashes(byte[]wots_nodes, short[] bases, byte[] hashes){
        for(short i = 0; i<WOTS_LEN; ++i){
            //W*N = distance to next segment
            Util.arrayCopy(wots_nodes,(short)(N*bases[i]+i*WOTS_W*N), hashes, (short)(i*N) , N);
        }
    }
    
    private void addr_conv_toReference(byte[] own_addr, byte[] ref_addr) {
    	/*	+-------------------------+
    	 * 	| layer address  (32 bits)|	height of tree within multi tree structure, 0 in single tree
    	 *	+-------------------------+ 
    	 *	Leaving it at 0 for now for single tree variant
    	 */ 
    	short layer_address = 0;
    	ref_addr[0] = (byte)((layer_address >> 8) & 0xff);
    	ref_addr[1] = (byte)(layer_address & 0xff);
    	
    	/*	+-------------------------+
		 * 	| tree address   (64 bits)| position within layer of MT, starting at 0 for leftmost tree
		 *	+-------------------------+ 
		 *	Leaving it at 0 for now for single tree variant
		 */
    	ref_addr[2] = (byte)(0x00);
    	ref_addr[3] = (byte)(0x00);
    	ref_addr[4] = (byte)(0x00);
    	ref_addr[5] = (byte)(0x00);
    	
    	
    	/*
    	+-------------------------+
    	| type = 0       (32 bits)|
    	+-------------------------+
    	*/
    	short type = getAddressType(own_addr);
    	ref_addr[6] = (byte)((type >> 8) & 0xff);
    	ref_addr[7] = (byte)(type & 0xff);
    	
    	if(type == ADDRESS_WOTS) {
    		ref_addr[8] = (byte)0x00;
    		ref_addr[9] = (byte)0x00;
    		short index = getAddressByte(own_addr, (short)1);
    		ref_addr[10] = (byte)((index >> 8) & 0xff);
        	ref_addr[11] = (byte)(index & 0xff);
    	}
    	
    	
    	
    }
    
    public void genAddress(short dummy, short type, short byte1, short byte2, byte[]addr) {
    	/* WOTS							L-tree						Hash-Tree
     
        +-------------------------+	+-------------------------+	+-------------------------+  
        | layer address  (32 bits)|	| layer address  (32 bits)|	| layer address  (32 bits)| 	height of tree
        +-------------------------+ +-------------------------+ +-------------------------+	  
        | tree address   (64 bits)| | tree address   (64 bits)| | tree address   (64 bits)|		position within layer of MT
        +-------------------------+ +-------------------------+ +-------------------------+
        | type = 0       (32 bits)|	| type = 1       (32 bits)| | type = 2       (32 bits)|		type: 0=wots 1=l-tree 2=hash
        +-------------------------+ +-------------------------+ +-------------------------+
        | OTS address    (32 bits)| | L-tree address (32 bits)|	| Padding = 0    (32 bits)|		OTS: index within tree  
        +-------------------------+ +-------------------------+ +-------------------------+
        | chain address  (32 bits)|	| tree height    (32 bits)|	| tree height    (32 bits)|		
        +-------------------------+ +-------------------------+ +-------------------------+
        | hash address   (32 bits)| | tree index     (32 bits)| | tree index     (32 bits)|		
        +-------------------------+ +-------------------------+ +-------------------------+
        | keyAndMask     (32 bits)| | keyAndMask     (32 bits)| | keyAndMask     (32 bits)|		
        +-------------------------+ +-------------------------+ +-------------------------+
    	
    	*/
    	
    	//type
    	addr[0] = (byte)((type >> 8) & 0xff);
    	addr[1] = (byte)(type & 0xff);
    	
    	//nodeNbr
    	addr[2] = (byte)((byte1 >> 8) & 0xff);
    	addr[3] = (byte)(byte1 & 0xff);
    	
    	addr[4] = (byte)((byte2 >> 8) & 0xff);
    	addr[5] = (byte)(byte2 & 0xff);
    	
    	
    }

    // generate address, arguments depend on type
    // Avoid modifying or handling addresses directly and without using the functions below!
    // This should prevent bugs if the address scheme is modified later on
    public void genAddress(short type, short customByte1, short customByte2, short customByte3, short customByte4, byte[]addr) {
    	/* WOTS							L-tree						Hash-Tree
     
        +-------------------------+	+-------------------------+	+-------------------------+  
        | layer address  (32 bits)|	| layer address  (32 bits)|	| layer address  (32 bits)| 	height of tree within multi tree structure, 0 in single tree
        +-------------------------+ +-------------------------+ +-------------------------+	  
        | tree address   (64 bits)| | tree address   (64 bits)| | tree address   (64 bits)|		position within layer of MT, starting at 0 for leftmost tree
        +-------------------------+ +-------------------------+ +-------------------------+
        | type = 0       (32 bits)|	| type = 1       (32 bits)| | type = 2       (32 bits)|		type: 0=wots 1=l-tree 2=hash
        +-------------------------+ +-------------------------+ +-------------------------+
        | OTS address    (32 bits)| | L-tree address (32 bits)|	| Padding = 0    (32 bits)|		OTS: index within tree  
        +-------------------------+ +-------------------------+ +-------------------------+
        | chain address  (32 bits)|	| tree height    (32 bits)|	| tree height    (32 bits)|		
        +-------------------------+ +-------------------------+ +-------------------------+
        | hash address   (32 bits)| | tree index     (32 bits)| | tree index     (32 bits)|		
        +-------------------------+ +-------------------------+ +-------------------------+
        | keyAndMask     (32 bits)| | keyAndMask     (32 bits)| | keyAndMask     (32 bits)|		
        +-------------------------+ +-------------------------+ +-------------------------+
    	
    	
    	*/
    	
    	/*	+-------------------------+
    	 * 	| layer address  (32 bits)|	height of tree within multi tree structure, 0 in single tree
    	 *	+-------------------------+ 
    	 *	Leaving it at 0 for now for single tree variant
    	 */ 
    	short layer_address = 0;
    	addr[0] = (byte)((layer_address >> 8) & 0xff);
    	addr[1] = (byte)(layer_address & 0xff);
    	
		/*	+-------------------------+
		 * 	| tree address   (64 bits)| position within layer of MT, starting at 0 for leftmost tree
		 *	+-------------------------+ 
		 *	Leaving it at 0 for now for single tree variant
		 */
    	addr[2] = (byte)(0x00);
    	addr[3] = (byte)(0x00);
    	addr[4] = (byte)(0x00);
    	addr[5] = (byte)(0x00);
    	
    	/*
    	+-------------------------+
    	| type = 0       (32 bits)|
    	+-------------------------+
    	*/
    	addr[6] = (byte)((type >> 8) & 0xff);
    	addr[7] = (byte)(type & 0xff);
    	
		/*
		+-------------------------+  +-------------------------+  +-------------------------+
		|  OTS address    (32 bits)| | L-tree address (32 bits)|  | Padding = 0    (32 bits)|
		+-------------------------+  +-------------------------+  +-------------------------+
		 */
		addr[8] = (byte)((customByte1 >> 8) & 0xff);
    	addr[9] = (byte)(customByte1 & 0xff);
    	addr[10] = (byte)((customByte1 >> 8) & 0xff);
    	addr[11] = (byte)(customByte1 & 0xff);
    	
    	/*
		+-------------------------+  +-------------------------+  +-------------------------+
    	| chain address  (32 bits)|	| tree height    (32 bits)|	| tree height    (32 bits)|
    	+-------------------------+  +-------------------------+  +-------------------------+
		 */
		addr[12] = (byte)((customByte2 >> 8) & 0xff);
    	addr[13] = (byte)(customByte2 & 0xff);
    	addr[14] = (byte)((customByte2 >> 8) & 0xff);
    	addr[15] = (byte)(customByte2 & 0xff);
    	
    	/*
    	+-------------------------+ +-------------------------+ +-------------------------+
        | hash address   (32 bits)| | tree index     (32 bits)| | tree index     (32 bits)|		
        +-------------------------+ +-------------------------+ +-------------------------+
    	*/
    	addr[16] = (byte)((customByte3 >> 8) & 0xff);
    	addr[17] = (byte)(customByte3 & 0xff);
    	addr[18] = (byte)((customByte3 >> 8) & 0xff);
    	addr[19] = (byte)(customByte3 & 0xff);
    	
    	/*
    	+-------------------------+ +-------------------------+ +-------------------------+
    	| keyAndMask     (32 bits)| | keyAndMask     (32 bits)| | keyAndMask     (32 bits)|		
    	+-------------------------+ +-------------------------+ +-------------------------+
    	 */
    	addr[20] = (byte)((customByte4 >> 8) & 0xff);
    	addr[21] = (byte)(customByte4 & 0xff);
    	addr[22] = (byte)((customByte4 >> 8) & 0xff);
    	addr[23] = (byte)(customByte4 & 0xff);
        	    	
    }
    
    public short getAddressType(byte[] addr) {
    	return Util.getShort(addr, (short)0);	
    }
    
    //byteNbr ==1 means first byte after addr type 
    public short getAddressByte(byte[] addr, short byteNbr) {
    	return Util.getShort(addr, (short)(byteNbr*2));
    }
    
    public short getShortAtByte(byte[] addr, short byteNbr) {
    	return Util.getShort(addr, (short)(byteNbr));
    }
    
    
    //converts XMSS leaf address to WOTS index and vice versa
    //TODO: would be unnecessary if pos on level and height bytes in XMSS Address was swapped, consider doing so
    // UPDATE: bytes were swapped, type needs to be changed nonetheless. Keep function to ease later changes in addresses 
    public void convertAddress(short dummy, byte[] address){
    	
    	short type = getAddressType(address);
    	short byte0 = getAddressByte(address, (short)1);
    	if(type == ADDRESS_XMSS_TREE && byte0 == 0) { //only if leaf-node = byte1 == 0
    		//genAddress(ADDRESS_WOTS, byte0, (short)0, address);
    	}
    	else if(type == ADDRESS_WOTS) {
    		//genAddress(ADDRESS_XMSS_TREE, byte0, (short)0, address);
    	}else {
    		// TODO throw some error
    	}
    	
    }
    
    

    

    


    public void genBitmask(byte address[], short offset, byte[]bitmask){
    	//TODO:Seems the size of address is too small. 
    	// Implement Bitmask creation properly  
    	Util.arrayCopyNonAtomic(address, (short)0, arr_N, (short)0, ADDRESS_SIZE);
    	Util.arrayFillNonAtomic(arr_N, ADDRESS_SIZE, (short)(N-ADDRESS_SIZE), (byte)0);
//    	RandomData randomness = RandomData.getInstance(RandomData.ALG_PSEUDO_RANDOM);
    	randomness.setSeed(arr_N, (short)0, secSeedSize);
    	
        randomness.generateData(bitmask, (short)offset, (short)(N));	
    	
    }
    
    //implemented similar to wots_pkgen in xmss reference:
    private void genSk(byte[] address, byte[] sk) {
    	//MessageDigest secretKey = MessageDigest.getInstance(MessageDigest.ALG_SHA_256, false);
    	//RandomData randomness = RandomData.getInstance(RandomData.ALG_PSEUDO_RANDOM);
    	/*
    	 * The private key elements are computed as sk[i] = PRF(S, toByte(i, 32)) whenever needed.
    	 */
    	get_seed(address, arr_N); //arr_N = seed used for wots_pkgen
    	
    	//expand_seed
    	Util.arrayFillNonAtomic(arr_N2, (short)0, N, (byte)0);
    	for(short s = 0; s<WOTS_LEN; ++s) {
    		ull_to_bytes(s, (short)32, arr_N2);
    		PRF(arr_N, arr_N2, arr_N3, s0);
    		Util.arrayCopyNonAtomic(arr_N3, (short)0, sk, (short)(s*N), N);
    	}
    }
    
    void ull_to_bytes(short in, short outlen, byte[] out)
    {
    	/* Iterate over out in decreasing order, for big-endianness. */
    	for (short i = (short)(outlen - 1); i >= 0; i--) {
    		out[i] = (byte) (in & 0xff);
    		in = (short) (in >> 8);
    	}
    }
    

    // CHAINING FUNCTION
    //computes a chain node pointed to by each element in index on secKey 
    //indices needs to be of length WOTS_LEN
    private void chain(byte[] secKey, byte[] addr_l, short[] indices, byte[] result, short offset) {
    	//byte[] input = new byte[N];
    	//Util.arrayCopy(sk,  (short)0, input, (short)0, N); //first N bytes as starting input
    	//byte[] temp = new byte[N];	//intermediate chain node
    	short internal_offset;
    	short i, j, len, index;
    	Util.arrayFillNonAtomic(arr_4N, s0, N, b0);
    	for( len = 0; len<WOTS_LEN; len++) {	 	//iterate over len segments
    		index = indices[len];
    		if(index!=0) {
    			internal_offset = (short)(len*N);
        		Util.arrayCopy(secKey, internal_offset, arr_4N, (short)(2*N), N);
        		setOtsChainAddr(addr_l, len);
    			for(short w = 0; w<index; w++) {	//calc chains hash values
    				setOtsHashAddr(addr_l, w);
    				setKeyAndMask(addr_l, KAM_SET_KEY);
//    				secretSeed.getKey(arr_N3, (short)0); //get SEED 
    				PRF(PUB_SEED, addr_l, arr_4N, N); //result = KEY
    				setKeyAndMask(addr_l, KAM_SET_BITMASK_LEAST);
    				PRF(PUB_SEED, addr_l, arr_bitmask1, s0);
    				
    		        /* Mask the input */
    		        for ( i = (short)(2*N), j=0; j < ADDRESS_SIZE; i++, j++) {
    		            arr_4N[i] ^= arr_bitmask1[j];
    		        }
    		        md256.doFinal(arr_4N, s0, (short)(3*N), arr_4N, (short)(2*N));
    		        //F(arr_N, arr_N2, arr_N2); // hash input message directly into output buffer
    		           		        
    				//hashIt(arr_N2, (short)(N*8), arr_N);
    				//Util.arrayCopyNonAtomic(arr_N, (short)0, arr_N2, (short)0, N); //result = next iteration input  			
    			}
    			Util.arrayCopy(arr_4N, (short)(2*N), result, (short)(internal_offset+offset), N);//store node in result
    		}
    		else { // in case index==0 -> use sk segment directly
    			Util.arrayCopy(secKey, (short)(len*N), result, (short)(len*N+offset), N);
    		}
    			
    		
    		
    	} 
    }
    private void chain_fast(byte[] secKey, byte[] addr_l, short[] indices, byte[] result, short offset) {
    	//byte[] input = new byte[N];
    	//Util.arrayCopy(sk,  (short)0, input, (short)0, N); //first N bytes as starting input
    	//byte[] temp = new byte[N];	//intermediate chain node
    	short internal_offset;
    	short i, j, len, index;
    	Util.arrayFillNonAtomic(arr_4N, s0, N, b0);
    	Util.arrayFillNonAtomic(arr_3N, s0, (short)(3*N), b0);
    	Util.arrayCopyNonAtomic(PUB_SEED, s0, arr_3N, (short)N , N);
    	arr_3N[N-2] = (byte)((3 >> 8) & 0xff); // XMSS_HASH_PADDING_PRF == 3 as domain seperator
    	arr_3N[N-1] = (byte)(3 & 0xff);
    	for( len = 0; len<WOTS_LEN; len++) {	 	//iterate over len segments
    		index = indices[len];
    		if(index!=0) {
    			internal_offset = (short)(len*N);
        		Util.arrayCopy(secKey, internal_offset, arr_4N, (short)(2*N), N);
        		//setOtsChainAddr(addr_l, len);
        		Util.setShort(arr_3N, (short)(2*N+22), len);
    			for(short w = 0; w<index; w++) {	//calc chains hash values
    				//setOtsHashAddr(addr_l, w);
    				Util.setShort(arr_3N, (short)(2*N+26), w);
    				//setKeyAndMask(addr_l, KAM_SET_KEY);
    				Util.setShort(arr_3N, (short)(2*N+30), KAM_SET_KEY);
//    				secretSeed.getKey(arr_N3, (short)0); //get SEED 
    				
    				//PRF(arr_seed, addr_l, arr_4N, N); //result = KEY
    				md256.doFinal(arr_3N, (short)0, (short)(3*N), arr_4N, N);	
    				
    				//setKeyAndMask(addr_l, KAM_SET_BITMASK_LEAST);
    				Util.setShort(arr_3N, (short)(2*N+30), KAM_SET_BITMASK_LEAST);
    				
    				//PRF(arr_seed, addr_l, arr_bitmask1, s0);
    				md256.doFinal(arr_3N, s0, (short)(3*N), arr_bitmask1, s0);
    				
    		        /* Mask the input */
    		        for ( i = (short)(2*N), j=0; j < ADDRESS_SIZE; i++, j++) {
    		            arr_4N[i] ^= arr_bitmask1[j];
    		        }
    		        md256.doFinal(arr_4N, s0, (short)(3*N), arr_4N, (short)(2*N));
    		        //F(arr_N, arr_N2, arr_N2); // hash input message directly into output buffer
    		           		        
    				//hashIt(arr_N2, (short)(N*8), arr_N);
    				//Util.arrayCopyNonAtomic(arr_N, (short)0, arr_N2, (short)0, N); //result = next iteration input  			
    			}
    			Util.arrayCopy(arr_4N, (short)(2*N), result, (short)(internal_offset+offset), N);//store node in result
    		}
    		else { // in case index==0 -> use sk segment directly
    			Util.arrayCopy(secKey, (short)(len*N), result, (short)(len*N+offset), N);
    		}
    			
    		
    		
    	} 
    }


    			
    

    //Function hashes input to output array of length hashLength
    //TODO: add error handling e.g. if output.length != hashLength
    private void hashIt(byte[] input, short hashLength, byte[]output) {
    	
    	 switch(hashLength) { 
         case 256:
        	 //MessageDigest md = MessageDigest.getInstance(MessageDigest.ALG_SHA_256, false);
        	 md256.reset();
        	 md256.update(input, (short)0, (short)input.length);
             md256.doFinal(input, (short)0, (short)input.length, output, (short) 0);
             //Util.arrayCopyNonAtomic(output, (short) 0, input, (short) 0, (short)(hashLength/8));
             md256.reset();
         	break;
         default:
        	 ISOException.throwIt(ISO7816.SW_UNKNOWN);
    	 }
    }
   
    private void sendSignature(APDU apdu, byte[] message) {
    	short maxData = APDU.getOutBlockSize();
        byte[] buffer = apdu.getBuffer();
        Util.arrayFillNonAtomic(buffer, (short) 0, maxData, (byte) 0);
        apdu.setOutgoing();
        short len = (short)message.length;
        
        apdu.setOutgoingLength((short)(len+68));
        apdu.sendBytesLong(arr_pk_idx, (short)0, (short)(68));
        apdu.sendBytesLong(message, (short)0, len);
//        short NE = apdu.setOutgoing();
//        apdu.setOutgoingLength(NE);
        
        //apdu.setOutgoingAndSend((short) 0, maxData);
    }
    private void getResponse(APDU apdu) {
 	   short offset = response_offset[0];
 	  apdu.setOutgoing();
 	 if(offset==0) {
		  apdu.setOutgoingLength((short)(68));
		  apdu.sendBytesLong(arr_pk_idx, s0, (short)(68));
		  offset+=(68);
	  }
 	 else if((offset+255)<=signatureSize) {
 		  apdu.setOutgoingLength((short)255);
 		  apdu.sendBytesLong(signature, offset, (short)255);
 		  offset+=255;
 	  }
 	  else {
 		  apdu.setOutgoingLength((short)(signatureSize-offset));
 		  apdu.sendBytesLong(signature, offset, (short)(signatureSize-offset));
 		  offset = -1;
 	  }
 	  response_offset[0]=offset;
    }
    private void sendSignatureShort(APDU apdu) {
        apdu.setOutgoing();
        apdu.setOutgoingLength((short)2);
        Util.setShort(arr_N, s0, (short)(signatureSize+64));
        apdu.sendBytesLong(arr_N, (short)0, (short)2);
//    	short maxData = APDU.getOutBlockSize();
//        byte[] buffer = apdu.getBuffer();
//        Util.arrayFillNonAtomic(buffer, (short) 0, maxData, (byte) 0);
//    	short numMsg = (short)(message.length/maxData);
//    	short rest = (short)(message.length%maxData);
//    	
//    	short s = 0;
//    	//apdu.setOutgoing();
//    	//apdu.setOutgoingLength((short) message.length);
//    	for(; s<numMsg; ++s) {
//    		
//    		apdu.setOutgoingAndSend(s0, maxData);
//    		Util.arrayCopyNonAtomic(message, (short)(s*maxData), buffer, s0, maxData);
//    		apdu.sendBytes(s0, maxData);
//    	}
//    	Util.arrayCopyNonAtomic(message, (short)(s*maxData), buffer, s0, rest);
//
//		apdu.sendBytes(s0, rest);
    }
    

    //generic sending
    private void send_response(APDU apdu, byte[] message, short len) {
    	apdu.setOutgoing();
    	apdu.setOutgoingLength(len);
    	apdu.sendBytesLong(message, (short)0, len);
    }
    
   private byte[] helloTest(APDU apdu, short index){
	   byte hash[] = new byte[N];
	   
	   MessageDigest md = MessageDigest.getInstance(MessageDigest.ALG_SHA_256, false);
	   
	   
	   byte buffer[] = apdu.getBuffer();      
	   short bytesRead = apdu.setIncomingAndReceive();
       short echoOffset = (short) 0;
       
       
       
    
       while (bytesRead > 0) {
           Util.arrayCopyNonAtomic(buffer, ISO7816.OFFSET_CDATA, echoBytes, echoOffset, bytesRead);
           echoOffset += bytesRead;
           bytesRead = apdu.receiveBytes(ISO7816.OFFSET_CDATA);
       }

       apdu.setOutgoing();
       apdu.setOutgoingLength((short) (echoOffset + 5));

       // echo header
       apdu.sendBytes((short) 0, (short) 5);
       // echo data
       apdu.sendBytesLong(echoBytes, (short) 0, echoOffset);
       return hash;
   }
   
	   
}
   
